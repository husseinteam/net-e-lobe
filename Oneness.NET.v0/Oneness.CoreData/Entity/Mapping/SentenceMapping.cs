﻿using Oneness.CoreData.Entity.Base;
using Oneness.CoreData.Entity.POCO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.CoreData.Entity.Mapping {
    public class SentenceMapping : BaseMapping<Sentence> {

        #region Ctor

        public SentenceMapping() {


            #region Properties

            this.Property(t => t.Text).HasColumnName("Text").IsRequired().HasMaxLength(450)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("UQ_Text") { IsUnique = true }));

            #endregion

            #region Relations

           
            #endregion

            this.ToTable("Sentences", "Translation");

        }

        #endregion

    }
}
