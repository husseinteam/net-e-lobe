﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oneness.CoreData.Entity.Base;
using Oneness.CoreData.Entity.POCO;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Oneness.CoreData.Entity.Mapping {
    public class AnswerMapping : BaseMapping<Answer> {

        public AnswerMapping() {

            #region Properties

            this.Property(t => t.IsCorrectMatch).HasColumnName("IsCorrectMatch").IsRequired();

            #endregion

            #region Navigation Properties

            this.HasOptional(t => t.Sentence).WithMany(t => t.Answers).Map(x => x.MapKey("SentenceID")).WillCascadeOnDelete(false);

            this.HasOptional(t => t.Question).WithMany(t => t.Answers).Map(x => x.MapKey("QuestionID"));

            #endregion

            this.ToTable("Answers", "Translation");

        }

    }
}
