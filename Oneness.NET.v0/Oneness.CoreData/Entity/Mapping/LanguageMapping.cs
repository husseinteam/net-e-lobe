﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Oneness.CoreData.Entity.Base;
using Oneness.CoreData.Entity.POCO;
using System.Data.Entity.Infrastructure.Annotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Oneness.CoreData.Entity.Mapping {
    public class LanguageMapping : BaseMapping<Language> {

        public LanguageMapping() {

            #region Properties

            this.Property(t => t.Name).HasColumnName("Name").IsRequired().HasMaxLength(64)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName,
                    new IndexAnnotation(
                        new IndexAttribute("UQ_Name") { IsUnique = true }));

            this.Property(t => t.Description).HasColumnName("Description").HasMaxLength(255).IsRequired();

            #endregion

            #region Navigation Properties

            #endregion

            this.ToTable("Languages", "Translation");

        }

    }
}
