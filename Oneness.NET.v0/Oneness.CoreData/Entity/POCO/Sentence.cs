﻿using Oneness.CoreData.Entity.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.CoreData.Entity.POCO {
    
    public class Sentence : BaseModel {

        #region Ctor

        public Sentence() {

            Manifests = new List<Manifest>();
            Answers = new List<Answer>();
            Questions = new List<Question>();
            Quizzes = new List<Quiz>();
                
        }

        #endregion

        #region Properties

        public String Text { get; set; }

        #endregion

        #region Navigation Properties

        public virtual ICollection<Manifest> Manifests { get; set; }
        public virtual ICollection<Answer> Answers { get; set; }
        public virtual ICollection<Question> Questions { get; set; }
        public virtual ICollection<Quiz> Quizzes { get; internal set; }

        #endregion

    }
}
