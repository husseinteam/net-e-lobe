﻿using Oneness.CoreData.Entity.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.CoreData.Entity.POCO {
    
    public class Language : BaseModel {

        #region Ctor

        public Language() {

            Manifests = new List<Manifest>();

        }

        #endregion

        #region Properties


        public String Name { get; set; }
        
        public String Description { get; set; }

        #endregion

        #region Navigation Properties

        
        public virtual ICollection<Manifest> Manifests { get; internal set; }

        #endregion


    }
}
