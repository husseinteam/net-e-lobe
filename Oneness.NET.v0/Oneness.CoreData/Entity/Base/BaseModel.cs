﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Oneness.CoreData.Entity.Base {
    
    public class BaseModel {

        public Int64 ID { get; set; }

    }
}
