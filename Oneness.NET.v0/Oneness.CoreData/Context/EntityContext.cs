﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oneness.CoreData.Entity.Mapping;
using Oneness.CoreData.Entity.POCO;
using Oneness.CoreData.Migrations;

namespace Oneness.CoreData.Context {
    [DbConfigurationType(typeof(Configuration))]
    public class EntityContext : DbContext, IDisposable {


        #region Ctors

        public EntityContext()
            : base(SConnectionProvider.CurrentConnection, true) {

        }

        #endregion

        #region Overrides
        protected override void OnModelCreating(DbModelBuilder modelBuilder) {

            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new LanguageMapping());
            modelBuilder.Configurations.Add(new QuestionMapping());
            modelBuilder.Configurations.Add(new AnswerMapping());
            modelBuilder.Configurations.Add(new QuizMapping());
            modelBuilder.Configurations.Add(new ManifestMapping());
            modelBuilder.Configurations.Add(new SentenceMapping());

        }

        #endregion

        #region DbSets

        public DbSet<Language> Languages { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Quiz> Quizzes { get; set; }
        public DbSet<Manifest> Manifests { get; set; }
        public DbSet<Sentence> Sentences { get; set; }

        #endregion

    }
}
