﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machinery.Extensions.Core;
using Oneness.CoreData.Context;
using Oneness.CoreData.Entity.POCO;

namespace Oneness.CoreData.Extensions {
    public static class EntityContextExtensions {

        public static async Task<EntityContext> SeedSelf(this EntityContext context) {

            await context.TruncateAll();
            var langTR = new Language() { Name = "tr-TR", Description = "Türkçe" };
            var langEN = new Language() { Name = "en-EN", Description = "English" };
            context.Languages.Extend(langTR, langEN);

            var mTR = new Manifest() { Sentence = new Sentence() { Text = "Sınav 1" }, Language = langTR };
            var mEN = new Manifest() { Sentence = new Sentence() { Text = "Quiz 1" }, Language = langEN };
            context.Manifests.Extend(mTR, mEN);

            var qzTR = new Quiz() { Sentence = mTR.Sentence };
            var qzEN = new Quiz() { Sentence = mEN.Sentence};
            context.Quizzes.Extend(qzTR, qzEN);

            #region Manifest

            Manifest mf01, mf02, mf03, mf11, mf12, mf13, mf21, mf22, mf23, mf31, mf32, mf33;
            Sentence s01, s02, s03, s11, s12, s13, s21, s22, s23, s31, s32, s33;
            Question q01, q02, q03, q11, q12, q13, q21, q22, q23, q31, q32, q33;
            context.Manifests.Extend(
                mf01 = new Manifest() { Sentence = (s01 = new Sentence() { Text = "Ak Akçe Kara Gün İçindir" }), Language = langTR },
                mf02 = new Manifest() { Sentence = (s02 = new Sentence() { Text = "Aslan Gibi Ye Aslan Gibi Çalış" }), Language = langTR },
                mf03 = new Manifest() { Sentence = (s03 = new Sentence() { Text = "Üzüm Üzüme Baka Baka Kararır" }), Language = langTR }
                );
            context.Manifests.Extend(
                mf11 = new Manifest() { Sentence = (s11 = new Sentence() { Text = "White fox is running away" }), Language = langEN },
                mf12 = new Manifest() { Sentence = (s12 = new Sentence() { Text = "Green fox is at home" }), Language = langEN },
                mf13 = new Manifest() { Sentence = (s13 = new Sentence() { Text = "Horses run along parking statations" }), Language = langEN }
                );

            context.Manifests.Extend(
                mf21 = new Manifest() { Sentence = (s21 = new Sentence() { Text = "Delikanlı adam öfkesini yenen kişiye denir" }), Language = langTR },
                mf22 = new Manifest() { Sentence = (s22 = new Sentence() { Text = "İlim ilim bilmentir ilim kendini bilmektir" }), Language = langTR },
                mf23 = new Manifest() { Sentence = (s23 = new Sentence() { Text = "Yalnızlık Allah'a mahsustur" }), Language = langTR }
                );
            context.Manifests.Extend(
                mf31 = new Manifest() { Sentence = (s31 = new Sentence() { Text = "Parking is a crucial problem for butterflies" }), Language = langEN },
                mf32 = new Manifest() { Sentence = (s32 = new Sentence() { Text = "House taxes inflated enormously" }), Language = langEN },
                mf33 = new Manifest() { Sentence = (s33 = new Sentence() { Text = "Love is affection" }), Language = langEN }
                );

            Sentence s001, s002, s003, s004, s005, s006,
                s011, s012, s013, s014, s015, s016,
                s021, s022, s023, s024, s025, s026,
                s031, s032, s033, s034, s035, s036;
            Manifest mf001 = new Manifest(), mf002 = new Manifest(), mf003 = new Manifest(), mf004 = new Manifest(), mf005 = new Manifest(), mf006 = new Manifest(),
                mf011 = new Manifest(), mf012 = new Manifest(), mf013 = new Manifest(), mf014 = new Manifest(), mf015 = new Manifest(), mf016 = new Manifest(),
                mf021 = new Manifest(), mf022 = new Manifest(), mf023 = new Manifest(), mf024 = new Manifest(), mf025 = new Manifest(), mf026 = new Manifest(),
                mf031 = new Manifest(), mf032 = new Manifest(), mf033 = new Manifest(), mf034 = new Manifest(), mf035 = new Manifest(), mf036 = new Manifest();
            context.Manifests.Extend(
               mf001 = new Manifest() {  Sentence = (s001 = new Sentence() { Text = "Beyaz para kararır" }), Language = langTR },
               mf002 = new Manifest() {  Sentence = (s002 = new Sentence() { Text = "Helal Kazanılmış Para Zor Zamanlarda Kullanılır" }), Language = langTR },
               mf003 = new Manifest() {  Sentence = (s003 = new Sentence() { Text = "Çok çalışan yemeği hakkıyla yer" }), Language = langTR },
               mf004 = new Manifest() {  Sentence = (s004 = new Sentence() { Text = "Çalışmakla yemek yemek aynı anda olmaz" }), Language = langTR },
               mf005 = new Manifest() {  Sentence = (s005 = new Sentence() { Text = "İki üzüm aynı olmaz" }), Language = langTR },
               mf006 = new Manifest() {  Sentence = (s006 = new Sentence() { Text = "Beraber bulunan kişilerin huyları benzeşir" }), Language = langTR },

               mf011 = new Manifest() {  Sentence = (s011 = new Sentence() { Text = "Snow white runs like a fox" }), Language = langEN },
               mf012 = new Manifest() {  Sentence = (s012 = new Sentence() { Text = "An animal with a long tail goes away" }), Language = langEN },
               mf013 = new Manifest() {  Sentence = (s013 = new Sentence() { Text = "A living creature with a color of a leaf stays home" }), Language = langEN },
               mf014 = new Manifest() {  Sentence = (s014 = new Sentence() { Text = "Green hair is a best fit for a fox" }), Language = langEN },
               mf015 = new Manifest() {  Sentence = (s015 = new Sentence() { Text = "Running animals take a trip near parking facilities" }), Language = langEN },
               mf016 = new Manifest() {  Sentence = (s016 = new Sentence() { Text = "4 legged animals run like a charm" }), Language = langEN },

               mf021 = new Manifest() {  Sentence = (s021 = new Sentence() { Text = "Er kişi gazabına üstün gelir" }), Language = langTR },
               mf022 = new Manifest() {  Sentence = (s022 = new Sentence() { Text = "Adam olan sinirlenince delirir" }), Language = langTR },
               mf023 = new Manifest() {  Sentence = (s023 = new Sentence() { Text = "İlim ilmek ilmektir" }), Language = langTR },
               mf024 = new Manifest() {  Sentence = (s024 = new Sentence() { Text = "Gerçek ilim öze ilişkin olandır" }), Language = langTR },
               mf025 = new Manifest() {  Sentence = (s025 = new Sentence() { Text = "Allah-ü Teala yalnızlığı kendisine özgü kılmıştır" }), Language = langTR },
               mf026 = new Manifest() {  Sentence = (s026 = new Sentence() { Text = "Allah-ü Teala hakkında yalnızca izin verildiği kadar bilebiliriz" }), Language = langTR },

               mf031 = new Manifest() {  Sentence = (s031 = new Sentence() { Text = "Butterflies has a huge problem, like cars" }), Language = langEN },
               mf032 = new Manifest() {  Sentence = (s032 = new Sentence() { Text = "Butterflies die when they broke apart" }), Language = langEN },
               mf033 = new Manifest() {  Sentence = (s033 = new Sentence() { Text = "Government payment policies increased payings of properties" }), Language = langEN },
               mf034 = new Manifest() {  Sentence = (s034 = new Sentence() { Text = "We should use wax in houses everytime" }), Language = langEN },
               mf035 = new Manifest() {  Sentence = (s035 = new Sentence() { Text = "When we love somebody we get effected from him or her" }), Language = langEN },
               mf036 = new Manifest() {  Sentence = (s036 = new Sentence() { Text = "Love is like a touch of mother to her child" }), Language = langEN }
               );

            #endregion

            #region Questions

            context.Questions.Extend(
                q01 = new Question() { Sentence =  s01, Quiz = qzTR },
                q02 = new Question() { Sentence =  s02, Quiz = qzTR },
                q03 = new Question() { Sentence =  s03, Quiz = qzTR },

                q11 = new Question() { Sentence =  s11, Quiz = qzEN },
                q12 = new Question() { Sentence =  s12, Quiz = qzEN },
                q13 = new Question() { Sentence =  s13, Quiz = qzEN },

                q21 = new Question() { Sentence =  s21, Quiz = qzTR },
                q22 = new Question() { Sentence =  s22, Quiz = qzTR },
                q23 = new Question() { Sentence =  s23, Quiz = qzTR },

                q31 = new Question() { Sentence =  s31, Quiz = qzEN },
                q32 = new Question() { Sentence =  s32, Quiz = qzEN },
                q33 = new Question() { Sentence =  s33, Quiz = qzEN }
                );

            #endregion


            #region Answers

            Answer a001, a002, a003, a004, a005, a006,
                    a011, a012, a013, a014, a015, a016,
                    a021, a022, a023, a024, a025, a026,
                    a031, a032, a033, a034, a035, a036;
            context.Answers.Extend(
               a001 = new Answer() { Question = q01, Sentence =  s001, IsCorrectMatch = false },
               a002 = new Answer() { Question = q01, Sentence =  s002, IsCorrectMatch = true },
               a003 = new Answer() { Question = q22, Sentence =  s003, IsCorrectMatch = true },
               a004 = new Answer() { Question = q02, Sentence =  s004, IsCorrectMatch = false },
               a005 = new Answer() { Question = q03, Sentence =  s005, IsCorrectMatch = false },
               a006 = new Answer() { Question = q03, Sentence =  s006, IsCorrectMatch = true },

               a011 = new Answer() { Question = q11, Sentence =  s011, IsCorrectMatch = false },
               a012 = new Answer() { Question = q11, Sentence =  s012, IsCorrectMatch = true },
               a013 = new Answer() { Question = q12, Sentence =  s013, IsCorrectMatch = true },
               a014 = new Answer() { Question = q12, Sentence =  s014, IsCorrectMatch = false },
               a015 = new Answer() { Question = q13, Sentence =  s015, IsCorrectMatch = true },
               a016 = new Answer() { Question = q13, Sentence =  s016, IsCorrectMatch = false },

               a021 = new Answer() { Question = q21, Sentence =  s021, IsCorrectMatch = true },
               a022 = new Answer() { Question = q21, Sentence =  s022, IsCorrectMatch = false },
               a023 = new Answer() { Question = q22, Sentence =  s023, IsCorrectMatch = false },
               a024 = new Answer() { Question = q22, Sentence =  s024, IsCorrectMatch = true },
               a025 = new Answer() { Question = q23, Sentence =  s025, IsCorrectMatch = true },
               a026 = new Answer() { Question = q23, Sentence =  s026, IsCorrectMatch = false },

               a031 = new Answer() { Question = q31, Sentence =  s031, IsCorrectMatch = true },
               a032 = new Answer() { Question = q31, Sentence =  s032, IsCorrectMatch = false },
               a033 = new Answer() { Question = q32, Sentence =  s033, IsCorrectMatch = true },
               a034 = new Answer() { Question = q32, Sentence =  s034, IsCorrectMatch = false },
               a035 = new Answer() { Question = q33, Sentence =  s035, IsCorrectMatch = false },
               a036 = new Answer() { Question = q33, Sentence =  s036, IsCorrectMatch = true }
               );

            #endregion

            await context.SaveChangesAsync();
            return context;

        }


    }

}
