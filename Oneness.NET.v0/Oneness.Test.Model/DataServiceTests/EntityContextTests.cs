﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using Oneness.IoC.Core;
using Oneness.Protocols.RepositoryProtocols;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.Test.Model.DataServiceTests {
    [TestClass]
    public class EntityContextTests {

        [TestMethod]
        public async Task ContextSeedsItself() {

            var service = await SInstanceProvider.Container.Get<IContextService>().SeedDatabase();
            service.Succeeded.Assert<InvalidOperationException>("Context Service<{0}> failed, exception message is <{1}>"
                .PostFormat(typeof(IContextService).Name, service.ThrownException));

        }

        [TestMethod]
        public void ContextTruncates() {

            var service = SInstanceProvider.Container.Get<IContextService>().TruncateDatabase();
            service.Succeeded.Assert<InvalidOperationException>("Context Service<{0}> failed, exception message is: <{1}>"
                .PostFormat(typeof(IContextService).Name, service.ThrownException));

        }
    }

}
