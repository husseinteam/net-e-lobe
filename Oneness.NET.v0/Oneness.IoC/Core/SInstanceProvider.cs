﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using Oneness.Protocols.RepositoryProtocols;
using Oneness.Repository.ContextServices;
using Oneness.Mocking.Base;
using Oneness.Mocking.Mocks.TranslationScheme;
using Oneness.CoreData.Entity.POCO;
using Shared.Extensions.Core;
using Oneness.Protocols.RepositoryProtocols.Base;

namespace Oneness.IoC.Core {
    public static class SInstanceProvider {

        #region Ctor
        static SInstanceProvider() {

            Container.Bind<IContextService>()
                .To<ContextService>();
            //.WithConstructorArgument("rate", .2M);

            Container.Bind<IEntityMock<Answer>>()
                 .To<AnswerMock>();
            Container.Bind<IEntityMock<Question>>()
                 .To<QuestionMock>();
            Container.Bind<IEntityMock<Quiz>>()
                 .To<QuizMock>();
            Container.Bind<IEntityMock<Manifest>>()
                 .To<ManifestMock>();
            Container.Bind<IEntityMock<Language>>()
                 .To<LanguageMock>();
            Container.Bind<IEntityMock<Sentence>>()
                 .To<SentenceMock>();

            Container.Bind<IRepositoryBaseAsync<Language>>()
                .ToConstant(new LanguageMock().GenerateMock());
            Container.Bind<IRepositoryBaseAsync<Sentence >>()
                .ToConstant(new SentenceMock().GenerateMock());
            Container.Bind<IRepositoryBaseAsync<Answer>>()
                .ToConstant(new AnswerMock().GenerateMock());
            Container.Bind<IRepositoryBaseAsync<Manifest>>()
                .ToConstant(new ManifestMock().GenerateMock());
            Container.Bind<IRepositoryBaseAsync<Quiz>>()
                .ToConstant(new QuizMock().GenerateMock());
            Container.Bind<IRepositoryBaseAsync<Question>>()
                .ToConstant(new QuestionMock().GenerateMock());

        }

        #endregion

        private static Lazy<IKernel> _KernelProvider = new Lazy<IKernel>(() => new StandardKernel());
        public static IKernel Container {
            get {
                return _KernelProvider.Value;
            }
        }
    }
}
