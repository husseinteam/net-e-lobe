﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oneness.Protocols.RepositoryProtocols;
using Oneness.CoreData.Context;
using Shared.Extensions.Core;
using Oneness.CoreData.Extensions;

namespace Oneness.Repository.ContextServices {
    internal class ContextService : IContextService {

        #region Fields
        private Boolean _Succeeded;
        private Exception _ThrownException;
        #endregion

        #region Implementations

        public Boolean Succeeded {
            get {
                return _Succeeded;
            }
        }

        public Exception ThrownException {
            get {
                return _ThrownException;
            }
        }

        public async Task<IContextService> SeedDatabase() {
            using (var context = new EntityContext()) {
                try {
                    await context.SeedSelf();
                    _Succeeded = true;
                } catch (Exception e) {
                    _Succeeded = false;
                    _ThrownException = e;
                }
            }
            return this;
        }

        public IContextService TruncateDatabase() {
            using (var context = new EntityContext()) {
                try {
                    context.TruncateAll();
                    _Succeeded = true;
                } catch (Exception e) {
                    _Succeeded = false;
                    _ThrownException = e;
                }
            }
            return this;
        }

        #endregion

    }
}
