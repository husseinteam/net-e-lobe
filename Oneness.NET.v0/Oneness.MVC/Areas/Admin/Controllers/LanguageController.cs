﻿using Oneness.CoreData.Entity.POCO;
using Oneness.Protocols.RepositoryProtocols;
using Oneness.UnitOfWork.Core;
using Oneness.UnitOfWork.Protocols;
using Oneness.ViewModel.EntityViewModels.QuizViewModels;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Oneness.ViewModel.EntityViewModels.Extensions;
using Oneness.Protocols.RepositoryProtocols.Base;
using Oneness.UnitOfWork.Base;
using Ninject;

namespace Oneness.MVC.Areas.Admin.Controllers {
    [OutputCache(Duration = 60)]
    public class LanguageController : Controller {

        #region Actions

        // GET: Admin/Language
        [HttpGet]
        public async Task<ActionResult> Index() {

            var response = await UnitOfWork.AllLanguages();

            return View(response.Select(m => m.ToViewModel()));

        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public async Task<ActionResult> Add() {

            if (Request.IsAjaxRequest()) {

                var name = Request["Name"].ToType<String>();
                var description = Request["Description"].ToType<String>();
                var langID = Request["CurrentLanguageID"].ToType<Int64>();

                IRepositoryBaseAsync<Language> response;
                if (langID == default(Int64)) {
                    response = await UnitOfWork.InsertLanguage(new Language() {
                        Name = name,
                        Description = description
                    });
                } else {
                    response = await UnitOfWork.UpdateLanguage(langID, lg => lg.Assign(l => l.Name = name).Assign(l => l.Description = description));
                }

                return View("Index");

            } else {
                return View("Index");
            }
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public async Task<ActionResult> Delete() {

            if (Request.IsAjaxRequest()) {
                var langID = Request["LanguageIDToBeDeleted"].ToType<Int64>();

                var response = await UnitOfWork.DeleteLanguage(langID);

                return View("Index");

            } else {
                return View("Index");
            }
        }


        #endregion

        #region UnitOfWork

        private IManifestOfWork UnitOfWork {
            get {
                return SUnitProvider.Container.Get<IManifestOfWork>();
            }
        }

        #endregion

    }
}