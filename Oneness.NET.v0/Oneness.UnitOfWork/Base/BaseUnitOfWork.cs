﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oneness.IoC.Core;
using Oneness.UnitOfWork.Protocols;
using Oneness.CoreData.Entity.Base;
using Oneness.Protocols.RepositoryProtocols.Base;
using Shared.Extensions.Core;

namespace Oneness.UnitOfWork.Base {
    public abstract class BaseUnitOfWork<TEntity> : IUnitOfWork 
            where TEntity  : BaseModel {

    }
}
