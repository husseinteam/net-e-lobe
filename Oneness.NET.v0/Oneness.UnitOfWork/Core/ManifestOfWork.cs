﻿
using System;
using System.Threading.Tasks;
using Oneness.Protocols.RepositoryProtocols.Base;
using Oneness.Protocols.RepositoryProtocols;
using Ninject;
using Oneness.UnitOfWork.Base;
using Shared.Extensions.Core;
using System.Collections.Generic;
using Oneness.CoreData.Entity.POCO;
using Oneness.IoC.Core;
using Oneness.Mocking.Base;
using Oneness.UnitOfWork.Protocols;

namespace Oneness.UnitOfWork.Core {
    internal class ManifestOfWork : BaseUnitOfWork<Manifest>, IManifestOfWork {

        public TResponse RunWorker<TResponse>(Func<IRepositoryBaseAsync<Manifest>, IRepositoryBaseAsync<Sentence>, IRepositoryBaseAsync<Language>, TResponse> worker) {

            var mfobj = SInstanceProvider.Container.Get<IRepositoryBaseAsync<Manifest>>();
            var sntkobj = SInstanceProvider.Container.Get<IRepositoryBaseAsync<Sentence>>();
            var langobj = SInstanceProvider.Container.Get<IRepositoryBaseAsync<Language>>();

            return worker(mfobj, sntkobj, langobj);

        }

        public async Task<IRepositoryBaseAsync<Manifest>> DeleteManifest(Int64 manifestID) {


            var response = await this.RunWorker<Task<IRepositoryBaseAsync<Manifest>>>(async (mf, snt, lang) => {
                var manifest = await mf.TheAsync(manifestID);
                await mf.DeleteAsync(manifest);
                return mf;
            });
            return response;

        }

        public async Task<IRepositoryBaseAsync<Manifest>> InsertManifest(Int64 languageID, Int64 sentenceID) {

            languageID.AssertNotNull();
            sentenceID.AssertNotNull();

            var response = await this.RunWorker<Task<IRepositoryBaseAsync<Manifest>>>(async (mf, snt, lang) => {
                var language = await lang.TheAsync(languageID);
                var sentence = await snt.TheAsync(sentenceID);
                await mf.InsertAsync(new Manifest().Assign(mff => mff.Language = language).Assign(mff => mff.Sentence = sentence));
                return mf;
            });
            return response;

        }

        public async Task<IRepositoryBaseAsync<Manifest>> UpdateManifest(Int64 manifestID, Int64 languageID, Int64 sentenceID, Func<Manifest, Manifest> modifier) {

            languageID.AssertNotNull();
            manifestID.AssertNotNull();
            sentenceID.AssertNotNull();

            var response = await this.RunWorker<Task<IRepositoryBaseAsync<Manifest>>>(async (mf, snt, lang) => {
                var language = await lang.TheAsync(languageID);
                var sentence = await snt.TheAsync(sentenceID);
                var manifest = await mf.TheAsync(manifestID);
                await mf.UpdateAsync(manifest, mff => mff.Assign(mfff => mfff.Language = language).Assign(mfff => mfff.Sentence = sentence));
                return mf;
            });
            return response;

        }

        public Task<IEnumerable<Sentence>> AllSentences() {
            return this.RunWorker<Task<IEnumerable<Sentence>>>(async (mf, snt, lang) => {
                var all = await snt.AllAsync();
                return all;
            });

        }
        public Task<IEnumerable<Language>> AllLanguages() {

            return this.RunWorker<Task<IEnumerable<Language>>>(async (mf, snt, lang) => {
                var all = await lang.AllAsync();
                return all;
            });

        }
        public Task<IEnumerable<Manifest>> AllManifests() {

            return this.RunWorker<Task<IEnumerable<Manifest>>>(async (mf, snt, lang) => {
                var all = await mf.AllAsync();
                return all;
            });

        }

        public async Task<IRepositoryBaseAsync<Sentence>> DeleteSentence(long sentenceID) {

            var response = await this.RunWorker<Task<IRepositoryBaseAsync<Sentence>>>(async (mf, snt, lang) => {
                var sentence = await snt.TheAsync(sentenceID);
                await snt.DeleteAsync(sentence);
                return snt;
            });
            return response;

        }

        public async Task<IRepositoryBaseAsync<Language>> DeleteLanguage(long languageID) {

            var response = await this.RunWorker<Task<IRepositoryBaseAsync<Language>>>(async (mf, snt, lang) => {
                var language = await lang.TheAsync(languageID);
                await lang.DeleteAsync(language);
                return lang;
            });
            return response;

        }

        public async Task<IRepositoryBaseAsync<Language>> UpdateLanguage(Int64 languageID, Func<Language, Language> modifier) {

            var response = await this.RunWorker<Task<IRepositoryBaseAsync<Language>>>(async (mf, snt, lang) => {
                var language = await lang.TheAsync(languageID);
                await lang.UpdateAsync(language, modifier);
                return lang;
            });
            return response;

        }

        public async Task<IRepositoryBaseAsync<Language>> InsertLanguage(Language language) {
            var response = await this.RunWorker<Task<IRepositoryBaseAsync<Language>>>(async (mf, snt, lang) => {
                await lang.InsertAsync(language);
                return lang;
            });
            return response;

        }

        public async Task<IRepositoryBaseAsync<Sentence>> UpdateSentence(long sentenceID, Func<Sentence, Sentence> modifier) {

            var response = await this.RunWorker<Task<IRepositoryBaseAsync<Sentence>>>(async (mf, snt, lang) => {
                var sentence = await snt.TheAsync(sentenceID);
                await snt.UpdateAsync(sentence, modifier);
                return snt;
            });
            return response;

        }

        public async Task<IRepositoryBaseAsync<Sentence>> InsertSentence(Sentence sentence) {

            var response = await this.RunWorker<Task<IRepositoryBaseAsync<Sentence>>>(async (mf, snt, lang) => {
                await snt.InsertAsync(sentence);
                return snt;
            });
            return response;

        }
    }
}
