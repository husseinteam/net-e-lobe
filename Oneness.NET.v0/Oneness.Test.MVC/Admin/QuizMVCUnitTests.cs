﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Oneness.MVC.Areas.Admin.Controllers;
using System.Threading.Tasks;
using System.Web.Mvc;
using Oneness.Test.MVC.Extensions;
using System.Collections.Generic;
using Newtonsoft.Json;
using Shared.Extensions.Core;
using Oneness.ViewModel.EntityViewModels.QuizViewModels;
using Oneness.Protocols.RepositoryProtocols;
using Oneness.IoC.Core;
using Ninject;
using Oneness.Protocols.RepositoryProtocols.Base;
using Oneness.CoreData.Entity.POCO;
using Oneness.UnitOfWork.Base;
using Oneness.UnitOfWork.Protocols;

namespace Oneness.Test.MVC.Admin {
    [TestClass]
    public class QuizMVCUnitTests {
        [TestMethod]
        public async Task QuizControllerAddReturnsProperly() {

            var controller = new QuizController();
            controller.MakeAjaxRequest();
            var result = await controller.Add() as JsonResult;
            dynamic data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(result.Data));
            Assert.IsNotNull(data["faultMessage"]);
            Assert.IsNotNull(data["done"]);
            Assert.IsTrue((Boolean)data["done"]);

        }

        [TestMethod]
        public async Task QuizIndexControllerReturnsProperly() {

            var controller = new QuizController();
            controller.SetFakeControllerContext();
            var index = await controller.Index() as ViewResult;
            var result = index.ViewData.Model as IEnumerable<QuizViewModel>;

            Assert.IsNotNull(result);
            result.Enumerate(qz => {
                Assert.IsNotNull(qz.SentenceViewModel, "qz.SentenceViewModel");
            }).Enumerate(qz => qz.QuestionViewModels.Enumerate(qs => Assert.IsNotNull(qs.SentenceViewModel, "qs.SentenceViewModel"))
                    .Enumerate(qs => qs.AnswerViewModels.Enumerate(aw => Assert.IsNotNull(aw))));

        }

        [TestMethod]
        public async Task QuizControllerInsertsProperly() {

            var unit = SUnitProvider.Container.Get<IQuizOfWork>();

            var top = await unit.TopQuiz(e => e.ID);
            var collection = new KeyValuePair<String, Object>[] {
                new KeyValuePair<string, object>("CurrentQuizID", top.ID),
                new KeyValuePair<string, object>("Text", "Updated By Charming Instruments!<{0}>".GenerateIdentifier()),
            };

            var controller = new QuizController();
            controller.MakeAjaxRequest(collection);
            var result = await controller.Add() as JsonResult;
            dynamic data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(result.Data));

            Assert.IsNull(data["fullPostback"]);
            Assert.IsNotNull(data["faultMessage"]);
            Assert.IsNotNull(data["done"]);

        }
    }
}
