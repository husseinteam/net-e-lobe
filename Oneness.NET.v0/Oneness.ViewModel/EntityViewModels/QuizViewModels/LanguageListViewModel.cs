﻿
using Oneness.Localization.Resources;
using Oneness.Localization.Static;
using Oneness.CoreData.Context;
using Oneness.CoreData.Entity.POCO;
using Oneness.ViewModel.EntityViewModels.GenericBase;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Web;

namespace Oneness.ViewModel.EntityViewModels.QuizViewModels {
    public class LanguageListViewModel {

        [Required(ErrorMessageResourceType = typeof(ValidationErrors)
           , ErrorMessageResourceName = SLocalization.LanguageSelectionCannotBeNull)]
        public IEnumerable<LanguageViewModel> LanguageViewModels { get; set; }

        public Int64 SelectedLanguageID { get; set; }


    }

}