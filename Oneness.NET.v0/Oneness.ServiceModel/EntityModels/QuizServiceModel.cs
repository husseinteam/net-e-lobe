﻿using Oneness.CoreData.Entity.POCO;
using Oneness.ServiceModel.EntityModels.GenericBase;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.ServiceModel.EntityModels {
    [DataContract]
    public class QuizServiceModel : GenericServiceModel {


        [DataMember]
        public IEnumerable<QuestionServiceModel> Questions { get; set; }

        [DataMember]
        public SentenceServiceModel Sentence { get; set; }
    }

    public static class QuizServiceModelExtensions {
        public static QuizServiceModel ToServiceModel(this Quiz quiz) {

            var sm = new QuizServiceModel();
            quiz.MapTo(ref sm);
            sm.Sentence = quiz.Sentence.ToServiceModel();
            sm.Questions = quiz.Questions.Select<Question, QuestionServiceModel>(qst => qst.ToServiceModel());
            return sm;

        }
    }

}
