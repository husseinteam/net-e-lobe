﻿using Oneness.CoreData.Entity.POCO;
using Oneness.ServiceModel.EntityModels.GenericBase;
using Shared.Extensions.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Oneness.ServiceModel.EntityModels {
    [DataContract]
    public class AnswerServiceModel : GenericServiceModel {

        [DataMember]
        public Boolean IsCorrectMatch { get; set; }

        [DataMember]
        public SentenceServiceModel Sentence { get; set; }

        [DataMember]
        public Int64 QuestionID { get; set; }
        [DataMember]
        public Int64 QuizID { get; set; }

    }

    public static class AnswerServiceModelExtensions {

        public static AnswerServiceModel ToServiceModel(this Answer answer) {

            var sm = new AnswerServiceModel();
            answer.MapTo(ref sm);
            sm.QuestionID = answer.Question.ID;
            sm.QuizID = answer.Question.Quiz.ID;
            sm.Sentence = answer.Sentence.ToServiceModel();
            return sm;

        }

    }

}
