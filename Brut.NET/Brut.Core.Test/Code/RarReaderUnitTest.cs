﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Collections.Generic;
using System;
using Brut.Core.Read;

namespace Brut.Core.Test.Code {
    [TestClass]
    public class RarReaderUnitTest {

        #region ctor
        public RarReaderUnitTest() {
            ResourcesDirectory = new DirectoryInfo(Path.Combine(Environment.CurrentDirectory, "Resource"));
            DestinationDirectory = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
        }
        #endregion

        #region Tests
        [TestMethod]
        public void Make() {
            Assert.IsTrue(SRarReader.RarReader.Make(GetTarget("dcm1.rar"), DestinationDirectory));
        }
        [TestMethod]
        public void MakeEncrypted() {
            Assert.IsTrue(SRarReader.RarReader.Make(GetTarget("dcm2.rar"), DestinationDirectory, "10052006"));
        }
        #endregion

        #region Private Methods
        private FileInfo GetTarget(String fileName) {
            return new FileInfo(Path.Combine(ResourcesDirectory.FullName, fileName));
        }
        #endregion

        #region Properties
        internal DirectoryInfo ResourcesDirectory { get; private set; }
        internal DirectoryInfo DestinationDirectory { get; private set; }
        #endregion
    }
}
