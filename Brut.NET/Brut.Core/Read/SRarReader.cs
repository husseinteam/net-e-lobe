﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brut.Core.Read {
    public static class SRarReader {

        #region Singleton
        public static IReader RarReader {
            get {
                if (_Singleton == null) {
                    _Singleton = new Reader();
                }
                return _Singleton;
            }
        }
        #endregion

        #region Fields
        private static IReader _Singleton;
        #endregion
    }
}
