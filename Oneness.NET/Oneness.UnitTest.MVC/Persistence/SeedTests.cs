﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Oneness.MVC.Persistence;
using Machinery.Extensions.Core;
using System.Diagnostics;

namespace Oneness.UnitTest.MVC.Persistence {
    [TestClass]
    public class SeedTests {
        [TestMethod]
        public void SeedCompleteRunsOk() {

            var errors = SSeeder.CompleteSeed();
            errors.Enumerate(err => err.IfExists(er => er.Assign(e => Debug.Write(e.Message))));
            errors.Enumerate(e=>Assert.IsNull(e));

        }
    }
}
