﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Oneness.MVC.Persistence;
using Machinery.Extensions.Core;
using Oneness.MVC;
using Oneness.MVC.Areas.Atrium.Models;

namespace Oneness.UnitTest.MVC.Persistence {
    /// <summary>
    /// Summary description for AppContextInitialzesProperly
    /// </summary>
    [TestClass]
    public class AppContextTests {

        [ClassInitialize]
        public static void MyTestMethod(TestContext context) {


        }

        [TestMethod]
        public void AppContextInitializesProperly() {

            using (var context = new AppContext()) {

                Assert.AreEqual(0, context.Profiles.Magnitude());
                Assert.AreEqual(1, context.Profiles.Magnitude());

                var model = new LoginModel();
                var login = model.CommitDatabase((app, m) => app.Profiles.Find(0));
                Assert.IsNull(login);
            }

        }
    }
}
