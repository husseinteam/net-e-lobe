﻿using Oneness.MVC.Areas.AdminPanel.AppConfig;
using Oneness.MVC.Areas.Atrium.AppConfig;
using Oneness.MVC.Areas.Residence.AppConfig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Oneness.MVC.Areas.Redirect.AppConfig;
using System.Data.Entity;
using Oneness.MVC.Migrations;
using Oneness.MVC.Persistence;

namespace Oneness.MVC {
    public class MvcApplication : System.Web.HttpApplication {
        protected void Application_Start() {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            //Admin-Panel
            AdminPanelConfig.BundleConfig.RegisterBundles(BundleTable.Bundles);

            //Atrium
            AtriumConfig.BundleConfig.RegisterBundles(BundleTable.Bundles);

            //Residence
            ResidenceConfig.BundleConfig.RegisterBundles(BundleTable.Bundles);

            //Redirect
            RedirectConfig.BundleConfig.RegisterBundles(BundleTable.Bundles);

        }
        protected void Session_Start() {

            HttpConfig.SessionConfig();

        }

    }
}
