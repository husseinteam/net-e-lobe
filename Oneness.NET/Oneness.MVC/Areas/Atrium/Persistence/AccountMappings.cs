﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Web;
using Oneness.MVC.Areas.Atrium.Models;
using Oneness.MVC.Areas.Atrium.Persistence;
using Oneness.MVC.Areas.Residence.Models;
using Oneness.MVC.Persistence;

namespace Oneness.MVC.Areas.Atrium.Persistence {

    public class LoginModelMapping : MappingProvider<LoginModel> {

        public LoginModelMapping() {

            #region Properties

            this.Property(t => t.ServerContainerName)
                .HasColumnName("ServerContainerName")
                .HasMaxLength(255)
                .IsRequired();

            this.Property(t => t.Email)
                .HasColumnName("Email")
                .HasMaxLength(255)
                .IsRequired()
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName, 
                        new IndexAnnotation(
                            new IndexAttribute[] {
                                new IndexAttribute("UQ_Email", 3) { IsUnique = true },
                                new IndexAttribute("IX_Email", 1) {  }
                            }));

            this.Property(t => t.Password)
                .HasColumnName("Password")
                .HasMaxLength(255)
                .IsRequired()
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                        new IndexAnnotation(
                            new IndexAttribute[] {
                                new IndexAttribute("IX_Password", 2) {  }
                            }));


            #endregion

            #region Navigation Properties

            this.HasOptional(t => t.AssociatedProfile).WithOptionalDependent()
                .Map(x => x.MapKey("AssociatedProfileID")
                    .HasColumnAnnotation("AssociatedProfileID", IndexAnnotation.AnnotationName, new IndexAnnotation(
                        new IndexAttribute("UQ_AssociatedProfile", 1) { IsUnique = true })));

            this.HasOptional(t => t.ReferredContact).WithMany(t => t.Contacts)
                .Map(x => x.MapKey("ReferredContactID"));

            #endregion


            this.ToTable("LoginModels");

        }

    }


}