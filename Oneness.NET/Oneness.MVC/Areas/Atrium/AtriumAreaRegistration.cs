﻿using Machinery.Extensions.Core;
using System.Web.Mvc;

namespace Oneness.MVC.Areas.Atrium
{
    public class AtriumAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "atrium";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Atrium_default",
                "{0}/{{controller}}/{{action}}/{{id}}".PostFormat(AreaName),
                new { action = "login", id = UrlParameter.Optional }
            );
        }
    }
}