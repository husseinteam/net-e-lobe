﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Oneness.MVC.Areas.Residence.Models;

namespace Oneness.MVC.Persistence {
    public partial class AppContext : DbContext {

        #region Profile

        public DbSet<Profile> Profiles { get; set; }
        public DbSet<ActivityContainer> ActivityContainers { get; set; }
        public DbSet<FollowingActivity> FollowingActivities { get; set; }
        public DbSet<FollowerActivity> FollowerActivities { get; set; }
        public DbSet<PhotoActivity> PhotoActivities { get; set; }
        public DbSet<PhotoItem> PhotoItems { get; set; }
        public DbSet<BlogActivity> BlogActivities { get; set; }
        public DbSet<MessageActivity> MessageActivities { get; set; }
        
        #endregion

        #region Project

        public DbSet<Client> Clients { get; set; }
        public DbSet<ProjectItem> ProjectItems { get; set; }
        public DbSet<ProjectTimelineItem> ProjectTimelineItems { get; set; }

        #endregion

        #region Mail

        public DbSet<MailFolderContainer> MailFolderContainers { get; set; }
        public DbSet<MailEntry> MailEntries { get; set; }
        public DbSet<MailCategoryItem> MailCategoryItems { get; set; }
        public DbSet<TagItem> TagItems { get; set; }
        public DbSet<DraftFolder> DraftFolders { get; set; }
        public DbSet<InboxFolder> InboxFolders { get; set; }
        public DbSet<SentFolder> SentFolders { get; set; }
        public DbSet<ImportantFolder> ImportantFolders { get; set; }
        public DbSet<TrashFolder> TrashFolders { get; set; }

        #endregion

        #region File
        public DbSet<FileFolderItem> FileFolderItems { get; set; }
        public DbSet<FileItem> FileItems { get; set; }
        public DbSet<FileType> FileTypes { get; set; }
        #endregion

    }

}