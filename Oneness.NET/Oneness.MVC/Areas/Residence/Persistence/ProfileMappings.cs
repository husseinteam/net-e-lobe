﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using Oneness.MVC.Areas.Atrium.Models;
using Oneness.MVC.Areas.Residence.Models;
using Oneness.MVC.Persistence;

namespace Oneness.MVC.Areas.Residence.Persistence {

    public class ProfileMapping : MappingProvider<Profile> {

        public ProfileMapping() {

            #region Properties

            this.Property(t => t.Name)
                .HasColumnName("Name")
                .HasMaxLength(255).IsRequired().HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                        new IndexAnnotation(
                            new IndexAttribute[] {
                                new IndexAttribute("IX_Password", 2) {  }
                            }));
            this.Property(t => t.Surname)
                .HasColumnName("Surname")
                .HasMaxLength(255).IsRequired().HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                        new IndexAnnotation(
                            new IndexAttribute[] {
                                new IndexAttribute("IX_Surname", 3) {  }
                            }));
            this.Property(t => t.Address)
                .HasColumnName("Address")
                .HasMaxLength(255).IsRequired().HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                        new IndexAnnotation(
                            new IndexAttribute[] {
                                new IndexAttribute("IX_Address", 4) {  }
                            }));
            this.Property(t => t.AboutMe)
                .HasColumnName("AboutMe")
                .HasMaxLength(1024 * 1024 * 16).IsRequired();
            this.Property(t => t.CoorporationName)
                .HasColumnName("CoorporationName")
                .HasMaxLength(512).IsRequired();
            this.Property(t => t.CoorporateAddressLine1)
                .HasColumnName("CoorporateAddressLine1")
                .HasMaxLength(512).IsRequired();
            this.Property(t => t.CoorporateAddressLine2)
                .HasColumnName("CoorporateAddressLine2")
                .HasMaxLength(512).IsRequired();
            this.Property(t => t.CoorporatePosition)
                .HasColumnName("CoorporatePosition")
                .HasMaxLength(512).IsRequired();
            this.Property(t => t.CoorporatePhone)
                .HasColumnName("CoorporatePhone")
                .HasMaxLength(64).IsRequired();

            #endregion

            #region Navigation Properties
            
            this.HasOptional(t => t.Avatar).WithMany().Map(x => x.MapKey("AvatarID"));

            this.HasRequired(t => t.FileFolderItem).WithMany(t => t.AssociatedProfiles)
                .Map(x => x.MapKey("FileFolderID"));

            this.HasRequired(t => t.MailFolderContainer).WithRequiredDependent()
                .Map(x => x.MapKey("MailFolderContainerID")
                .HasColumnAnnotation("MailFolderContainerID", IndexAnnotation.AnnotationName
                    , new IndexAnnotation(new IndexAttribute("UQ_MailFolderContainer") { IsUnique = true, Order=1 })));

            this.HasMany(t => t.ProfileActivities).WithOptional()
                .Map(x => x.MapKey("ProfileID"));

            this.HasMany(t => t.ProjectItems)
                .WithOptional()
                .Map(x => x.MapKey("ProfileID"));

            #endregion

            this.ToTable("ProfileItems");
        }

    }
    public class ActivityContainerMapping : MappingProvider<ActivityContainer> {

        public ActivityContainerMapping() {

            #region Navigation Properties

            this.HasOptional(t => t.NewFollower).WithOptionalDependent()
                .Map(x => x.MapKey("NewFollowerID"));

            this.HasOptional(t => t.NewFollowing).WithOptionalDependent()
                .Map(x => x.MapKey("NewFollowingID"));

            this.HasOptional(t => t.NewMessage).WithOptionalDependent()
                .Map(x => x.MapKey("NewMessageID"));

            this.HasOptional(t => t.NewPhoto).WithOptionalDependent()
                .Map(x => x.MapKey("NewPhotoID"));

            this.HasOptional(t => t.NewPost).WithOptionalDependent()
                .Map(x => x.MapKey("NewPostID"));

            #endregion

            this.ToTable("ActivityContainers");

        }

    }

    public class FollowingActivityMapping : MappingProvider<FollowingActivity> {

        public FollowingActivityMapping() {
            #region Properties
            this.Property(t => t.ActedOn)
                .HasColumnName("ActedOn")
                .HasColumnType("datetime2")
                .IsRequired();

            this.Property(t => t.FollowType)
                    .HasColumnName("FollowType")
                    .IsRequired();

            #endregion

            #region Navigation PRoperties

            this.HasRequired(t => t.Leader).WithMany()
                .Map(x => x.MapKey("LeaderID")).WillCascadeOnDelete(false);


            this.HasRequired(t => t.Subject).WithMany()
                .Map(x => x.MapKey("SubjectID")).WillCascadeOnDelete(false);


            #endregion

            this.ToTable("FollowingActivities");
        }

    }
    public class FollowerActivityMapping : MappingProvider<FollowerActivity> {

        public FollowerActivityMapping() {
            #region Properties
            this.Property(t => t.ActedOn)
                .HasColumnName("ActedOn")
                .HasColumnType("datetime2")
                .IsRequired();

            this.Property(t => t.FollowType)
                    .HasColumnName("FollowType")
                    .IsRequired();

            #endregion
            #region Navigation PRoperties

            this.HasRequired(t => t.Leader).WithMany()
                .Map(x => x.MapKey("LeaderID")).WillCascadeOnDelete(false);


            this.HasRequired(t => t.Subject).WithMany()
                .Map(x => x.MapKey("SubjectID")).WillCascadeOnDelete(false);


            #endregion

            this.ToTable("FollowerActivities");
        }

    }
    public class PhotoActivityMapping : MappingProvider<PhotoActivity> {

        public PhotoActivityMapping() {
            #region Properties

            this.Property(t => t.ActedOn)
                .HasColumnName("ActedOn")
                .HasColumnType("datetime2")
                .IsRequired();

            #endregion

            #region Navigation Properties

            this.HasMany(t => t.PhotoItems).WithOptional()
                .Map(x => x.MapKey("PhotoActivityID"));

            this.HasOptional(t => t.Leader).WithMany()
                .Map(x => x.MapKey("LeaderID")).WillCascadeOnDelete(false);

            this.HasOptional(t => t.Subject).WithMany()
                .Map(x => x.MapKey("SubjectID")).WillCascadeOnDelete(false);

            #endregion

            this.ToTable("PhotoActivities");
        }

    }
    public class PhotoItemMapping : MappingProvider<PhotoItem> {

        public PhotoItemMapping() {
            #region Properties

            this.Property(t => t.ContainerName)
                .HasColumnName("ContainerName")
                .HasMaxLength(512)
                .IsRequired();

            this.Property(t => t.RealFileName)
                .HasColumnName("RealFileName")
                .HasMaxLength(512)
                .IsRequired();

            this.Property(t => t.GeneratedFileName)
                .HasColumnName("GeneratedFileName")
                .HasMaxLength(512)
                .IsRequired();

            this.Property(t => t.Extension)
                .HasColumnName("Extension")
                .HasMaxLength(255)
                .IsRequired();

            #endregion

            this.ToTable("PhotoItems");
        }

    }
    public class BlogActivityMapping : MappingProvider<BlogActivity> {

        public BlogActivityMapping() {

            #region Properties
            this.Property(t => t.ActedOn)
                .HasColumnName("ActedOn")
                .HasColumnType("datetime2")
                .IsRequired();

            this.Property(t => t.BlogText)
                    .HasColumnName("BlogText")
                    .HasMaxLength(1024 * 1024 * 8)
                    .IsRequired();
            this.Property(t => t.BlogActivityType)
                    .HasColumnName("BlogActivityType")
                    .IsRequired();

            #endregion
            #region Navigation Properties

            this.HasRequired(t => t.Leader).WithMany()
                .Map(x => x.MapKey("LeaderID")).WillCascadeOnDelete(false);


            this.HasRequired(t => t.Subject).WithMany()
                .Map(x => x.MapKey("SubjectID")).WillCascadeOnDelete(false);

            #endregion
            this.ToTable("BlogActivities");
        }

    }
    public class MessageActivityMapping : MappingProvider<MessageActivity> {

        public MessageActivityMapping() {

            #region Properties
            this.Property(t => t.ActedOn)
                .HasColumnName("ActedOn")
                .HasColumnType("datetime2")
                .IsRequired();

            this.Property(t => t.MessageText)
                    .HasColumnName("MessageText")
                    .HasMaxLength(1024 * 1024 * 8)
                    .IsRequired();

            #endregion

            #region Navigation Properties
            this.HasRequired(t => t.Leader).WithMany()
                .Map(x => x.MapKey("LeaderID")).WillCascadeOnDelete(false);


            this.HasRequired(t => t.Subject).WithMany()
                .Map(x => x.MapKey("SubjectID")).WillCascadeOnDelete(false);

            #endregion

            this.ToTable("MessageActivities");
        }

    }

}