﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oneness.MVC.Areas.Atrium.Persistence;
using Oneness.MVC.Areas.Residence.Models;
using Oneness.MVC.Persistence;

namespace Oneness.MVC.Areas.Residence.Persistence {

    public class ProjectItemMapping : MappingProvider<ProjectItem> {

        public ProjectItemMapping() {

            #region Properties

            this.Property(t => t.Status)
                .HasColumnName("Status")
                .IsRequired();
            this.Property(t => t.ProjectTitle)
                .HasColumnName("ProjectTitle")
                .HasMaxLength(255)
                .IsRequired();
            this.Property(t => t.ReleasedOn)
                .HasColumnName("ReleasedOn")
                .IsRequired();
            this.Property(t => t.LastUpdatedOn)
                .HasColumnName("LastUpdatedOn")
                .IsRequired();
            this.Property(t => t.CompletionRatio)
                .HasColumnName("CompletionRatio")
                .IsRequired();
            this.Property(t => t.Version)
                .HasMaxLength(64)
                .HasColumnName("Version")
                .IsRequired();
            this.Property(t => t.ProjectDescription)
                .HasColumnName("ProjectDescription")
                .HasMaxLength(512)
                .IsRequired();

            #endregion

            #region Navigation Properties

            this.HasRequired(t => t.Client)
                .WithMany()
                .Map(x => x.MapKey("ClientID"));

            this.HasRequired(t => t.ReleasedBy)
                .WithMany()
                .Map(x => x.MapKey("ReleasedByID"));

            this.HasRequired(t => t.ProjectLogo)
                .WithMany()
                .Map(x => x.MapKey("ProjectLogoID"));

            this.HasMany(t => t.Participants)
                .WithOptional()
                .Map(x => x.MapKey("ProjectItemID"));

            this.HasMany(t => t.ProjectActivities)
                .WithOptional()
                .Map(x => x.MapKey("ProjectItemID"));

            this.HasMany(t => t.ProjectTimeline)
                .WithOptional()
                .Map(x => x.MapKey("ProjectItemID"));

            this.HasMany(t => t.ProjectLabels)
                .WithOptional()
                .Map(x => x.MapKey("ProjectItemID"));

            this.HasMany(t => t.ProjectAttachments)
                .WithOptional()
                .Map(x => x.MapKey("ProjectItemID"));

            #endregion


        }
    }
    public class ProjectTimelineItemMapping : MappingProvider<ProjectTimelineItem> {

        public ProjectTimelineItemMapping() {

            #region Properties

            this.Property(t => t.Status)
                .HasColumnName("Status")
                .IsRequired();

            this.Property(t => t.Title)
                .HasMaxLength(128)
                .HasColumnName("Title")
                .IsRequired();

            this.Property(t => t.StartTime)
                .HasColumnName("StartTime")
                .IsRequired();

            this.Property(t => t.EndTime)
                .HasColumnName("EndTime")
                .IsRequired();

            this.Property(t => t.Comment)
                .HasMaxLength(1024)
                .HasColumnName("Comment")
                .IsRequired();


            #endregion

        }

    }

    public class ClientMapping : MappingProvider<Client> {

        public ClientMapping() {

            #region  Properties

            this.Property(t => t.Title)
                .HasColumnName("Title")
                .HasMaxLength(128)
                .IsRequired();

            #endregion

        }

    }

}
