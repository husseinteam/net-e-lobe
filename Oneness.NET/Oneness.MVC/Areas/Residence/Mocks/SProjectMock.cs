﻿using Oneness.MVC.Areas.Residence.Models;
using Machinery.Extensions.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Oneness.MVC.Persistence;
using Oneness.MVC.Areas.Atrium.Models;

namespace Oneness.MVC.Areas.Residence.Mocks {
    public class SProjectMock {

        public static IEnumerable<ProjectItem> Projects(LoginModel parentLogin, Func<ProjectItem, IEnumerable<ActivityContainer>> activityGenerator) {

            var titles = new String[] {
                "Contract with Zender Company", "There are many variations of passages"
            };

            var releaseDates = new DateTime[] {
                DateTime.Parse("14.08.2014"),
                DateTime.Parse("11.08.2014"),
            };

            var completionRatios = new Double[] { 0.5, 0.7 };

            for (int i = 0; i < titles.Length; i++) {
                var pji= new ProjectItem() {
                    Status = 119.Randomize() % 2 == 0,
                    CompletionRatio = completionRatios[i],
                    ProjectTitle = titles[i],
                    ReleasedOn = releaseDates[i],
                    ReleasedBy = parentLogin,
                    ProjectDescription =
@"
There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look
                even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing
",
                    Priority = EPriority.High,
                    ProjectLogo = new PhotoItem() {
                        Extension = "png",
                        ContainerName = parentLogin.ServerContainerName
                    }.Assign(pi => pi.ImportFile("~/Areas/Residence/Mocks/Static/zender_logo.png")),
                    Client = new Client() {
                        ID = 1,
                        Title = "Zender Company"
                    },
                    Version = "v1.4.{0}".PostFormat(9.Randomize()),
                    LastUpdatedOn = 22.DaysAgo(),
                }
                .Assign(pr => pr.ProjectActivities.Extend(activityGenerator(pr).ToArray()))
                .Assign(pr => pr.Participants.Extend(SProfileMock.RandomLogin()))
                .Assign(pr => pr.ProjectAttachments.Extend(SMailMock.Attachments(parentLogin).Shuffle().ToArray()))
                .Assign(pr => pr.ProjectLabels.Extend(SMailMock.ProjectLabels()))
                .Assign(pr => pr.ProjectTimeline.Extend(SProjectMock.ProjectTimeline().ToArray()));
                parentLogin.AssociatedProfile.ProjectItems.Extend(pji);
                yield return pji;
            }

        }
        public static IEnumerable<ProjectTimelineItem> ProjectTimeline() {

            var statuses = new ETimelineStatus[] {
                ETimelineStatus.Completed,
                ETimelineStatus.Accepted,
                ETimelineStatus.Sent,
                ETimelineStatus.Reported,
                ETimelineStatus.Accepted,
                ETimelineStatus.Sent
            };

            var titles = new String[] {
                "Create project in webapp",
                "Various versions",
                "There are many variations",
                "Latin words",
                "The generated Lorem",
                "The first line"
            };

            var startTimes = new DateTime[] {
                DateTime.Parse("16.07.2015 10:12:1"),
                DateTime.Parse("12.11.2015 10:20:1"),
                DateTime.Parse("15.07.2015 10:10:1"),
                DateTime.Parse("19.07.2015 10:10:2"),
                DateTime.Parse("14.01.2015 10:10:1"),
                DateTime.Parse("14.01.2015 10:10:1"),
            };

            var endTimes = new DateTime[] {
                DateTime.Parse("14.07.2014 10:16:36"),
                DateTime.Parse("14.07.2014 10:16:36"),
                DateTime.Parse("14.07.2014 10:16:36"),
                DateTime.Parse("14.07.2014 10:16:36"),
                DateTime.Parse("14.07.2014 10:16:36"),
                DateTime.Parse("14.07.2014 10:16:36"),
            };

            var comments = new String[] {
                "Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable.",
                "Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
                "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which",
                "Latin words, combined with a handful of model sentence structures",
                "The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc",
                @"The first line of Lorem Ipsum, ""Lorem ipsum dolor sit amet.."", comes from a line in section 1.10.32"
            };

            for (int i = 0; i < statuses.Length; i++) {
                yield return new ProjectTimelineItem() {
                    Status = statuses[i],
                    Title = titles[i],
                    StartTime = startTimes[i],
                    EndTime = endTimes[i],
                    Comment = comments[i]
                };
            }

        }
    }
}