﻿using Oneness.MVC.Areas.Residence.Mocks;
using Oneness.MVC.Areas.Residence.Models;
using Machinery.Extensions.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Oneness.MVC.Persistence;
using Oneness.MVC.Controllers;

namespace Oneness.MVC.Areas.Residence.Controllers {
    [OutputCache(Duration = 60)]
    [RouteArea("residence")]
    public class ProjectController : BaseController {

        // GET: Residence/Project
        public ActionResult Index() {

            return AccessGrant<ProjectItem>((idx, set) => View(set.ToList()));

        }

        // GET: Residence/Project
        public ActionResult Detail(Int32 id) {

            return AccessGrant<ProjectItem>((idx, set) => View(set.SingleOrDefault(m => m.ID == id)));

        }
        [HttpPost]
        public ActionResult Activities(Int32 id) {

            if (Request.IsAjaxRequest()) {
                return PartialView("_Activities", SResidenceProvider.ActivitiesOfProfile(id));
            }

            if (!ModelState.IsValid) {
                return View("detail", id);
            } else {
                return View("detail", id);
            }

        }

    }
}