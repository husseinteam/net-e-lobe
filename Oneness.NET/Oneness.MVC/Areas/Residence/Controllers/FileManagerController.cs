﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Oneness.MVC.Areas.Residence.Mocks;
using Oneness.MVC.Areas.Residence.Models;
using Oneness.MVC.Controllers;
using Oneness.MVC.Persistence;

namespace Oneness.MVC.Areas.Residence.Controllers {
    [OutputCache(Duration = 60)]
    [RouteArea("residence")]
    public class FileManagerController : BaseController {

        // GET: Residence/FileManager
        public ActionResult Index() {

            return AccessGrant<FileItem>((id, set) => View(set.ToList()));

        }

    }
}