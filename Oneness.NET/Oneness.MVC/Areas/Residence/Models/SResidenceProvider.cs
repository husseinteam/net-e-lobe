﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oneness.MVC.Persistence;
using Machinery.Extensions.Core;
using Oneness.MVC.Models;

namespace Oneness.MVC.Areas.Residence.Models {
    public class SResidenceProvider {


        public static IEnumerable<TagItem> MailTags(Int32 mailEntryID = 0) {

            var me = SContextProvider.Get<MailEntry>().SingleOrDefault(m => m.ID == mailEntryID);
            if (me == null) {
                return SContextProvider.Get<TagItem>().Unique(it => it.Description).ToArray();
            } else {
                return me.MailTags.ToArray();
            }

        }

        public static IEnumerable<TagItem> ProjectLabels(Int32 projectItemID = 0) {

            var pl = SContextProvider.Get<ProjectItem>().SingleOrDefault(p => p.ID == projectItemID);
            if (pl == null) {
                return SContextProvider.Get<TagItem>().Unique(it => it.Description).ToArray();
            } else {
                return pl.ProjectLabels.ToArray();
            }

        }

        public static IEnumerable<MailCategoryItem> Categories(params MailEntry[] mailEntries) {

            var cats = mailEntries.Select(me => me.MailCategory);
            if (cats.Count() > 0) {
                return cats.ToArray().Unique(c => c.InfoClass);
            } else {
                return SContextProvider.Get<MailCategoryItem>().Unique(it => it.InfoClass).ToArray();
            }

        }

        public static IEnumerable<FolderBase> GenericFolders() {

            var id = SContextProvider.Session<Int32>("login");
            var prof = SContextProvider.Get<Profile>().SingleOrDefault(m => m.ID == id);
            if (prof == null) {
                return new FolderBase[] {
                    SContextProvider.Get<InboxFolder>().FirstOrDefault() ?? new InboxFolder(),
                    SContextProvider.Get<SentFolder>().FirstOrDefault() ?? new SentFolder(),
                    SContextProvider.Get<ImportantFolder>().FirstOrDefault() ?? new ImportantFolder(),
                    SContextProvider.Get<DraftFolder>().FirstOrDefault() ?? new DraftFolder(),
                    SContextProvider.Get<TrashFolder>().FirstOrDefault() ?? new TrashFolder(),
                };
            } else {
                return new FolderBase[] {
                    prof.MailFolderContainer.InboxFolder ?? new InboxFolder(),
                    prof.MailFolderContainer.SentFolder ?? new SentFolder(),
                    prof.MailFolderContainer.ImportantFolder ?? new ImportantFolder(),
                    prof.MailFolderContainer.DraftFolder ?? new DraftFolder(),
                    prof.MailFolderContainer.TrashFolder ?? new TrashFolder(),
                };
            }
        }

        private static Int32 activityPageIndex = 0;
        public static IPagedList<ActivityContainer> ActivitiesOfProfile(Int32 profileID, Boolean startNew = false) {

            if (startNew) {
                activityPageIndex = 0;
            }
            var list = SContextProvider.Get<Profile>().Single(pr => pr.ID == profileID)
                   .ProfileActivities.AsQueryable().ToPagedList(++activityPageIndex, 1);
            return list;
        }

        public static IPagedList<ActivityContainer> ActivitiesOfProjectItem(Int32 projectID, Boolean startNew = false) {

            if (startNew) {
                activityPageIndex = 0;
            }
            var list = SContextProvider.Get<ProjectItem>().Single(pr => pr.ID == projectID)
                .ProjectActivities.AsQueryable().ToPagedList(++activityPageIndex, 1);
            return list;

        }
    }
}