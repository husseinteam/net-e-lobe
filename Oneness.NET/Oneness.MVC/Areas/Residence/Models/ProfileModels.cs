﻿using Oneness.MVC.Areas.Atrium.Models;
using Machinery.Extensions.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Oneness.MVC.Persistence;
using Oneness.MVC.Areas.Residence.Resources;
using System.IO;

namespace Oneness.MVC.Areas.Residence.Models {

    public class Profile : KeyProvider {

        public Profile() {
            this.ProfileActivities = new List<ActivityContainer>();
            this.ProjectItems = new List<ProjectItem>();
        }

        [Required(ErrorMessageResourceType = typeof(ErrorMessages)
            , ErrorMessageResourceName = "NameNotSupplied")]
        [Display(Name = "Name")]
        public String Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessages)
            , ErrorMessageResourceName = "SurnameNotSupplied")]
        [Display(Name = "Surname")]
        public String Surname { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessages)
            , ErrorMessageResourceName = "AddressNotSupplied")]
        [Display(Name = "Home Address")]
        public String Address { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessages)
            , ErrorMessageResourceName = "AboutMeNotSupplied")]
        [Display(Name = "About You")]
        public String AboutMe { get; set; }

        public virtual PhotoItem Avatar { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessages)
          , ErrorMessageResourceName = "CoorporationNameNotSupplied")]
        [Display(Name = "Coorporate")]
        public String CoorporationName { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessages)
            , ErrorMessageResourceName = "CoorporateAddressLine1NotSupplied")]
        [Display(Name = "Corp. Address Line 1")]
        public String CoorporateAddressLine1 { get; set; }

        [Display(Name = "Corp. Address Line 2")]
        public String CoorporateAddressLine2 { get; set; }

        [Required(ErrorMessageResourceType = typeof(ErrorMessages)
         , ErrorMessageResourceName = "CoorporatePositionNotSupplied")]
        [Display(Name = "Corp. Position")]
        public String CoorporatePosition { get; set; }

        [RegularExpression(@"^\(\d{3}\)\s\d{3}\-\d{4}", ErrorMessageResourceType = typeof(ErrorMessages), ErrorMessageResourceName = "CoorporatePhoneNotSupplied")]
        [Required(ErrorMessageResourceType = typeof(ErrorMessages)
         , ErrorMessageResourceName = "CoorporatePhoneNotSupplied")]
        [Display(Name = "Corp. Phone")]
        public String CoorporatePhone { get; set; }


        public virtual ICollection<ActivityContainer> ProfileActivities { get; set; }
        public virtual ICollection<ProjectItem> ProjectItems { get; set; }

        public virtual MailFolderContainer MailFolderContainer { get; set; }

        public virtual FileFolderItem FileFolderItem { get; set; }

        [NotMapped]
        public Int32 FollowingCount {
            get {
                return ProfileActivities.Count(act => act.NewFollowing != null);
            }
        }
        [NotMapped]
        public Int32 FollowerCount {
            get {
                return ProfileActivities.Count(act => act.NewFollower != null);
            }
        }
        [NotMapped]
        public Int32 PostCount {
            get {
                return ProfileActivities.Count(act => act.NewPost != null);
            }
        }
        [NotMapped]
        public String PostActivityChartData {
            get {
                return "";
            }
        }
        [NotMapped]
        public String FollowingActivityChartData {
            get {
                return "";
            }
        }
        [NotMapped]
        public String FollowerActivityChartData {
            get {
                return "";
            }
        }
        [NotMapped]
        public String Identity {
            get {
                return "{0} {1}".PostFormat(Name, Surname);
            }
        }

    }

    public class ActivityContainer : KeyProvider {

        public virtual FollowingActivity NewFollowing { get; set; }
        public virtual FollowerActivity NewFollower { get; set; }
        public virtual PhotoActivity NewPhoto { get; set; }
        public virtual BlogActivity NewPost { get; set; }
        public virtual MessageActivity NewMessage { get; set; }

    }
    public abstract class ActivityBase : KeyProvider {

        [Required]
        [DataType(DataType.DateTime)]
        [Display(Name = "ActedOn")]
        public DateTime ActedOn { get; set; }

        public abstract String Manifest { get; }

        public virtual LoginModel Leader { get; set; }
        public virtual LoginModel Subject { get; set; }

        [NotMapped]
        public String ActedDay {
            get {
                return ActedOn.SimplifyDaysPassed();
            }
        }

        [NotMapped]
        public String ActedPastAgo {
            get {
                return ActedOn.ActualAgo();
            }
        }
    }
    public class FollowingActivity : ActivityBase {

        [Required]
        [Display(Name = "FollowType")]
        public EFollowType FollowType { get; set; }

        public override string Manifest {
            get {
                return "started following";
            }
        }
    }
    public class FollowerActivity : ActivityBase {

        [Required]
        [Display(Name = "FollowType")]
        public EFollowType FollowType { get; set; }

        public override string Manifest {
            get {
                return "had a new follower";
            }
        }
    }
    public class PhotoActivity : ActivityBase {

        public PhotoActivity() {
            this.PhotoItems = new List<PhotoItem>();
        }

        public override string Manifest {
            get {
                return "posted a new photo on";
            }
        }

        public virtual ICollection<PhotoItem> PhotoItems { get; set; }

    }

    public class PhotoItem : BaseServerFileItem {

        public String Extension { get; set; }

        [NotMapped]
        public String Base64 {
            get {
                return "data:image/{0};base64,{1}".PostFormat(Extension, Convert.ToBase64String(Data));
            }
        }

    }

    public class BlogActivity : ActivityBase {

        public virtual String BlogText { get; set; }

        [NotMapped]
        public String BlogTextTrimmed {
            get {
                return BlogText.TrimByWords(15);
            }
        }

        public EBlogActivityType BlogActivityType { get; set; }

        public override string Manifest {
            get {
                return "a new blog was posted by";
            }
        }
    }

    public class MessageActivity : ActivityBase {

        [Display(Name = "MessageText")]
        public String MessageText { get; set; }

        [Display(Name = "MessageTextTrimmed")]
        public String MessageTextTrimmed {
            get {
                return MessageText.TrimByWords(15);
            }
        }

        public override string Manifest {
            get {
                return "posted message on";
            }
        }
    }


    public enum EBlogActivityType {

        [Display(Name = "Post")]
        Post = 1

    }

    public enum EFollowType {

        [Display(Name = "Love")]
        Love = 1,
        [Display(Name = "Like")]
        Like,
        [Display(Name = "Follow")]
        Follow

    }
}