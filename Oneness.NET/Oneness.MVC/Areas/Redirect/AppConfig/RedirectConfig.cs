﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;


namespace Oneness.MVC.Areas.Redirect.AppConfig {
    public static class RedirectConfig {

        public class BundleConfig {

            public static void RegisterBundles(BundleCollection bundles) {

                bundles.Add(new StyleBundle("~/Content/redirect/css")
                    .Include("~/Areas/Redirect/Content/bootstrap.min.css")
                    .Include("~/Areas/Redirect/Content/font-awesome.min.css")
                    .Include("~/Areas/Redirect/Content/animate.css")
                    .Include("~/Areas/Redirect/Content/style.css")
                    );

                bundles.Add(new ScriptBundle("~/bundles/redirect/main")
                    .Include("~/Areas/Redirect/Scripts/main/jquery-2.1.1.js")
                    .Include("~/Areas/Redirect/Scripts/main/bootstrap.min.js")
                    );
 
            }
        }

    }
}