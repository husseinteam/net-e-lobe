﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oneness.MVC.Areas.Residence.Mocks;
using Machinery.Extensions.Core;
using System.IO;

namespace Oneness.MVC.Persistence {
    public static class SSeeder {

        public static String MapServerPath(this String virtualPath) {

            return @"P:\Source\Workspaces\CSharp\Oneness.NET\Oneness.MVC\{0}".PostFormat(virtualPath.TrimStart('~').TrimStart('/').Replace('/', '\\'));

        }

        public static List<Exception> CompleteSeed() {

            var perProfile = new DirectoryInfo("~/Images/ClientImages/PerProfile".MapServerPath());
            foreach (var file in perProfile.GetFiles()) {
                file.Delete();
            }
            foreach (var dir in perProfile.GetDirectories()) {
                dir.Delete(true);
            }

            var errors = new List<Exception>();
            Action<AppContext> saver = (ctx) => {
                try {
                    ctx.SaveChanges();
                } catch (Exception exc) {
                    errors.Add(exc);
                }
            };

            //Login Mock
            var login = SProfileMock.RandomLogin();
            var exLogin = login.CommitDatabase((app, lg) => {
                try {
                    app.LoginModels.Add(lg);
                    return null;
                } catch (Exception ex) {
                    return ex;
                }
            });
            errors.Add(exLogin);
            saver(AppContext.AppInstance);

            //Project Mock
            SProjectMock.Projects(login, (pr) => 1.Times(() => SProfileMock.ActivityContainer(login).Assign(cont => pr.ProjectActivities.Extend(cont))))
                .Enumerate(proj => {
                    var exc = proj.CommitDatabase((app, pr) => {
                        try {
                            app.ProjectItems.Add(pr);
                            return null;
                        } catch (Exception ex) {
                            return ex;
                        }
                    });
                    errors.Add(exc);
                });
            saver(AppContext.AppInstance);

            //Attachment Mock
            SMailMock.Attachments(login).Enumerate(m => {
                var exc = m.CommitDatabase((app, mm) => {
                    try {
                        app.FileItems.Add(mm);
                        return null;
                    } catch (Exception ex) {
                        return ex;
                    }
                });
                errors.Add(exc);
            });
            saver(AppContext.AppInstance);

            //Category Mock
            SMailMock.Categories().Enumerate(m => {
                var exc = m.CommitDatabase((app, mm) => {
                    try {
                        app.MailCategoryItems.Add(mm);
                        return null;
                    } catch (Exception ex) {
                        return ex;
                    }
                });
                errors.Add(exc);
            });
            saver(AppContext.AppInstance);

            //LabelItems Mock
            SMailMock.ProjectLabels().Enumerate(m => {
                var exc = m.CommitDatabase((app, mm) => {
                    try {
                        app.TagItems.Add(mm);
                        return null;
                    } catch (Exception ex) {
                        return ex;
                    }
                });
                errors.Add(exc);
            });
            saver(AppContext.AppInstance);

            //TagItems Mock
            SMailMock.MailTags().Enumerate(m => {
                var exc = m.CommitDatabase((app, mm) => {
                    try {
                        app.TagItems.Add(mm);
                        return null;
                    } catch (Exception ex) {
                        return ex;
                    }
                });
                errors.Add(exc);
            });
            saver(AppContext.AppInstance);

            //TagItems Mock
            SMailMock.Mailbox(login).Enumerate(m => {
                var exc = m.CommitDatabase((app, mm) => {
                    try {
                        app.MailEntries.Add(mm);
                        return null;
                    } catch (Exception ex) {
                        return ex;
                    }
                });
                errors.Add(exc);
            });
            saver(AppContext.AppInstance);

            //FileTypes Mock
            SFileMock.FileTypes().Enumerate(m => {
                var exc = m.CommitDatabase((app, mm) => {
                    try {
                        app.FileTypes.Add(mm);
                        return null;
                    } catch (Exception ex) {
                        return ex;
                    }
                });
                errors.Add(exc);
            });
            saver(AppContext.AppInstance);

            //FileTypes Mock
            SFileMock.FileFolderItems(login).Enumerate(m => {
                var exc = m.CommitDatabase((app, mm) => {
                    try {
                        app.FileFolderItems.Add(mm);
                        return null;
                    } catch (Exception ex) {
                        return ex;
                    }
                });
                errors.Add(exc);
            });
            saver(AppContext.AppInstance);

            //FileItems Mock
            SFileMock.FileItems(login).Enumerate(m => {
                var exc = m.CommitDatabase((app, mm) => {
                    try {
                        app.FileItems.Add(mm);
                        return null;
                    } catch (Exception ex) {
                        return ex;
                    }
                });
                errors.Add(exc);
            });
            saver(AppContext.AppInstance);


            return errors;
        }

    }
}