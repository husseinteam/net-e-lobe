/****** Script for SelectTopNRows command from SSMS  ******/
SELECT  LM.ID AS 'LoginModelID', LM.Email AS 'LoginEmail', *
  FROM [ondb].[dbo].[ProfileItems] PI
  LEFT OUTER JOIN [ondb].[dbo].[PhotoItems] PH ON PH.ID = PI.AvatarID
  LEFT OUTER JOIN [ondb].[dbo].[ActivityContainers] AC ON PI.ID = AC.ProfileID
  LEFT OUTER JOIN [ondb].[dbo].[LoginModels] LM ON PI.ID = LM.AssociatedProfileID
  WHERE AC.ProfileID IS NOT NULL