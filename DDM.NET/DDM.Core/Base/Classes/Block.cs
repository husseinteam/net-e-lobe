﻿using System.Collections.Generic;
using DDM.Core.Base.Cells;

namespace DDM.Core.Base.Classes
{
    public class Block
    {
        public Block()
        {
            InitializeComponents();
        }

        private void InitializeComponents()
        {
            _parent = null;
            _childs = new List<Block>();
            _depth = -1;
        }

        private Block _parent;
        private List<Block> _childs;   //nested design.
        private Cell _startCell;    //blocks start Cell
        private Cell _endCell;      //blocks end Cell
        private int _depth;


        internal int Depth
        {
            get { return _depth; }
            set { _depth = value; }
        }
        internal Cell EndCell
        {
            get { return _endCell; }
            set { _endCell = value; }
        }
        internal Cell StartCell
        {
            get { return _startCell; }
            set { _startCell = value; }
        }
        public List<Block> Childs
        {
            get { return _childs; }
            set { _childs = value; }
        }
        public Block Parent
        {
            get { return _parent; }
            set { _parent = value; }
        }

    }
}
