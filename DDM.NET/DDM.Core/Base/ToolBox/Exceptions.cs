﻿using System;

namespace DDM.Core.Base.ToolBox
{
    internal class InvalidDelimeterStringException : Exception
    {
        public InvalidDelimeterStringException(string message)
            : base(message)
        {
        }
        public InvalidDelimeterStringException()
        {
        }
    }

    internal class DefectTypeIsNullException : Exception
    {
        public DefectTypeIsNullException(string message)
            : base(message)
        {
        }
        public DefectTypeIsNullException()
        {
        }
    }

    internal class NonBorderCellBalancedException : Exception
    {
        public NonBorderCellBalancedException(string message)
            : base(message)
        {
        }
        public NonBorderCellBalancedException()
        {
        }
    }

    internal class InvalidStartCellInSequenceException : Exception
    {
        public InvalidStartCellInSequenceException(string message)
            : base(message)
        {
        }
        public InvalidStartCellInSequenceException()
        {
        }
    }

    internal class UpdateAlreadyStartedException : Exception
    {
        public UpdateAlreadyStartedException(string message)
            : base(message)
        {
        }
        public UpdateAlreadyStartedException()
        {
        }
    }

    internal class EndedUpdateReEndedException : Exception
    {
        public EndedUpdateReEndedException(string message)
            : base(message)
        {
        }
        public EndedUpdateReEndedException()
        {
        }
    }

    internal class DeficientDocParseException : Exception
    {
        public DeficientDocParseException(string message)
            : base(message)
        {
        }
        public DeficientDocParseException()
            : base("You Can Not Parse Deficient Documents!!.")
        {
        }
    }

    internal class BaseClassTemplateNotOverriddenException : Exception
    {
        public BaseClassTemplateNotOverriddenException(string message)
            : base(message)
        {
        }
        public BaseClassTemplateNotOverriddenException()
            : base("You didn't override InitializeComponents().")
        {
        }
    }
    
    internal class IrregularCellNotClosedException : Exception
    {
        public IrregularCellNotClosedException(string message)
            : base(message)
        {
        }
        public IrregularCellNotClosedException()
            : base("Irregular Cell Not Closed.")
        {
        }
    }
    internal class NoNextBorderCellException : Exception
    {
        public NoNextBorderCellException(string message)
            : base(message)
        {
        }
        public NoNextBorderCellException()
            : base("Next Border Not Found.")
        {
        }
    }
    internal class NoPreviousBorderCellException : Exception
    {
        public NoPreviousBorderCellException(string message)
            : base(message)
        {
        }
        public NoPreviousBorderCellException()
            : base("Previous Border Not Found.")
        {
        }
    }
    internal class CorpusNotSetException : Exception
    {
        public CorpusNotSetException(string message)
            : base(message)
        {
        }
        public CorpusNotSetException()
            : base("You must set Corpus before filling DDM.Core document.")
        {
        }
    }
    internal class RootBlockNotSetException : Exception
    {
        public RootBlockNotSetException(string message)
            : base(message)
        {
        }
        public RootBlockNotSetException()
            : base("You must set rootBlock before browsing in structure.")
        {
        }
    }
}
