﻿using System.Collections.Generic;
using DDM.Core.Base.ToolBox;

namespace DDM.Core.Base.Settings
{
    internal static class Delims
    {
        static Delims()
        {
            SetDelimeters();
            Irregulars = irregularString.Split(';');
            CellBeginEndSeperator = "@";
            AttrNameValueSeperator = "=";
        }

        internal static StandartDelimeter Standart = new StandartDelimeter();
        internal static List<Delimeter> Extra = new List<Delimeter>();
        internal static Delimeter[] Borders = new Delimeter[2];
        internal static string[] Irregulars { get; set; }
        internal static string CellBeginEndSeperator { get; set; }
        internal static string AttrNameValueSeperator { get; set; }

        internal static Delimeter GetDelimeterByType(string type)
        {
            foreach (Delimeter delim in Extra)
            {
                if (delim.Type == type)
                    return delim;
            }
            return null;
        }

        //begin, end, type:
        private static string delimsString = @"<,>,Standart;,,Open;/,,Close;,/,Single;!--,--,Comment;![CDATA[*,*]],CDATA;!DOCTYPE,,DOCTYPE";
        private static string irregularString = "script;style";
        private static void SetDelimeters()
        {
            //Standart.Begin = "<";
            //Standart.End = ">";
            //Standart.Type = "Standart";
            string[] delims = delimsString.Split(';');
            string[] delim = new string[3];
            Delimeter tempDelim = new Delimeter();
            for (int i = 0; i < delims.Length; i++)
            {
                if (delimsString.Split(';')[i].Split(',').Length != 3)
                    throw new InvalidDelimeterStringException("Delimeter String is Invalid.");
                if (i == 0)
                {
                    delim = delims[i].Split(',');
                    Standart.Begin = delim[0];
                    Standart.End = delim[1];
                    Standart.Type = delim[2];
                    continue;

                }
                else if (i <= 2)
                {
                    tempDelim = new Delimeter();
                    delim = delims[i].Split(',');
                    tempDelim.Begin = delim[0];
                    tempDelim.End = delim[1];
                    tempDelim.Type = delim[2];
                    Borders[i - 1] = tempDelim;
                    continue;
                }
                tempDelim = new Delimeter();
                delim = delims[i].Split(',');
                tempDelim.Begin = delim[0];
                tempDelim.End = delim[1];
                tempDelim.Type = delim[2];
                Extra.Add(tempDelim);
            }
        }
    }
}
