﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;

namespace WF.POC.Console.Xamlx {

	public sealed class GenerateNumberCodeActivity : CodeActivity<Int32> {
		public InArgument<Int32> TypedNumber { get; set; }
		public InArgument<Int32> upperLimit { get; set; }
		public InArgument<Int32> lowerLimit { get; set; }

		// If your activity returns a value, derive from CodeActivity<TResult>
		// and return the value from the Execute method.
		protected override Int32 Execute(CodeActivityContext context) {
			// Obtain the runtime value of the Text input argument
			var typedNumber = context.GetValue(this.TypedNumber);
			var upperLimit = context.GetValue(this.upperLimit);
			var lowerLimit = context.GetValue(this.lowerLimit);

			return new Random().Next(lowerLimit, upperLimit);
		}
	}
}
