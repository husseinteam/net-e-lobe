﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PharmaTect.Model.POCO {
	public class Role : BaseModel {

		#region Properties

		public String Name { get; set; }
		public Boolean IsActive { get; set; }
        public EModuleType ModuleType { get; set; }

		#endregion

		#region Navigation Properties

		public virtual ICollection<RoleFunction> RoleFunctions { get; set; }
		public virtual ICollection<UserRoleMembership> UserRoleMemberships { get; set; }


        #endregion



    }
}
