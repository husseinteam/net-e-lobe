﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model.POCO {
	public class User : BaseModel {

		#region Properties

		public String UserName { get; set; }
		public String Password { get; set; }
		public String FullName { get; set; }
		public String EMail { get; set; }
		public String Description { get; set; }
		public EMembershipStatus MembershipStatus { get; set; }
		public Boolean PasswordNeverExpires { get; set; }
		public Boolean MustChangePassword { get; set; }
		public Boolean CanChangePassword { get; set; }
		public String DefaultCulture { get; set; }
		public Boolean IsActive { get; set; }
		public DateTime LastAccessTime { get; set; }
		public DateTime PasswordValidityDate { get; set; }
		public DateTime PasswordChangeOn { get; set; }
		public String LastAccessIP { get; set; }
		public Int32 PasswordChangeBy { get; set; }

		public String SecurityQuestion { get; set; }
		public String SecurityAnswer { get; set; }
		public Int32 IncorrectTrialCount { get; set; }
		public DateTime LastIncorrectAccessDate { get; set; }
		public DateTime AccountActivationDate { get; set; }

        #endregion

        #region Navigation Properties

        public virtual ICollection<UserRoleMembership> UserRoleMemberships { get; set; }

        #endregion


    }
}
