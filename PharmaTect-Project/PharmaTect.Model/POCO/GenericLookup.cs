﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model.POCO
{
    public class GenericLookup : BaseModel
    {

        #region Properties

        public bool IsActive { get; set; }
        public string ShortCode { get; set; }
        public int Order { get; set; }
        public string OptionName { get; set; }
        public string Description { get; set; }

        #endregion

        #region Navigation Properties

        public virtual GenericCodeType GenericCodeType { get; set; }
        public virtual GenericLookup ParentGenericLookup { get; set; }

        #endregion
    }
}
