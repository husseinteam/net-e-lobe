﻿using PharmaTect.Model.POCO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model {
	public class RoleMapping : BaseMapping<Role> {

		#region Ctors
		public RoleMapping() {

			#region PropertiesMapping

			this.Property(t => t.Name).HasMaxLength(128)
				.HasColumnName("Name").IsRequired();

            this.Property(t => t.IsActive)
                .HasColumnName("IsActive").IsRequired();

            this.Property(t => t.ModuleType)
                .HasColumnName("ModuleType").IsRequired();

            #endregion

            this.ToTable("Role", "Infrastructure");

		}

		#endregion

	}
}

