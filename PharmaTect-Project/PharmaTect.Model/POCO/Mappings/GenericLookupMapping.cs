﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model.POCO.Mappings
{
    public class GenericLookupMapping : BaseMapping<GenericLookup>
    {
        public GenericLookupMapping()
        {
            Property(x => x.IsActive).HasColumnName("IsActive").IsOptional();
            Property(x => x.ShortCode).HasColumnName("ShortCode").IsOptional();
            Property(x => x.Order).HasColumnName("Order").IsOptional();
            Property(x => x.OptionName).HasColumnName("OptionName").IsOptional();
            Property(x => x.Description).HasColumnName("Description").IsOptional();

            #region Navigation Properties

            this.HasOptional(t => t.ParentGenericLookup)
                .WithOptionalDependent()
                .Map(x => x.MapKey("ParentGenericLookupID"));

            this.HasRequired(t => t.GenericCodeType)
                .WithMany(t => t.GenericLookups)
                .Map(x => x.MapKey("GenericCodeTypeID"));

            #endregion

            this.ToTable("GenericLookup", "Infrastructure");
        }
    }
}
