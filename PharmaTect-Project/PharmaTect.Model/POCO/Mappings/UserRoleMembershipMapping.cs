﻿using PharmaTect.Model.POCO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model {
    public class UserRoleMembershipMapping : BaseMapping<UserRoleMembership> {

        #region Ctors
        public UserRoleMembershipMapping() {

            #region Properties

            this.Property(t => t.StartDate)
                .HasColumnName("StartDate").IsOptional().HasColumnType("datetime2");

            this.Property(t => t.EndDate)
                .HasColumnName("EndDate").IsOptional().HasColumnType("datetime2");

            #endregion

            #region NavigationMapping

            this.HasRequired(t => t.User)
                .WithMany(t => t.UserRoleMemberships)
                .Map(x => x.MapKey("UserID"));

            this.HasRequired(t => t.Role)
                .WithMany(t => t.UserRoleMemberships)
                .Map(x => x.MapKey("RoleID"));

            #endregion

            this.ToTable("UserRoleMembership", "Infrastructure");

        }

        #endregion

    }
}

