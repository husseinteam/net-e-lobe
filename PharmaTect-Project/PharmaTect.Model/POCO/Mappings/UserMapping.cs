﻿using PharmaTect.Model.POCO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model {
	public class UserMapping : BaseMapping<User> {

		#region Ctors
		public UserMapping() {

            #region PropertiesMapping

            this.Property(t => t.UserName).HasMaxLength(128)
                .HasColumnName("UserName");

            this.Property(t => t.Password).HasMaxLength(128)
                .HasColumnName("Password");

            this.Property(t => t.FullName).HasMaxLength(256)
                .HasColumnName("FullName").IsRequired();

            this.Property(t => t.EMail).HasMaxLength(256)
				.HasColumnName("EMail").IsRequired();

            this.Property(t => t.Description).HasMaxLength(256)
                .HasColumnName("Description").IsOptional();

            this.Property(t => t.MembershipStatus)
                .HasColumnName("MembershipStatus").IsOptional();

            this.Property(t => t.CanChangePassword)
               .HasColumnName("CanChangePassword").IsRequired();

            this.Property(t => t.DefaultCulture).HasMaxLength(32)
                .HasColumnName("DefaultCulture").IsOptional();

            this.Property(t => t.IncorrectTrialCount)
				.HasColumnName("IncorrectTrialCount").IsRequired();

			this.Property(t => t.IsActive)
				.HasColumnName("IsActive").IsRequired();

			this.Property(t => t.LastAccessIP).HasMaxLength(32)
				.HasColumnName("LastAccessIP").IsOptional();

			this.Property(t => t.LastAccessTime)
				.HasColumnName("LastAccessTime")
				.IsOptional().HasColumnType("datetime2");
			
			this.Property(t => t.LastIncorrectAccessDate)
				.HasColumnName("LastIncorrectAccessDate")
				.IsOptional().HasColumnType("datetime2");

			this.Property(t => t.PasswordChangeBy)
				.HasColumnName("PasswordChangeBy");

			this.Property(t => t.PasswordChangeOn)
				.HasColumnName("PasswordChangeOn")
				.IsOptional().HasColumnType("datetime2");

			this.Property(t => t.PasswordNeverExpires)
				.HasColumnName("PasswordNeverExpires");

			this.Property(t => t.PasswordValidityDate)
				.HasColumnName("PasswordValidityDate")
				.IsOptional().HasColumnType("datetime2");

			this.Property(t => t.SecurityAnswer).HasMaxLength(256)
				.HasColumnName("SecurityAnswer");

			this.Property(t => t.SecurityQuestion).HasMaxLength(256)
				.HasColumnName("SecurityQuestion");

			this.Property(t => t.AccountActivationDate)
				.HasColumnName("AccountActivationDate")
				.IsOptional().HasColumnType("datetime2");

            #endregion

            this.ToTable("User", "Infrastructure");

		}

		#endregion

	}
}

			