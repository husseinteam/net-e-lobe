﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace PharmaTect.Model {
	public enum EUnitType {

		[Display(Description = "Kurum Başkanlığı")]
		KurumBaskanligi = 1,
		[Display(Description = "Kurum Başkan Yardımcılığı")]
		KurumBaskanYardımciligi,
		[Display(Description = "Daire Başkanlığı")]
		DaireBaskanligi,
		[Display(Description = "Koordinatörlük")]
		Koordinatorluk,
		[Display(Description = "Birim Başkanlığı")]
		BirimBaskanligi,
		[Display(Description = "Birim Sorumluluğu")]
		BirimSorumlulugu,
		[Display(Description = "Birim Çalışanlığı")]
		BirimCalisanligi

	}
}
