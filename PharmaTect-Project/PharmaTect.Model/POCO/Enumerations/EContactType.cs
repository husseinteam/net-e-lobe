﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace PharmaTect.Model {
	public enum EContactType {

		[Display(Description = "E-Posta")]
		EMail = 1,
		[Display(Description = "Fax Numarası")]
		FaxNumber,
		[Display(Description = "Cep Telefonu")]
		CellPhoneNumber,
		[Display(Description = "Sabit Telefon")]
		FixedPhone,
		[Display(Description = "Ev Adresi")]
		HomeAddress,
		[Display(Description = "İş Adresi")]
		WorkAddress,
		[Display(Description = "Web Sitesi")]
		WebSite


	}
}
