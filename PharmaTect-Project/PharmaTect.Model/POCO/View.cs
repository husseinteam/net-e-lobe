﻿using PharmaTect.Model.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Model.POCO {
	public class View : BaseModel {

		#region Properties

		public String Name { get; set; }
		public EUriType UriType { get; set; }
		public String Uri { get; set; }
		public Int32 Width { get; set; }
		public Int32 Height { get; set; }
		public Int32 Order { get; set; }
		public String Code { get; set; }
		public ETarget Target { get; set; }
		public Boolean ShowInMenu { get; set; }
		public Boolean IsDefaultView { get; set; }
		public EViewCategory ViewCategory { get; set; }

		#endregion

		#region Navigation Properties

		public virtual View Parent { get; set; }
        public virtual Function DefaultFunction { get; set; }

		#endregion



	}
}
