﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.Entity;
using PharmaTect.Core;
using PharmaTect.Model;
using PharmaTect.Core.Extensions;

namespace PharmaTect.UnitTest.ModelTests {
	[TestClass]
	public class GenericTest {
		[TestMethod]
		public void ModelGeneratedSuccessfully() {

			using (var context = new PharmaTectContext()) {
				SDatabaseService.InitializeDatabase(context);
				SDatabaseService.Seed(context);
				Assert.AreNotEqual(0, context.Users.Magnitude());
			}

		}
	}
}
