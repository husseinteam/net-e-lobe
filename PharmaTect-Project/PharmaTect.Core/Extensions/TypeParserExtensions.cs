﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PharmaTect.Core.Extensions {
    public static class TypeParserExtensions {

        public static TItem ParseInto<TItem>(this Object obj) {
            dynamic instance = obj;
            if (instance.GetType().GetInterface("IConvertible") == null || typeof(TItem).Equals(typeof(ValueType))) {
                return instance != null ? (TItem)instance : default(TItem);
            } else {
                if (instance.GetType().IsEnum) {
                    return (TItem)Enum.Parse(instance.GetType(), instance.ToString());
                } else {
                    var conv = TypeDescriptor.GetConverter(typeof(TItem));
                    if (conv.CanConvertFrom(instance.GetType())) {
                        return (TItem)conv.ConvertFrom(instance);
                    } else {
                        return default(TItem);
                    }
                }
            }
        }

    }
}
