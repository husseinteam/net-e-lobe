﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CourseAct.Model.Script {
    public interface IDrawing {
        String Tooltip { get; set; }
        byte[] Data { get; set; }
    }
}
