﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CourseAct.Model.Script {
    public interface ILanguage {
        String Description { get; set; }
        String CountryCode { get; set; }
        String RegionCode { get; set; }
    }
}
