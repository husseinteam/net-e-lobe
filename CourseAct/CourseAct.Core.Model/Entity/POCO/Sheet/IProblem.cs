﻿using CourseAct.Model.Script;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CourseAct.Model.Case;


namespace CourseAct.Model.Sheet {
    public interface IProblem {
        IScripture Query { get; set; }
        ICollection<IOption> Options { get; set; }

        IWorksheet Worksheet { get; set; }
    }
}
