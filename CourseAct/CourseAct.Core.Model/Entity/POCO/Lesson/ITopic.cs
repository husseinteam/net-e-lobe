﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CourseAct.Model.Lesson {
    public interface ITopic{
        ILesson ParentLecture { get; set; }
        
        String Title { get; set; }
        ELevel Level { get; set; }
        ITopic ParentTopic { get; set; }
    }
}
