﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CourseAct.Model.User {
    public interface IDeveloper : IClient {
        ICollection<IInstitution> Institutions { get; set; }
    }
}
