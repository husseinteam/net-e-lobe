﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace CourseAct.Model.User.Client {
    public interface ICredentialInfo {
        String Logon { get; set; }
        byte[] Cipher { get; set; }
        byte[] Key { get; set; }
        byte[] IV { get; set; }
    }
}
