﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CourseAct.Model.Sheet;
using CourseAct.Model.Department;

namespace CourseAct.Model.Case {
    public interface IQuiz : IWorksheet {
        IGrade Grade { get; set; }
    }
}
