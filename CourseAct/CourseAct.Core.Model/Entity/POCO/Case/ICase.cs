﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CourseAct.Model.Department;

using CourseAct.Model.Sheet;
using CourseAct.Model.User;


namespace CourseAct.Model.Case {
    public interface ICase<TClient, TSheet>
        where TSheet : IWorksheet
        where TClient : IClient {
        DateTime DateTaken { get; set; }
        ICollection<TClient> Clients { get; set; }
        ICollection<TSheet> Sheets { get; set; }
        IClassRoom ClassRoom { get; set; }
    }
}
