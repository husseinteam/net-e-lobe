﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CourseAct.Model.User;

namespace CourseAct.Model.Department {
    public interface IDepartment{
        IDepartmentManager DepartmentManager { get; set; }
        ICollection<IClassRoom> ClassRooms { get; set; }
        ITitle Title { get; set; }
       
        IDepartment ParentDepartment { get; set; }
    }
}
