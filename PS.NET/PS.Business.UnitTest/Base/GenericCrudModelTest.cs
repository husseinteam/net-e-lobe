﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PS.Business.Controller.Base;
using PS.Business.Extensions.Instance;
using PS.Business.Model.Base;
using PS.Core.Model.Entity.Base;
using PS.Emitter.Ligand.Emit;

namespace PS.Business.UnitTest.Base {

    public abstract class GenericCrudTest<TEntity>
        where TEntity : BaseModel {

        protected async Task<Dictionary<String, Boolean>> CrudSuite() {

            var controller = SLigand.Attach<BaseController<TEntity>>();

            //Reset Table Data
            await controller.ResetTable();

            var repr = controller.Representative;

            //Insert
            var s = await controller.Insert(repr);


            var insertSucceeded = repr.InstanceEqual(s);

            //Update
            var u = await controller.Update(UpdatedEntity(s));

            var one = await controller.ForKey(u.ID);
            var updateSuccessful = one.InstanceEqual(UpdatedEntity(u));

            //Delete
            await controller.Remove(one);
            var that = await controller.ForKey(u.ID);
            var deleteSuccessful = that == null;

            var report = new Dictionary<String, Boolean>()
                {
                    { "Insert Not Successful", insertSucceeded },
                    { "Update Not Successful", updateSuccessful },
                    { "Delete Not Successful", deleteSuccessful }
                };

            //Reload
            await controller.SeedDatabase();

            return report;
        }

        protected abstract TEntity UpdatedEntity(TEntity seededEntity);

    }
}
