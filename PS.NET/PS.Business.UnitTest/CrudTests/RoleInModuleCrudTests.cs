﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PS.Business.UnitTest.Base;
using PS.Core.Model.Entity.POCO;

namespace PS.Business.UnitTest.CrudTests {
    [TestClass]
    public class RoleInModuleCrudTests : GenericCrudTest<RoleInModule> {

        [TestMethod]
        public async Task CrudModuleWorksProperly() {

            var dict = await base.CrudSuite();
            foreach (var kv in dict) {
                Assert.IsTrue(kv.Value, kv.Key);
            }

        }

        protected override RoleInModule UpdatedEntity(RoleInModule seededEntity) {

            seededEntity.IsDeleted = true;
            return seededEntity;

        }

    }
}
