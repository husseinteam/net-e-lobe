﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PS.Business.Extensions.Instance;

namespace PS.Business.UnitTest.ExtensionTests {
    [TestClass]
    public class InstanceTests {
        [TestMethod]
        public void InstanceEqualRunsProperly() {

            var elements = new List<MyElement>(
                    new MyElement[] {
                        new MyElement()
                        {
                            MyElementProperty = "String1"
                        }
                        });

            var instance1 = new MyClass()
            {
                MyInt = 1,
                MyString = "One",
                MyElements = elements
            };
            var instance2 = new MyClass()
            {
                MyInt = 2,
                MyString = "One"
            };
            Assert.IsFalse(instance1.InstanceEqual(instance2));

            instance2.MyInt = 1;
            Assert.IsFalse(instance1.InstanceEqual(instance2));

            instance2.MyInt = 1;
            instance2.MyElements = elements;

            Assert.IsTrue(instance1.InstanceEqual(instance2));
        }
    }

    class MyClass {
        public Int32 MyInt { get; set; }
        public String MyString { get; set; }
        public virtual ICollection<MyElement> MyElements { get; set; }
    }

    public class MyElement {

        public String MyElementProperty { get; set; }

    }
}

