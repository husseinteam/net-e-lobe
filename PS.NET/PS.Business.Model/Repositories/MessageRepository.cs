﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PS.Business.Model.Base;
using PS.Core.Extensions.ParserExtensions;
using PS.Core.Model.Entity.Enum;
using PS.Core.Model.Entity.POCO;

namespace PS.Business.Model.Repositories {
    internal class MessageRepository : GenericRepository<Message>{

        public MessageRepository() {
            var r = new Random();
            var users = new UserRepository().Seed();
            var texts = new TextRepository().Seed();

            base.SetSeeder(index => new Message()
            {
                ID = index,
                IsDeleted = false,
                Receiver = users.ElementAt(r.Next(N.ParseToInt32())),
                Sender = users.ElementAt(r.Next(N.ParseToInt32())),
                Texts = new List<Text>(texts.Take(index.ParseToInt32())),
                PostDate = DateTime.Now,
                DelayKind = EDelayKind.FromDays,
                DelayValue = 10
            });
        }

    }
}
