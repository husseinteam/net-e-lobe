﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PS.Business.Model.Base;
using PS.Core.Model.Entity.POCO;

namespace PS.Business.Model.Repositories {
    internal class RoleRepository : GenericRepository<Role> {

        public RoleRepository() {

            base.SetSeeder(index =>
                new Role()
                {
                    ID = index,
                    Code = "code-" + index,
                    IsDeleted = false
                }
            );

        }

    }
}
