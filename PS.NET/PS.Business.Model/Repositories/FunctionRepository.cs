﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PS.Business.Model.Base;
using PS.Core.Extensions.ParserExtensions;
using PS.Core.Model.Entity.Enum;
using PS.Core.Model.Entity.POCO;

namespace PS.Business.Model.Repositories {
    internal class FunctionRepository : GenericRepository<Function> {

        public FunctionRepository() {

            base.SetSeeder(index => new Function()
            {
                ID = index,
                IsDeleted = false,
                Code = "code-" + index,
                Description = "description-" + index
            });

        }

    }
}
