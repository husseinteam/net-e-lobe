﻿using PS.Core.Model.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using PS.Core.Extensions.ParserExtensions;
using System.Linq.Expressions;
using PS.Business.Extensions.Data;
using PS.Core.Model.Seed;
using System.Data.Entity.Migrations;
using PS.Emitter.Markups.Attributes;
using PS.Core.Model.Entity.Base;

namespace PS.Business.Model.Base {
    [Omit]
    public abstract class GenericRepository<TEntity> : BaseSeeder<TEntity>, IRepository<TEntity>
        where TEntity : BaseModel {

        #region Properties

        private PSDataContext DataContext = new PSDataContext();
        private TEntity _Entity;

        #endregion

        #region Implementation

        public async Task<TEntity> Insert() {

            DataContext.Set<TEntity>().Add(_Entity);
            await DataContext.SaveChangesAsync();
            return _Entity;

        }

        public IRepository<TEntity> With(TEntity entity) {

            _Entity = entity;
            return this;

        }

        public async Task Delete() {

            var entity = await One();
            DataContext.Set<TEntity>().Remove(entity ?? _Entity);
            await DataContext.SaveChangesAsync();

        }

        public async Task<TEntity> Update() {

            var entry = DataContext.Entry<TEntity>(_Entity);
            if (entry.State == EntityState.Detached) {
                DataContext.Set<TEntity>().Attach(_Entity);
            }
            entry.State = EntityState.Modified;
            await DataContext.SaveChangesAsync();
            return _Entity;

        }

        public async Task<IEnumerable<TEntity>> All() {

            IEnumerable<TEntity> all = null;
            if (_Filter == null) {
                all = await DataContext.Set<TEntity>().ToListAsync();
            } else {
                all = await DataContext.Set<TEntity>()
                 .Where(_Filter)
                 .ToListAsync();
                _Filter = null;
            }
            return all.AsEnumerable();
        }

        public async Task<TEntity> One() {

            var found = await DataContext.Set<TEntity>()
               .FindAsync(_Key.ParseToInt32());
            _Key = default(Int64);
            return found;

        }

        public IRepository<TEntity> AddCollection<TElement>(
            Expression<Func<TEntity, ICollection<TElement>>> query)
            where TElement : class {

            DataContext.Collection(query);
            return this;

        }

        public IRepository<TEntity> AddReference<TElement>(
            Expression<Func<TEntity, TElement>> query)
            where TElement : class {

            DataContext.Reference(query);
            return this;

        }

        public IRepository<TEntity> Key(Int64 key) {

            _Key = key;
            return this;

        }
        public IRepository<TEntity> Filter(Expression<Func<TEntity, Boolean>> filter) {

            _Filter = filter;
            return this;

        }

        private Int64 _Key;
        private Expression<Func<TEntity, bool>> _Filter;


        #endregion


    }
}
