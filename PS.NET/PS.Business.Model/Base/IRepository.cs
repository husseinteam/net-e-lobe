﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using PS.Core.Model.Seed;

namespace PS.Business.Model.Base {
    public interface IRepository<TEntity> : ISeeder<TEntity>
        where TEntity : class {

        IRepository<TEntity> Key(Int64 key);
        IRepository<TEntity> Filter(Expression<Func<TEntity, Boolean>> filter);

        IRepository<TEntity> With(TEntity entity);

        Task Delete();
        Task<TEntity> Insert();
        Task<TEntity> Update();

        Task<TEntity> One();
        Task<IEnumerable<TEntity>> All();

        IRepository<TEntity> AddCollection<TElement>(
            Expression<Func<TEntity, ICollection<TElement>>> query)
            where TElement : class;

        IRepository<TEntity> AddReference<TElement>(
            Expression<Func<TEntity, TElement>> query)
            where TElement : class;

    }
}
