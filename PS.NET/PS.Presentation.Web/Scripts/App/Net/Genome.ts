class Code<T> {

	code: string;
	decode: T;

	constructor() {

	}

	static Transform(code: string) {
		if (code.indexOf('-') > -1) {
			return code.split('-').map(c=> {
				c = c[0].toUpperCase() + c.slice(1).toLowerCase();
				return c;
			}).reduce((soFar, c) => soFar + c);
		} else {
			return code;
		}
	}

	static Construct<TCode>(code: string, decode: TCode) {
		return new Code<TCode>().assign(code, decode);
	}

	assign(code: string, decode: T) {

		var c = Code.Transform(code);

		if (c in this) {
			this[c] = decode;
		} else {
			Object.defineProperty(this, c, { value: decode, writable: true, configurable: true });
		}
		this.code = c;
		this.decode = decode;
		return this;

	}

	equals(c: Code<T>) {
		return this.decode === c.decode;
	}
}

class Genome<T> {

	private _sequence: Array<Code<T>>;
	private count: number;
	private max: number;
	private indeedSubscriber: (genome: Genome<T>) => any;

	constructor() {

		this._sequence = new Array<Code<T>>();
		this.count = 0;
		this.max = -1;
	}

	Swap(code: string, decode: T) {

		var found = this._sequence.filter(c=> c.code === code);
		if (found.length > 0) {
			found[0].assign(code, decode);
		} else {
			this.Add(new Code<T>().assign(code, decode));
		}
		return this;

	}

	Take(code: string, that: (c: Code<T>) => void) {

		var cd = new Code<T>().assign(code, undefined);
		var found = this._sequence.filter(c=> c.code === cd.code);

		if (found.length > 0) {
			that(found[0]);
		} else {
			this.Add(new Code<T>().assign(code, undefined));
			that(this._sequence.filter(c=> c.code === cd.code)[0]);
		}
		return this;

	}

	ForEach(that: (code: Code<T>, index?: number, genome?: Genome<T>) => void) {

		this._sequence.forEach((c, i, a) => that(c, i, this));
		return this;

	}

	Remove(code: Code<T>) {
		var i;
		if ((i = this._sequence.indexOf(code)) > -1) {
			this._sequence = this._sequence.slice(0, i).concat(this._sequence.slice(i + 1));
		}

	}

	Magnitude(which: (code: Code<T>) => boolean) {

		return this._sequence.filter(which).length;

	}

	Last(which?: (code: Code<T>) => boolean) {
		var list: Array<Code<T>>;
		if (which) {
			list = this._sequence.filter(which);
		} else {
			list = this._sequence;
		}
		return list[list.length - 1];
	}

	SubscribeIndeeder(max: number, subscriber: (genome: Genome<T>) => any) {

		this.max = max;
		this.indeedSubscriber = subscriber;
		return this;

	}

	Indeed() {

		this.count += 1;
		if (this.count === this.max) {
			this.indeedSubscriber(this);
		}
		return this;

	}

	Add(code: Code<T>) {

		var found: Array<Code<T>>;
		if ((found = this._sequence.filter(c=> c.code === code.code)).length > 0) {
			var idx = this._sequence.indexOf(found[0]);
			this._sequence[idx] = code;
		} else {
			this._sequence.push(code);
		}
		return this;

	}

	Extend(genome: Genome<T>) {

		for (var i = 0; i < genome._sequence.length; ++i) {
			this.Add(genome._sequence[i]);
		}
		return this;

	}

	get Width() {

		return this._sequence.length;

	}

	toJSONString() {

		return JSON.stringify(this.toJSONObject());

	}

	toJSONObject() {

		var flat = {};
		this._sequence.forEach(c=> flat[c.code] = c.decode);
		return { data: flat };

	}

}