
class KeyFrame {

    constructor(public percent: number, public patterns: string[]) { }

    toString(designator: string) {

        var transformList = this.patterns.filter(p=> p.indexOf(':') === -1);
        var normalList = this.patterns.filter(p=> p.indexOf(':') !== -1);

        var transformstring = transformList.length > 0 ? (transformList
            .reduce((prev, cur) => prev + ' ' + cur, designator + 'transform: ') + ';') : '';
        var normalString = normalList.length > 0 ? normalList.map(p => p + '; ')
            .reduce((prev, cur) => prev + ' ' + cur, ' ') : '';

        return ' ' + this.percent + '% { ' + transformstring + normalString + ' } ';

    }

}
class AnimeConfig {

    constructor(public ease: string, public fillmode: string, public delay: number, public transition: number) { }

}
class Anime {

	private keyframes: Array<KeyFrame>
    static Config: AnimeConfig;

    constructor(private name: string) {

        Anime.Config = new AnimeConfig('ease-in-out', 'forwards', 0, App.Config.TransitionLength);
        this.keyframes = new Array<KeyFrame>();

    }

    keyframe(percent: number, patterns: string[]) {

        var kfs: Array<KeyFrame>;
        if ((kfs = this.keyframes.filter(kf=> kf.percent === percent)).length > 0) {
            kfs[0].patterns = patterns;
        } else {
            this.keyframes.push(new KeyFrame(percent, patterns));
        }
        return this;

    }

    apply(to: HTMLElement, config?: AnimeConfig) {

        var cfg = config || Anime.Config;
        var added;
        var id = '__ll_keyframe_' + this.name;
        var style = document.querySelector('#' + id);
        if (!style) {
            var st = document.createElement("style");
            st.id = id;
            added = document.documentElement.appendChild(st);
        } else {
            added = style;
        }

        added.innerText = this.toString('keyframes') + '  ' + this.toString('-moz-keyframes') + '  ' + this.toString('-webkit-keyframes');
        to.style.animation = to.style.webkitAnimation = to.style['webkitAnimation'] =
        this.name + ' ' + cfg.transition + 's ' + cfg.fillmode + ' ' + cfg.ease + ' ' + cfg.delay + 's';
        return this;

    }

    monaic(to: HTMLElement, pattern: string, config?: AnimeConfig) {

        var cfg = config || Anime.Config;
        to.style.webkitTransition = to.style.transition = to.style['-webkit-transition'] =
        'all ' + cfg.transition + 's ' + cfg.ease + ' ' + cfg.delay + 's';
        to.style.transform = to.style.webkitTransform = to.style['-webkit-transform'] = pattern;

    }

    toString(designator: string) {

        return '@' + designator + ' ' + this.name + ' {' +
            this.keyframes.map((kf) => kf.toString(designator.replace('keyframes', '')))
                .reduce((prev, cur) => prev + ' ' + cur, ' ') + '}';

    }

    static test() {

        //test('Anime toString() should return value', () => {
        //    var a = new Anime('demo');
        //    var str = a.keyframe(0, ['scaleX(1.0) translateX(0px)']).keyframe(50, ['scaleX(0.5) translateX(-305px)'])
        //        .keyframe(100, ['scaleX(0.5) translateX(-305px) translateY(100px)'])
        //        .toString('keyframes');
        //    expect(1);
        //    console.log(str);
        //    equal(str.length, 10, 'toString() returned a value');
        //});

    }
}