var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var App = (function () {
    function App() {
    }
    App.run = function () {
        App.Task.Stream();
        App.Task.Glyph();
        App.Task.SiteMaster();
        App.Task.Playground();
        App.Task.NewItemModal();
        //Test.make();
    };
    Object.defineProperty(App, "Task", {
        get: function () {
            var task = {
                Stream: function () { return MainStream.generate(App.Config).registerFlow(); },
                Glyph: function () { return Glyph.morph(App.Config); },
                SiteMaster: function () { return SiteMaster.ConstructUI(App.Config); },
                Playground: function () { return Playground.ConstructUI(App.Config); },
                NewItemModal: function () { return NewItemModal.ConstructUI(App.Config); },
                Core: {
                    ExtractID: function (from) {
                        var m = from.match(/\d+/g);
                        return m ? m[0] : '';
                    },
                    IDExists: function (path) {
                        return task.Core.ExtractID(path) != '';
                    },
                    Union: function (first, second) {
                        var un = {};
                        for (var e1 in first) {
                            if (first.hasOwnProperty(e1)) {
                                un[e1] = first[e1];
                            }
                        }
                        for (var e2 in second) {
                            if (second.hasOwnProperty(e2)) {
                                un[e2] = second[e2];
                            }
                        }
                        return un;
                    }
                }
            };
            return task;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(App, "Config", {
        get: function () {
            var cfg = {
                TransitionLength: 1.2,
                VirtualDirectory: '/playground/',
                ServiceHosts: {
                    'Generic': function () { return cfg.VirtualDirectory + 'Service/GenericService.asmx'; },
                    'Transform': function () { return cfg.VirtualDirectory + 'Service/TransformService.asmx'; }
                },
                Patterns: {
                    "password": /(.+){8,64}/i,
                    "email": /[a-zA-Z][a-zA-Z0-9\.]*@\w+\.\w{2,4}/i
                },
                Messages: {
                    "locatorMistyped": "Provided email does not meet the required criteria",
                    "passwordMistyped": "Provided password does not meet the required criteria",
                    "HandshakeRejected": "Handshake Rejected",
                    "NotLocated": "Why Not Join Us",
                    "PleaseTryAgain": "Please Try Again",
                },
                Constants: {
                    "LocatorRangeCount": 6,
                    "LocatorSlideCount": 31,
                    "RequestedModel": "Achievement"
                },
                WebFontConfig: {
                    google: { families: ['Marmelad::latin,latin-ext,cyrillic'] }
                },
                Captions: {
                    "RemoveItem": "Delete Item",
                    "DetailsItem": "Details Of",
                    "UpdateItem": "Update Item",
                    "CommandsColumn": "Commands",
                },
                AjaxTransform: function (key, genome, done) {
                    new Asyncll(AsyncMode.jQueryAjax, cfg.ServiceHosts['Transform'](), AsyncMethod.Post)
                        .transmit(genome.Add(Code.Construct('key', key)), 'GetTemplate', done, function (err, genome) { });
                }
            };
            return cfg;
        },
        enumerable: true,
        configurable: true
    });
    return App;
}());
/// <reference path="../../typings/jquery/jquery.d.ts" />
var AsyncMode;
(function (AsyncMode) {
    AsyncMode[AsyncMode["WebSocket"] = 1] = "WebSocket";
    AsyncMode[AsyncMode["jQueryAjax"] = 2] = "jQueryAjax";
})(AsyncMode || (AsyncMode = {}));
var AsyncMethod;
(function (AsyncMethod) {
    AsyncMethod[AsyncMethod["Get"] = 1] = "Get";
    AsyncMethod[AsyncMethod["Post"] = 2] = "Post";
})(AsyncMethod || (AsyncMethod = {}));
var Asyncll = (function () {
    function Asyncll(asyncMode, host, method) {
        if (method === void 0) { method = AsyncMethod.Get; }
        this.asyncMode = asyncMode;
        this.host = host;
        this.method = method;
        if (this.asyncMode == AsyncMode.WebSocket) {
            this.host = "ws://" + document.domain + App.Config.VirtualDirectory + "Hub/Handlers/GenericWebSocketHandler.ashx";
        }
    }
    Asyncll.prototype.transmit = function (genome, action, seeder, check, always) {
        if (this.asyncMode == AsyncMode.WebSocket) {
            var ws = new WebSocket(this.host + '?sw=' + action);
            ws.onerror = function (ev) {
                check(ev, genome);
                always(genome);
            };
            ws.onopen = function (ev) {
                if (ws.readyState === WebSocket.OPEN) {
                    ws.send(genome.toJSONString());
                }
            };
            ws.onmessage = function (ev) {
                seeder(JSON.parse(ev.data));
                always(genome);
            };
        }
        else {
            var jqxhr;
            if (this.method == AsyncMethod.Get) {
                jqxhr = $.ajax({
                    url: this.host + '/' + action,
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    cache: false,
                    success: function (data, textStatus, jqXHR) {
                        seeder(JSON.parse(data.d));
                    },
                    async: true
                });
            }
            else if (this.method == AsyncMethod.Post) {
                jqxhr = $.ajax({
                    url: this.host + '/' + action,
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: genome.toJSONString(),
                    dataType: 'json',
                    cache: false,
                    success: function (data, textStatus, jqXHR) {
                        seeder(JSON.parse(data.d));
                    },
                    async: true
                });
            }
            jqxhr
                .fail(function (jqXHR, textStatus, errorThrown) {
                check(errorThrown, genome);
            })
                .always(function () {
                always && always(genome);
            });
        }
    };
    return Asyncll;
}());
var Test = (function () {
    function Test() {
    }
    Test.make = function () {
        Anime.test();
        Neuron.test();
    };
    return Test;
}());
var Code = (function () {
    function Code() {
    }
    Code.Transform = function (code) {
        if (code.indexOf('-') > -1) {
            return code.split('-').map(function (c) {
                c = c[0].toUpperCase() + c.slice(1).toLowerCase();
                return c;
            }).reduce(function (soFar, c) { return soFar + c; });
        }
        else {
            return code;
        }
    };
    Code.Construct = function (code, decode) {
        return new Code().assign(code, decode);
    };
    Code.prototype.assign = function (code, decode) {
        var c = Code.Transform(code);
        if (c in this) {
            this[c] = decode;
        }
        else {
            Object.defineProperty(this, c, { value: decode, writable: true, configurable: true });
        }
        this.code = c;
        this.decode = decode;
        return this;
    };
    Code.prototype.equals = function (c) {
        return this.decode === c.decode;
    };
    return Code;
}());
var Genome = (function () {
    function Genome() {
        this._sequence = new Array();
        this.count = 0;
        this.max = -1;
    }
    Genome.prototype.Swap = function (code, decode) {
        var found = this._sequence.filter(function (c) { return c.code === code; });
        if (found.length > 0) {
            found[0].assign(code, decode);
        }
        else {
            this.Add(new Code().assign(code, decode));
        }
        return this;
    };
    Genome.prototype.Take = function (code, that) {
        var cd = new Code().assign(code, undefined);
        var found = this._sequence.filter(function (c) { return c.code === cd.code; });
        if (found.length > 0) {
            that(found[0]);
        }
        else {
            this.Add(new Code().assign(code, undefined));
            that(this._sequence.filter(function (c) { return c.code === cd.code; })[0]);
        }
        return this;
    };
    Genome.prototype.ForEach = function (that) {
        var _this = this;
        this._sequence.forEach(function (c, i, a) { return that(c, i, _this); });
        return this;
    };
    Genome.prototype.Remove = function (code) {
        var i;
        if ((i = this._sequence.indexOf(code)) > -1) {
            this._sequence = this._sequence.slice(0, i).concat(this._sequence.slice(i + 1));
        }
    };
    Genome.prototype.Magnitude = function (which) {
        return this._sequence.filter(which).length;
    };
    Genome.prototype.Last = function (which) {
        var list;
        if (which) {
            list = this._sequence.filter(which);
        }
        else {
            list = this._sequence;
        }
        return list[list.length - 1];
    };
    Genome.prototype.SubscribeIndeeder = function (max, subscriber) {
        this.max = max;
        this.indeedSubscriber = subscriber;
        return this;
    };
    Genome.prototype.Indeed = function () {
        this.count += 1;
        if (this.count === this.max) {
            this.indeedSubscriber(this);
        }
        return this;
    };
    Genome.prototype.Add = function (code) {
        var found;
        if ((found = this._sequence.filter(function (c) { return c.code === code.code; })).length > 0) {
            var idx = this._sequence.indexOf(found[0]);
            this._sequence[idx] = code;
        }
        else {
            this._sequence.push(code);
        }
        return this;
    };
    Genome.prototype.Extend = function (genome) {
        for (var i = 0; i < genome._sequence.length; ++i) {
            this.Add(genome._sequence[i]);
        }
        return this;
    };
    Object.defineProperty(Genome.prototype, "Width", {
        get: function () {
            return this._sequence.length;
        },
        enumerable: true,
        configurable: true
    });
    Genome.prototype.toJSONString = function () {
        return JSON.stringify(this.toJSONObject());
    };
    Genome.prototype.toJSONObject = function () {
        var flat = {};
        this._sequence.forEach(function (c) { return flat[c.code] = c.decode; });
        return { data: flat };
    };
    return Genome;
}());
var Act = (function () {
    function Act(verb) {
        this.verb = verb;
    }
    return Act;
}());
var Neuron = (function () {
    function Neuron(thresholder, threshold) {
        if (threshold === void 0) { threshold = 0; }
        this.thresholder = thresholder;
        this.threshold = threshold;
        this.thresholder = thresholder;
    }
    Object.defineProperty(Neuron.prototype, "Threshold", {
        get: function () {
            return this.threshold;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Neuron.prototype, "State", {
        get: function () {
            return this.soma.state;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Neuron.prototype, "Memory", {
        get: function () {
            return this.bus;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Neuron.prototype, "Value", {
        get: function () {
            return this.soma.body.get('value');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Neuron.prototype, "Affirmative", {
        get: function () {
            return this.thresholder(this);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Neuron.prototype, "Identity", {
        get: function () {
            return this.soma.body.get('id') || this.soma.body.get('name');
        },
        enumerable: true,
        configurable: true
    });
    Neuron.prototype.invoke = function (state) {
        this.soma.invoke(state);
        return this;
    };
    Neuron.prototype.actify = function (verb) {
        this.act = new Act(verb);
        return this;
    };
    Neuron.prototype.modify = function (that) {
        that(this.soma);
        return this;
    };
    Neuron.prototype.somatize = function (soma) {
        this.soma = soma;
        return this;
    };
    Neuron.prototype.memorize = function (bus) {
        this.bus = this.bus ? this.bus.Extend(bus) : bus;
        return this;
    };
    Neuron.prototype.Forget = function () {
        var exclude = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            exclude[_i] = arguments[_i];
        }
        if (exclude.length > 0) {
            this.bus.ForEach(function (c, i, g) { return !exclude.some(function (e) { return Code.Transform(e) === c.code; }) && g.Remove(c); });
        }
        else {
            this.bus.ForEach(function (c, i, g) { return g.Remove(c); });
        }
        return this;
    };
    Neuron.prototype.innervate = function (that) {
        if (that instanceof BipolarNeuron || !UIEngine.AreIdentical(this, that)) {
            this.soma.synapse.bind(that);
            that.threshold += 1;
        }
        return that;
    };
    Neuron.prototype.charge = function (type, charger) {
        var _this = this;
        this.soma.body.on(type, function (obj, ev) {
            charger(ev, _this, function () {
                _this.act.verb(_this.Affirmative, _this, function (advance, bus) {
                    if (advance) {
                        _this.soma.synapse.jumps(function (n) { return n.memorize(bus).invoke(_this.Affirmative).act.verb(n.Affirmative, n, function (done, bus) { return n.memorize(bus); }); });
                    }
                });
            });
        });
        return this;
    };
    Neuron.prototype.cascade = function (step) {
        var last = null;
        do {
            step(last);
            last = (last || this).soma.synapse.nexus;
        } while (last);
        return this;
    };
    Neuron.test = function () {
        //test('Neuron Test', (assert?) => {
        //    var n = new Neuron(n=> n.soma.body.get('tagName') === 'a').somatize(new Soma(new ObjectModel().create('a'))).actify((done, n, next) => {
        //        assert.ok(done, 'done revoked false!');
        //        next(done);
        //    }).charge<UIEvent>('click', e=> assert.ok(e));
        //    n.soma.body.run('click');
        //});
    };
    return Neuron;
}());
var BipolarNeuron = (function (_super) {
    __extends(BipolarNeuron, _super);
    function BipolarNeuron(excitator, inhibitor, threshold) {
        var _this = _super.call(this, threshold) || this;
        _this.excitator = excitator;
        _this.inhibitor = inhibitor;
        _this.somatize(new Soma(new ObjectModel().create('input').mark({ type: 'hidden' }).commit()));
        return _this;
    }
    BipolarNeuron.prototype.resolve = function (resolver) {
        //this.actify((done, n, next, bus) => {
        //    done && resolver(this.Affirmative, this.State ? this.excitator.memorize(bus) : this.inhibitor.memorize(bus));
        //});
        return this;
    };
    return BipolarNeuron;
}(Neuron));
var Axon = (function () {
    function Axon(synapse) {
        this.synapse = synapse;
        this.synapse = new Synapse();
    }
    return Axon;
}());
var Soma = (function (_super) {
    __extends(Soma, _super);
    function Soma(body) {
        var _this = _super.call(this) || this;
        _this.invoked = true;
        _this._body = body;
        return _this;
    }
    Object.defineProperty(Soma.prototype, "body", {
        get: function () {
            return this._body;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Soma.prototype, "state", {
        get: function () {
            return this.invoked;
        },
        enumerable: true,
        configurable: true
    });
    Soma.prototype.invoke = function (state) {
        this.invoked = state;
        return this;
    };
    return Soma;
}(Axon));
var Synapse = (function () {
    function Synapse() {
    }
    Object.defineProperty(Synapse.prototype, "nexus", {
        get: function () {
            return this._binded;
        },
        enumerable: true,
        configurable: true
    });
    Synapse.prototype.bind = function (to) {
        this._binded = to;
    };
    Synapse.prototype.jumps = function (then) {
        if (this._binded) {
            then(this._binded);
        }
    };
    return Synapse;
}());
var Lobby = (function () {
    function Lobby() {
    }
    Lobby.ConstructUI = function (config) {
        Lobby.Async = new Asyncll(AsyncMode.WebSocket, '/');
        Lobby.Handshake(config, function (cfg) {
            Lobby.SetSlider(cfg);
            Lobby.SetNet(cfg);
        });
    };
    Lobby.SetNet = function (config) {
        var net = new Array();
        for (var i = 0; i < config.Constants['LocatorRangeCount']; ++i) {
            var input = new ObjectModel(document.getElementById('input-range-' + i));
            var neuron = new Neuron(function (n) { return n.Value > 0; }).somatize(new Soma(input)).actify(function (done, n, next) {
                next(done, new Genome().Add(UIEngine.GenerateCode(n)));
            }).charge('change', function (e, n, fire) { return fire(); });
            net.push(neuron);
        }
        var submit = new ObjectModel().create('input').commit();
        var submitter = new Neuron(function (n) { return n.Memory.Magnitude(function (c) { return c.code != Code.Transform('who'); }) === n.Threshold; })
            .somatize(new Soma(submit)).actify(function (done, n, next) {
            if (done) {
                Lobby.SetAnimation(net);
                var code = new Code().assign('type', 'type.action');
                Lobby.Async.transmit(n.Memory.Add(code), 'locate', function (preprotein) { return Lobby.SubmitLocate(preprotein, n, config); }, function (mutation) { return UIEngine.CreateMessenger(mutation.message || config.Messages['PleaseTryAgain']); }, function (genome) {
                    n.Forget('who');
                    Lobby.SetAnimation(net);
                });
            }
        });
        for (var i = 0; i < net.length; ++i) {
            net[i].innervate(submitter);
        }
    };
    Lobby.SubmitLocate = function (preprotein, n, config) {
        if (!preprotein.CycleCompleted) {
            var who = parseInt(preprotein.Am, 10) * parseInt(preprotein.I, 10);
            n.Memory.Take('who', function (cd) { return cd.decode = who.toString(); });
            UIEngine.CreateMessenger(preprotein.Message);
        }
        else if (preprotein.Located) {
            UIEngine.CreateMessenger(preprotein.Message);
        }
        else {
            UIEngine.CreateMessenger(config.Messages['PleaseTryAgain']);
        }
    };
    Lobby.SetAnimation = function (net) {
        for (var i = 0; i < net.length; ++i) {
            UIEngine.Phantom(net[i]);
        }
    };
    Lobby.SetSlider = function (config) {
        var history = function (elem, t) {
            if (elem.get('data-slider-track')) {
                var track = new ObjectModel(document.getElementById('figure-locator-' + elem.get('data-slider-track')));
                track.animate({
                    0: ['scaleX(1.0)', 'opacity: 1.0'],
                    100: ['scaleX(0.0)', 'opacity: 0.0']
                });
            }
            var pos = parseInt(elem.get('value'), 10);
            var floct = config.Constants['LocatorSlideCount'] * t + pos;
            var floc = new ObjectModel(document.getElementById('figure-locator-' + floct));
            floc.animate({
                0: ['scaleX(0.0)', 'opacity: 0.0'],
                100: ['scaleX(1.0)', 'opacity: 1.0']
            });
            elem.mark({ dataSliderTrack: floct });
        };
        var floccer = function (elem, t) {
            var pos = parseInt(elem.get('value'), 10);
            for (var i = 0; i < config.Constants['LocatorSlideCount']; ++i) {
                var floct = config.Constants['LocatorSlideCount'] * t + i;
                var floc = new ObjectModel(document.getElementById('figure-locator-' + floct))
                    .stylize(function (fl) { return [{
                        position: 'absolute',
                        right: '25%',
                        zIndex: -1
                    }]; });
                if (pos === i) {
                    elem.mark({ dataSliderTrack: floct });
                    floc.animate({
                        0: ['scaleX(0.0)', 'opacity: 0.0'],
                        100: ['scaleX(1.0)', 'opacity: 1.0']
                    });
                }
                else {
                    floc.animate({
                        0: ['scaleX(1.0)', 'opacity: 1.0'],
                        100: ['scaleX(0.0)', 'opacity: 0.0']
                    });
                }
            }
        };
        for (var i = 0; i < config.Constants['LocatorRangeCount']; ++i) {
            var input = new ObjectModel(document.getElementById('input-range-' + i));
            input.stylize(function (el) { return [{ paddingBottom: '10px', marginTop: '20px', marginRight: '2px', width: '35%' }]; });
            floccer(input, i);
            input.on('change', function (elem, e) {
                history(elem, parseInt(elem.get('id').replace('input-range-', ''), 10));
            });
        }
    };
    Lobby.Handshake = function (config, golgi) {
        Lobby.Async.transmit(new Genome().Add(new Code().assign('type', 'type.handshake')), 'locate', function (preprotein) { Lobby.Seed(preprotein); golgi(config); }, function (mutation) { UIEngine.CreateMessenger(mutation.message || config.Messages['HandshakeRejected']); }, function (genome) { });
    };
    Lobby.Seed = function (preprotein) {
        new ObjectModel(document.getElementById('content')).put(preprotein.Seed);
    };
    return Lobby;
}());
var NewItemModal = (function () {
    function NewItemModal() {
    }
    NewItemModal.ConstructUI = function (config) {
        SiteMaster.AsyncGenome.Add(Code.Construct('key', 'NewItemModal'));
        if (App.Task.Core.IDExists(document.location.pathname)) {
            SiteMaster.SelectModelsForDetail(config);
        }
        else {
            SiteMaster.SelectModel(config);
        }
        config.AjaxTransform('NewItemModal', SiteMaster.AsyncGenome, NewItemModal.TransformArrived);
        return NewItemModal;
    };
    NewItemModal.TransformArrived = function (data) {
        var response = data.response;
        function NewItemModalViewModel() {
            var self = this;
            self.FormData = {
                Close: ko.observable(response.FormData.Close),
                ModalTitle: ko.observable(response.FormData.ModalTitle),
                SaveChanges: ko.observable(response.FormData.SaveChanges),
                OptionTitle: ko.observable(response.FormData.OptionTitle),
                InputFields: ko.observableArray(response.FormData.InputFields),
                OptionFields: ko.observableArray(response.FormData.OptionFields)
            };
            self.OptionFieldsExist = ko.observable(response.FormData.OptionFields.length > 0);
            self.InputFieldsExist = ko.observable(response.FormData.InputFields.length > 0);
        }
        ;
        SiteMaster.AsyncGenome.Add(Code.Construct('NewItemModalViewModel', NewItemModalViewModel));
        SiteMaster.SubscribeIndeeder('NewItemModalViewModel', data);
        SiteMaster.AsyncGenome.Indeed();
    };
    NewItemModal.Check = function (err, genome) {
        genome.Add(Code.Construct('StatusText', err['statusText']));
    };
    NewItemModal.Always = function (genome) {
        App.Task.Stream().SwitchTo(1);
    };
    return NewItemModal;
}());
/// <reference path="../../typings/knockout/knockout.d.ts" />
/// <reference path="../../typings/knockout.viewmodel/knockout.viewmodel.d.ts" />
var Playground = (function () {
    function Playground() {
    }
    Playground.ConstructUI = function (config) {
        Playground.AsyncPost = new Asyncll(AsyncMode.jQueryAjax, config.ServiceHosts['Generic'](), AsyncMethod.Post);
        SiteMaster.AsyncGenome = new Genome();
        if (App.Task.Core.IDExists(document.location.pathname)) {
            Playground.GenerateDetailMarkup(config);
        }
        else {
            Playground.GenerateMarkup(config);
        }
        return Playground;
    };
    Playground.GenerateMarkup = function (config) {
        SiteMaster.SelectModel(config);
        Playground.AsyncPost.transmit(SiteMaster.AsyncGenome, 'List', Playground.ModelListed, Playground.Check, Playground.Always);
    };
    Playground.GenerateDetailMarkup = function (config) {
        SiteMaster.SelectModelsForDetail(config);
        Playground.AsyncPost.transmit(SiteMaster.AsyncGenome, 'List', Playground.ModelListed, Playground.Check, Playground.Always);
    };
    Playground.ModelListed = function (data) {
        var komodels = {};
        var koRowGenerator = function (row) {
            var korow = {};
            var relatedModels = [];
            for (var i = 0; i < row.Columns.length; i++) {
                var col = row.Columns[i];
                korow[col.Key] = ko.observable(col.Value);
                if (col.RelatedModel) {
                    relatedModels.push({
                        ID: ko.observable(row.RowID),
                        Model: ko.observable(col.RelatedModel)
                    });
                }
            }
            korow["RelatedModels"] = ko.observableArray(relatedModels);
            korow["CommandButtonExists"] = ko.observable(relatedModels.length > 0);
            return korow;
        };
        function PlaygroundViewModel() {
            var self = this;
            self.DataList = ko.observableArray(data.response['DataList'].Rows.map(function (row) { return koRowGenerator(row); }));
            self.removeItem = function (row) {
                Playground.AsyncPost.transmit(SiteMaster.AsyncGenome.Add(Code.Construct("ItemID", row.ID())), 'Delete', Playground.ModelDeleted, Playground.Check, Playground.Always);
                self.DataList.remove(row);
            };
            self.updateItem = function (row) {
                Playground.AsyncPost.transmit(SiteMaster.AsyncGenome.Add(Code.Construct("ItemID", row.ID())), 'Update', Playground.ModelUpdated, Playground.Check, Playground.Always);
            };
            self.detailsItem = function (row) {
                Playground.AsyncPost.transmit(SiteMaster.AsyncGenome.Add(Code.Construct("ItemID", row.ID()))
                    .Add(Code.Construct("RelatedModel", row.Model())), 'Details', Playground.ModelDetailed, Playground.Check, Playground.Always);
            };
        }
        ;
        SiteMaster.AsyncGenome.Add(Code.Construct('PlaygroundViewModel', PlaygroundViewModel));
        SiteMaster.SubscribeIndeeder('PlaygroundViewModel', data);
        SiteMaster.AsyncGenome.Indeed();
    };
    Playground.ModelDeleted = function (data) {
        console.log("deleted successfully: ");
        console.log(data);
    };
    Playground.ModelUpdated = function (data) {
        console.log("updated successfully: ");
        console.log(data);
    };
    Playground.ModelDetailed = function (data) {
        var response = data["response"];
        var form = document.forms[0];
        form.action = response["DetailsUrl"];
        form.submit();
    };
    Playground.Check = function (err, genome) {
        genome.Add(Code.Construct('StatusText', err['statusText']));
    };
    Playground.Always = function (genome) {
        App.Task.Stream().SwitchTo(1);
    };
    return Playground;
}());
var SiteMaster = (function () {
    function SiteMaster() {
    }
    SiteMaster.ConstructUI = function (config) {
        SiteMaster.AsyncGenome = new Genome();
        SiteMaster.DataArray = {};
        return SiteMaster;
    };
    SiteMaster.SubscribeIndeeder = function (k, data) {
        SiteMaster.DataArray[k] = data;
        SiteMaster.AsyncGenome.SubscribeIndeeder(2, function (genome) {
            SiteMaster.AsyncGenome.Take('PlaygroundViewModel', function (kovm1) {
                SiteMaster.AsyncGenome.Take('NewItemModalViewModel', function (kovm2) {
                    var template = SiteMaster.DataArray['NewItemModalViewModel'].response.Template;
                    var om = new ObjectModel(ObjectModel.that('#section-absolute-elements')).put(template);
                    var columnMap = {};
                    SiteMaster.DataArray['PlaygroundViewModel'].response.ColumnList.forEach(function (ci) { return columnMap[ci.Title] = ci.Label; });
                    var form = ObjectModel.that('#form-authenticate');
                    new ObjectModel(form)
                        .constructTable(columnMap, SiteMaster.AsyncGenome, function (omodel, table) {
                        omodel.put(table);
                        ko.cleanNode(form);
                        ko.applyBindings(App.Task.Core.Union(new kovm1.decode(), new kovm2.decode()));
                        ObjectModel.that('#buttonModalNewItem').click();
                    });
                });
            });
        });
    };
    SiteMaster.SelectModelsForDetail = function (config) {
        var match = document.location.pathname.match(/\/([^\d]+)\/?/g);
        var requestedModel = match[0].replace(config.VirtualDirectory, '').replace(/(\/)/g, '');
        var relatedModel = match[1].replace(/(\/)+/g, '');
        var itemID = document.location.pathname.match(/\d+/g)[0];
        SiteMaster.AsyncGenome.Add(Code.Construct("RequestedModel", requestedModel))
            .Add(Code.Construct("RelatedModel", relatedModel))
            .Add(Code.Construct("ItemID", itemID));
        return SiteMaster;
    };
    SiteMaster.SelectModel = function (config) {
        var match = document.location.pathname.replace(/\/(\w+)\//g, '').match(/\w+/g);
        var requestedModel = match[0];
        SiteMaster.AsyncGenome.Add(Code.Construct("RequestedModel", requestedModel));
        return SiteMaster;
    };
    return SiteMaster;
}());
var Glyph = (function () {
    function Glyph() {
    }
    Glyph.morph = function (config) {
        WebFontConfig = config.WebFontConfig;
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
            '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = true;
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.appendChild(wf);
        return this;
        //UIEngine.PaintRainbow(<HTMLElement>document.querySelector('#h1-greetings'));
        //UIEngine.PaintRainbow(<HTMLElement>document.querySelector('#p-greetings'));
    };
    return Glyph;
}());
var ObjectModel = (function () {
    function ObjectModel(element, anime) {
        this.element = element;
        this.anime = anime;
    }
    ObjectModel.that = function (the) {
        return document.querySelector(the);
    };
    ObjectModel.those = function (having, step) {
        var all = document.querySelectorAll(having);
        for (var i = 0; i < all.length; i++) {
            step(all.item(i), i, all.length);
        }
        return this;
    };
    ObjectModel.wipe = function (the) {
        var that = ObjectModel.that(the);
        that.parentElement.removeChild(that);
    };
    ObjectModel.contains = function (selector) {
        return !!this.that(selector);
    };
    Object.defineProperty(ObjectModel, "documentTop", {
        get: function () {
            var doc = document.documentElement;
            var top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
            return top;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ObjectModel, "documentLeft", {
        get: function () {
            var doc = document.documentElement;
            var left = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
            return left;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ObjectModel, "documentWidth", {
        get: function () {
            var doc = document.documentElement;
            var width = (window.outerWidth || doc.clientWidth || doc.scrollWidth);
            return width;
        },
        enumerable: true,
        configurable: true
    });
    ObjectModel.prototype.get = function (key) {
        if (key in this.element) {
            return this.element[key];
        }
        else if (key in this.element.attributes) {
            return this.element.attributes[key].nodeValue;
        }
        else if (key in this.element.style) {
            return this.element.style[key];
        }
    };
    ObjectModel.prototype.constructTable = function (columnMapping, genome, constructed) {
        var _this = this;
        App.Config.AjaxTransform("CommandList", genome, function (data) {
            var thead = '<thead><tr>';
            var tr = '<tr>';
            for (var col in columnMapping) {
                if (col in columnMapping) {
                    thead += '<th class="text-center">' + columnMapping[col] + '</th>';
                    tr += '<td data-bind="text: ' + col + '"></td>';
                }
            }
            thead += '<th class="text-center">' + App.Config.Captions["CommandsColumn"] + '</th>';
            tr += '<td>' + data.response.Template + '</td>';
            thead += '</tr></thead>';
            tr += '</tr>';
            var tbody = '<tbody data-bind="foreach: DataList">';
            tbody += tr;
            tbody += '</tbody>';
            var table = '<table class="table table-hover table-bordered">' + thead + tbody + '</table>';
            constructed(_this, table);
        });
        return this;
    };
    ObjectModel.prototype.take = function (elem) {
        this.element = elem;
        return this;
    };
    ObjectModel.prototype.mark = function (by) {
        for (var b in by) {
            if (b in this.element) {
                this.element[b] = by[b];
            }
            else if (b in this.element.attributes) {
                this.element.attributes[b].value = by[b];
            }
            else if (b.indexOf('data') === 0) {
                var ds = b.replace("data", "");
                ds = ds[0].toLowerCase() + ds.slice(1);
                this.element.dataset[ds] = by[b];
            }
        }
        return this;
    };
    ObjectModel.prototype.classify = function () {
        var _this = this;
        var classes = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            classes[_i] = arguments[_i];
        }
        classes.filter(function (cls) { return !_this.element.classList.contains(cls); }).forEach(function (cls) { return _this.element.classList.add(cls); });
        return this;
    };
    ObjectModel.prototype.append = function (the) {
        this.element.parentNode.insertBefore(the.element, this.element.nextSibling);
        return this;
    };
    ObjectModel.prototype.include = function (the) {
        this.element.appendChild(the.element);
        return this;
    };
    ObjectModel.prototype.create = function (tag) {
        this.element = document.createElement(tag);
        return this;
    };
    ObjectModel.prototype.hide = function () {
        this.element.hidden = true;
        return this;
    };
    ObjectModel.prototype.stylize = function (styler) {
        var styles = styler(this);
        for (var i = 0; i < styles.length; i++) {
            var style = styles[i];
            for (var s in style) {
                this.element.style[s] = style[s];
            }
        }
        return this;
    };
    Object.defineProperty(ObjectModel.prototype, "size", {
        get: function () {
            return {
                height: this.element.clientHeight || parseInt(this.element.style.height),
                width: this.element.clientWidth || parseInt(this.element.style.width),
                offset: {
                    left: this.element.clientLeft || this.element.scrollLeft || parseInt(this.element.style.left) || this.element.offsetLeft,
                    top: this.element.clientTop || this.element.scrollTop || parseInt(this.element.style.top) || this.element.offsetTop,
                },
                margin: {
                    left: parseInt(this.element.style.marginLeft, 10),
                    right: parseInt(this.element.style.marginRight, 10),
                    top: parseInt(this.element.style.marginTop, 10),
                    bottom: parseInt(this.element.style.marginBottom, 10)
                }
            };
        },
        enumerable: true,
        configurable: true
    });
    ObjectModel.prototype.put = function (text) {
        if ('value' in this.element) {
            this.element['value'] = text;
        }
        else {
            this.element.innerHTML = text;
        }
        return this;
    };
    ObjectModel.prototype.show = function () {
        this.element.style.display = 'block';
        this.element.hidden = false;
        return this;
    };
    ObjectModel.prototype.terminate = function () {
        var _this = this;
        setTimeout(function () { return _this.element.parentElement.removeChild(_this.element); }, 2000);
    };
    ObjectModel.prototype.commit = function (to) {
        to ? to.appendChild(this.element) : document.body.appendChild(this.element);
        return this;
    };
    ObjectModel.prototype.animate = function (keyFrames, config, newAnimation) {
        var an = new Anime('anime' + (Math.random() * 6666).toString().replace('.', '-'));
        this.anime = newAnimation ? an : this.anime || an;
        for (var key in keyFrames) {
            this.anime.keyframe(parseInt(key), keyFrames[key]);
        }
        this.anime.apply(this.element, config);
        return this;
    };
    ObjectModel.prototype.wait = function (by, what) {
        var _this = this;
        setTimeout(function () { return what(_this); }, by);
        return this;
    };
    ObjectModel.prototype.on = function (type, handler) {
        var _this = this;
        this.element.addEventListener(type, function (e) {
            handler(_this, e);
        }, false);
        return this;
    };
    ObjectModel.prototype.run = function (type) {
        var ev = new Event(type);
        this.element.dispatchEvent(ev);
    };
    return ObjectModel;
}());
var UIEngine = (function () {
    function UIEngine() {
    }
    UIEngine.Progress = function (n) {
        n.modify(function (soma) {
            var idSelector = '#' + n.Identity + '-skewed-ll';
            var skewed = new ObjectModel().create('div').mark({ id: idSelector.slice(1) }).hide();
            for (var k = 1; k <= 10; k++) {
                skewed.include(new ObjectModel().create('div').mark({ id: 'skew' + k })
                    .stylize(function (elem) { return [{
                        width: '13px',
                        height: '10px',
                        marginLeft: '2px',
                        display: 'inline-block',
                    }]; }));
            }
            soma.body.append(skewed);
            skewed
                .animate({
                0: ['translateX(0px)', 'skew(160deg)', 'scaleX(1.0)', 'opacity: 0.50'],
                100: ['translateX(180px)', 'skew(160deg)', 'scaleX(0.1)', 'opacity: 0.05']
            }, new AnimeConfig('ease-out', 'infinite', 0, 1.8)).show()
                .stylize(function (elem) { return [{
                    width: soma.body.size.width + 'px',
                    backgroundColor: '#101010',
                    position: 'absolute',
                    zIndex: 6666,
                    right: '0px',
                    top: -Math.floor((soma.body.size.height + elem.size.height) / 2) + 'px'
                }]; });
            soma.body.mark({ disabled: true });
        });
    };
    UIEngine.EndProgress = function (n, fadeout) {
        n.modify(function (soma) {
            var idSelector = '#' + n.Identity + '-skewed-ll';
            if (ObjectModel.contains(idSelector)) {
                ObjectModel.wipe(idSelector);
            }
            soma.body.mark({ disabled: false });
        });
        fadeout && UIEngine.Phantom(n);
    };
    UIEngine.Phantom = function (n) {
        n.modify(function (soma) {
            if (!soma.body.get('data-hidden-ll')) {
                soma.body.animate({ 0: ['opacity: 1.0'], 100: ['opacity: 0.0'] }).mark({ dataHiddenLl: true });
            }
            else {
                soma.body.animate({ 0: ['opacity: 0.0'], 100: ['opacity: 1.0'] }, new AnimeConfig('ease-out', 'forwards', 0, 1.8), true).mark({ dataHiddenLl: false });
            }
        });
    };
    UIEngine.AreIdentical = function (n, o) {
        return n.Identity === o.Identity;
    };
    UIEngine.GenerateCode = function (from) {
        return new Code().assign(from.Identity, from.Value);
    };
    UIEngine.CreateMessenger = function (message, withLink) {
        UIEngine.registeredMessengerTop = UIEngine.registeredMessengerTop > document.documentElement.clientHeight ? 54 : UIEngine.registeredMessengerTop;
        var msngr = new ObjectModel().create('div').mark({ id: 'div-vector-message' + UIEngine.registeredMessengerTop })
            .stylize(function (elem) { return [{ position: 'fixed', top: ObjectModel.documentTop + UIEngine.registeredMessengerTop + 'px', right: '20px', width: (message.length * 0.6) + 'em', lineHeight: '140%', textAlign: 'center' }]; })
            .stylize(function (elem) { return [{ borderRadius: '6px', padding: '18px 10px', backgroundColor: 'white', color: '101010', cursor: 'pointer', zIndex: 1800, border: '2px solid #101010', opacity: '0.6' }]; });
        withLink ? msngr.include(withLink.put(message)) : msngr.put(message);
        msngr.commit().animate({
            0: ['translateX(' + msngr.size.width + 'px)', 'rotateX(90deg)'],
            100: ['translateX(0px)', 'rotateX(0deg)']
        })
            .on('click', function (elem, e) {
            return elem.animate({
                0: ['translateX(0px)', 'rotateX(0deg)'],
                100: ['translateX(' + (elem.size.width + 20) + 'px)', 'rotateX(90deg)']
            }, null, true).terminate();
        });
        UIEngine.registeredMessengerTop += msngr.size.height + 2;
        withLink && App.Task.Stream();
    };
    UIEngine.LinkToStream = function (streamIndex, action) {
        var an = new ObjectModel().create('a').mark({ href: '#', 'dataStreamIndex': streamIndex })
            .on('click', function (elem, ev) { return action(elem, ev); });
        return an;
    };
    UIEngine.PaintRainbow = function (to) {
        var text = to.innerText;
        to.innerText = '';
        var len = text.length;
        var toObj = new ObjectModel().take(to);
        for (var i = 0; i < len; i++) {
            var span = new ObjectModel().create('span').classify('rb' + i % 5).put(text[i]);
            toObj.include(span);
        }
    };
    return UIEngine;
}());
UIEngine.registeredMessengerTop = 54;
var KeyFrame = (function () {
    function KeyFrame(percent, patterns) {
        this.percent = percent;
        this.patterns = patterns;
    }
    KeyFrame.prototype.toString = function (designator) {
        var transformList = this.patterns.filter(function (p) { return p.indexOf(':') === -1; });
        var normalList = this.patterns.filter(function (p) { return p.indexOf(':') !== -1; });
        var transformstring = transformList.length > 0 ? (transformList
            .reduce(function (prev, cur) { return prev + ' ' + cur; }, designator + 'transform: ') + ';') : '';
        var normalString = normalList.length > 0 ? normalList.map(function (p) { return p + '; '; })
            .reduce(function (prev, cur) { return prev + ' ' + cur; }, ' ') : '';
        return ' ' + this.percent + '% { ' + transformstring + normalString + ' } ';
    };
    return KeyFrame;
}());
var AnimeConfig = (function () {
    function AnimeConfig(ease, fillmode, delay, transition) {
        this.ease = ease;
        this.fillmode = fillmode;
        this.delay = delay;
        this.transition = transition;
    }
    return AnimeConfig;
}());
var Anime = (function () {
    function Anime(name) {
        this.name = name;
        Anime.Config = new AnimeConfig('ease-in-out', 'forwards', 0, App.Config.TransitionLength);
        this.keyframes = new Array();
    }
    Anime.prototype.keyframe = function (percent, patterns) {
        var kfs;
        if ((kfs = this.keyframes.filter(function (kf) { return kf.percent === percent; })).length > 0) {
            kfs[0].patterns = patterns;
        }
        else {
            this.keyframes.push(new KeyFrame(percent, patterns));
        }
        return this;
    };
    Anime.prototype.apply = function (to, config) {
        var cfg = config || Anime.Config;
        var added;
        var id = '__ll_keyframe_' + this.name;
        var style = document.querySelector('#' + id);
        if (!style) {
            var st = document.createElement("style");
            st.id = id;
            added = document.documentElement.appendChild(st);
        }
        else {
            added = style;
        }
        added.innerText = this.toString('keyframes') + '  ' + this.toString('-moz-keyframes') + '  ' + this.toString('-webkit-keyframes');
        to.style.animation = to.style.webkitAnimation = to.style['webkitAnimation'] =
            this.name + ' ' + cfg.transition + 's ' + cfg.fillmode + ' ' + cfg.ease + ' ' + cfg.delay + 's';
        return this;
    };
    Anime.prototype.monaic = function (to, pattern, config) {
        var cfg = config || Anime.Config;
        to.style.webkitTransition = to.style.transition = to.style['-webkit-transition'] =
            'all ' + cfg.transition + 's ' + cfg.ease + ' ' + cfg.delay + 's';
        to.style.transform = to.style.webkitTransform = to.style['-webkit-transform'] = pattern;
    };
    Anime.prototype.toString = function (designator) {
        return '@' + designator + ' ' + this.name + ' {' +
            this.keyframes.map(function (kf) { return kf.toString(designator.replace('keyframes', '')); })
                .reduce(function (prev, cur) { return prev + ' ' + cur; }, ' ') + '}';
    };
    Anime.test = function () {
        //test('Anime toString() should return value', () => {
        //    var a = new Anime('demo');
        //    var str = a.keyframe(0, ['scaleX(1.0) translateX(0px)']).keyframe(50, ['scaleX(0.5) translateX(-305px)'])
        //        .keyframe(100, ['scaleX(0.5) translateX(-305px) translateY(100px)'])
        //        .toString('keyframes');
        //    expect(1);
        //    console.log(str);
        //    equal(str.length, 10, 'toString() returned a value');
        //});
    };
    return Anime;
}());
var MainStream = (function () {
    function MainStream(transition, anime, clickRegister, distRegister) {
        if (clickRegister === void 0) { clickRegister = 0; }
        if (distRegister === void 0) { distRegister = 0; }
        this.anime = anime;
        this.clickRegister = clickRegister;
        this.distRegister = distRegister;
        this.anime = anime || new Anime('wanderfly');
    }
    MainStream.generate = function (config) {
        return new MainStream(config.TransitionLength);
    };
    MainStream.prototype.SwitchTo = function (i) {
        ObjectModel.that('a[data-stream-index="' + i + '"]').click();
    };
    MainStream.prototype.registerFlow = function () {
        var streamAnchors = new Array();
        var al = document.getElementsByTagName('a');
        for (var j = 0; j < al.length; j++) {
            var a = al.item(j);
            if (a.attributes['data-stream-index'] !== undefined) {
                streamAnchors.push(al.item(j));
            }
        }
        for (var j = 0; j < streamAnchors.length; j++) {
            var a = streamAnchors[j];
            this.a(streamAnchors.length, a);
        }
        return this;
    };
    MainStream.prototype.a = function (count, a) {
        a.addEventListener('click', function (that, e) {
            e.preventDefault();
            var clickedIndex = parseInt(a.attributes['data-stream-index'].value, 10);
            var diff = this.clickRegister - clickedIndex;
            var dist = this.distRegister;
            if (diff < 0) {
                //up
                for (var k = clickedIndex - 1; k >= clickedIndex + diff; k--) {
                    dist -= document.getElementById('stream-' + k).clientHeight;
                }
            }
            else if (diff > 0) {
                //down
                for (var k = clickedIndex + 1; k <= clickedIndex + diff; k++) {
                    dist += document.getElementById('stream-' + k).clientHeight;
                }
            }
            else {
                //just to the frame of clicked index
                dist = this.distRegister;
            }
            this.d(count, dist);
            this.clickRegister = clickedIndex;
            this.distRegister = dist;
            return false;
        }.bind(this, a), false);
    };
    MainStream.prototype.d = function (count, dist) {
        var pattern = 'translateY(' + (dist + ObjectModel.documentTop) + 'px)';
        for (var j = 0; j < count; j++) {
            //flow by dist
            var s = document.getElementById('stream-' + j);
            s && this.anime.monaic(s, pattern);
        }
    };
    return MainStream;
}());
//# sourceMappingURL=one.js.map