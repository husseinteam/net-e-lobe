﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PS.Core.Model.Context;
using PS.Core.Extensions.ParserExtensions;

namespace PS.Core.UnitTest.ModelTests {
    [TestClass]
    public class DataTests {

        [TestMethod]
        public void DatabaseInitializesProperly() {

            using (var context = new PSDataContext()) {
                Assert.AreNotEqual(0, context.Users.Magnitude());
            }

        }

    }
}
