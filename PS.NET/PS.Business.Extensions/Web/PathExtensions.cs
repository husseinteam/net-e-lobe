﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PS.Core.Definitions.Web;

namespace PS.Business.Extensions.Web {
    public static class PathExtensions {

        public static String Virtualize(this String path) {

            return String.Format("/{0}{1}", SWebDefinitions.ApiVirtualDirectory, path);

        }

    }
}
