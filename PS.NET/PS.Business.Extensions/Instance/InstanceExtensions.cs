﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PS.Business.Extensions.Instance {
    public static class InstanceExtensions {

        public static Boolean InstanceEqual<TItem>(this TItem item, TItem to) {

            var props = typeof(TItem).GetProperties(BindingFlags.Instance
                | BindingFlags.Public)
                .Where(pi => pi.CanRead && pi.CanWrite).ToArray();

            foreach (var prop in props) {
                try {
                    if (!prop.GetValue(item).Equals(prop.GetValue(to))) {
                        return false;
                    }
                } catch {
                    continue;
                }
            }
            return true;

        }

    }
}
