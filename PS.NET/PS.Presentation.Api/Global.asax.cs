﻿using System;
using PS.Business.Api.Configuration;
using System.Web.Http;

namespace PS.Presentation.Api {
    public class WebApiApplication : System.Web.HttpApplication
    {

        protected void Application_Start()
        {
			GlobalConfiguration.Configure(GenericConfig.Routes);
            GlobalConfiguration.Configure(GenericConfig.Formatters);

        }

		void Session_Start(object sender, EventArgs e) {

			GlobalConfiguration.Configure(GenericConfig.Session);

		}

		public override void Init() {

			base.Init();
			this.PostAuthenticateRequest += GenericConfig.PostAuthenticateRequest;

		}

    }
}
