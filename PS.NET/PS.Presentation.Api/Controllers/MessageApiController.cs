﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using PS.Business.Controller.Base;
using PS.Business.Controller.Infrastructure;
using PS.Core.Definitions.Web;
using PS.Core.Model.Entity.POCO;
using PS.Emitter.Ligand.Emit;

namespace PS.Presentation.Api.Controllers {
    [RoutePrefix(SControllerDefinitions.ApiRoute + "/" + SControllerDefinitions.MessageRoute)]
    public class MessageApiController : GenericApiController<Message> {

        protected override BaseController<Message> ApiController {
            get {
                return SLigand.Attach<BaseController<Message>>();
            }
        }

        #region Get Actions
        // GET: api/Message
        [Route("all"), HttpGet]
        public async Task<ICollection<Message>> All() {

            return await ApiController.All();

        }

        // GET: api/messages/received/5
        [Route("received/{receiverID}"), HttpGet]
        public async Task<ICollection<Message>> GetReceived(Int64 receiverID) {

            var filtered = await ApiController
                .AddSelectQuery(m => m.Receiver.ID == receiverID).All();
            return filtered.ToArray();

        }

        [Route("sent/{senderID}"), HttpGet]
        public async Task<ICollection<Message>> GetSent(Int64 senderID) {

            var filtered = await ApiController
                .AddSelectQuery(m => m.Sender.ID == senderID).All();
            return filtered.ToArray();

        }

        [Route("from/{senderID}/to/{receiverID}"), HttpGet]
        public async Task<ICollection<Message>> GetBetween(Int64 senderID
            , Int64 receiverID) {

            var filtered = await ApiController
                .AddSelectQuery(m => m.Receiver.ID == receiverID && m.Sender.ID == senderID).All();
            return filtered.ToArray();

        }

        #endregion

        #region Post Actions

        [Route("insert"), HttpPost]
        public async Task<IHttpActionResult> Insert([FromBody] Message message) {

            var inserted = await ApiController.Insert(message);
            return Ok(inserted);

        }
        #endregion

    }
}
