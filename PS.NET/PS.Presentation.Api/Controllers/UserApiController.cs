﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using PS.Business.Api.Handlers;
using PS.Business.Api.Session;
using PS.Business.Controller.Base;
using PS.Business.Controller.Infrastructure;
using PS.Business.Model.Base;
using PS.Core.Definitions.Web;
using PS.Core.Model.Entity.Enum;
using PS.Core.Model.Entity.POCO;
using PS.Emitter.Ligand.Emit;

namespace PS.Presentation.Api.Controllers {
    [RoutePrefix(SControllerDefinitions.ApiRoute + "/" + SControllerDefinitions.UserRoute)]
    public class UserApiController : GenericApiController<User> {
        
        protected override BaseController<User> ApiController {
            get {
                return SLigand.Attach<BaseController<User>>();
            }
        }

        #region Get Actions
        // GET: api/User
        [Route("all"), HttpGet]
        public async Task<ICollection<User>> All() {

            return await ApiController.All();

        }

        #endregion

        #region Post Actions

        [Route("insert"), HttpPost]
        public async Task<IHttpActionResult> Insert([FromBody] User User) {

            var inserted = await ApiController.Insert(User);
            return Ok(inserted);

        }
        #endregion

    }
}
