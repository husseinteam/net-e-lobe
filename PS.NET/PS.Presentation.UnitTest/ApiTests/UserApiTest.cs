﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PS.Business.Controller.Base;
using PS.Business.Extensions.Instance;
using PS.Core.Definitions.Web;
using PS.Core.Model.Entity.POCO;
using PS.Emitter.Ligand.Emit;
using PS.Presentation.UnitTest.Base;

namespace PS.Presentation.UnitTest.ApiTests {
    [TestClass]
    public class UserApiTest : ApiTestBase {

        #region Implemented

        protected override string ControllerRoutePrefix {
            get {
                return SControllerDefinitions.UserRoute;
            }
        }

        #endregion

        #region TestMethods For Post Actions

        [TestMethod]
        public async Task UserInsertCallRunsProperly() {

            var UserController = SLigand.Attach<BaseController<User>>();
            Assert.IsNotNull(UserController);

            var credential = UserController.Representative;
            credential = await ClientCallPostAsync<User>("insert", credential);

            var filtered = await UserController.ForKey(credential.ID);
            Assert.IsFalse(credential.InstanceEqual(filtered));

            await UserController.Remove(filtered);
        }

        [TestMethod]
        public async Task UserInsertRunsProperly() {

            var UserController = SLigand.Attach<BaseController<User>>();
            Assert.IsNotNull(UserController);

            var credential = UserController.Representative;
            credential = await UserController.Insert(credential);

            var filtered = await UserController.ForKey(credential.ID);
            Assert.IsTrue(credential.InstanceEqual(filtered));

            await UserController.Remove(filtered);

        }

        #endregion

        #region TestMethods For Get Actions

        [TestMethod]
        public async Task UsersAllClientCallRunsProperly() {

            var credentials = await ClientCallGetAsync<IEnumerable<User>>("all");
            Assert.IsNotNull(credentials);
            Assert.AreNotEqual(0, credentials.Count());

        }

        [TestMethod]
        public async Task UserControllerGetsAllUsersProperly() {

            var UserController = SLigand.Attach<BaseController<User>>();
            Assert.IsNotNull(UserController);
            var gotten = await UserController.All();
            Assert.IsNotNull(gotten);
            Assert.AreNotEqual(0, gotten.Count);

        }

        #endregion

    }

}
