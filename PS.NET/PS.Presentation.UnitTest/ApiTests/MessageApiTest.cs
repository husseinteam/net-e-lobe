﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PS.Business.Controller.Base;
using PS.Business.Extensions.Instance;
using PS.Core.Definitions.Web;
using PS.Core.Model.Entity.POCO;
using PS.Emitter.Ligand.Emit;
using PS.Presentation.UnitTest.Base;

namespace PS.Presentation.UnitTest.ApiTests {
    [TestClass]
    public class MessageApiTest : ApiTestBase {

        #region Implemented

        protected override string ControllerRoutePrefix {
            get {
                return SControllerDefinitions.MessageRoute;
            }
        }

        #endregion

        #region TestMethods For Post Actions

        [TestMethod]
        public async Task MessageInsertCallRunsProperly() {

            var messageController = SLigand.Attach<BaseController<Message>>();
            Assert.IsNotNull(messageController);

            var message = messageController.Representative;
            message = await ClientCallPostAsync<Message>("insert", message);

            var filtered = await messageController.ForKey(message.ID);
            Assert.IsFalse(message.InstanceEqual(filtered));

            await messageController.Remove(filtered);
        }

        [TestMethod]
        public async Task MessageInsertRunsProperly() {

            var messageController = SLigand.Attach<BaseController<Message>>();
            Assert.IsNotNull(messageController);

            var message = messageController.Representative;
            message = await messageController.Insert(message);

            var filtered = await messageController.ForKey(message.ID);
            Assert.IsTrue(message.InstanceEqual(filtered));

            await messageController.Remove(filtered);

        }

        #endregion

        #region TestMethods For Get Actions

        [TestMethod]
        public async Task MessagesAllClientCallRunsProperly() {

            var messages = await ClientCallGetAsync<IEnumerable<Message>>("all");
            Assert.IsNotNull(messages);
            Assert.AreNotEqual(0, messages.Count());

        }

        [TestMethod]
        public async Task MessagesReceivedClientCallRunsProperly() {

            var messageController = SLigand.Attach<BaseController<Message>>();
            var all = await messageController.All();
            var receiverId = all.First().Receiver.ID;
            var messages = await ClientCallGetAsync<IEnumerable<Message>>("received/" + receiverId);
            Assert.IsNotNull(messages);
            Assert.AreNotEqual(0, messages.Count());

        }

        [TestMethod]
        public async Task MessagesSentClientCallRunsProperly() {

            var messageController = SLigand.Attach<BaseController<Message>>();
            var all = await messageController.All();
            var senderID = all.First().Sender.ID;
            var messages =
                await ClientCallGetAsync<IEnumerable<Message>>(
                    "sent/" + senderID);
            Assert.IsNotNull(messages);
            Assert.AreNotEqual(0, messages.Count());

        }

        [TestMethod]
        public async Task MessagesBetweenClientCallRunsProperly() {

            var messageController = SLigand.Attach<BaseController<Message>>();
            var all = await messageController.All();
            var senderID = all.First().Sender.ID;
            var receiverID = all.First().Receiver.ID;
            var messages =
                await ClientCallGetAsync<IEnumerable<Message>>(
                    String.Format("from/{0}/to/{1}"
                    , senderID, receiverID));
            Assert.IsNotNull(messages);
            Assert.AreNotEqual(0, messages.Count());

        }

        [TestMethod]
        public async Task MessageControllerGetsAllMessagesProperly() {

            var messageController = SLigand.Attach<BaseController<Message>>();
            Assert.IsNotNull(messageController);
            var gotten = await messageController.All();
            Assert.IsNotNull(gotten);
            Assert.AreNotEqual(0, gotten.Count);

        }

        [TestMethod]
        public async Task MessageControllerGetsReceivedMessagesProperly() {

            var messageController = SLigand.Attach<BaseController<Message>>();
            var all = await messageController.All();
            var receiverId = all.First().Receiver.ID;
            var filtered = await messageController
                 .AddSelectQuery(m => m.Receiver.ID == receiverId).All();
            Assert.AreNotEqual(0, filtered.Count);

        }

        [TestMethod]
        public async Task MessageControllerGetsSentMessagesProperly() {

            var messageController = SLigand.Attach<BaseController<Message>>();
            var all = await messageController.All();
            var senderID = all.First().Sender.ID;
            var filtered = await messageController
                 .AddSelectQuery(m => m.Sender.ID == senderID).All();
            Assert.AreNotEqual(0, filtered.Count);

        }

        [TestMethod]
        public async Task MessageControllerGetsInBetweenMessagesProperly() {

            var messageController = SLigand.Attach<BaseController<Message>>();
            var all = await messageController.All();
            var senderID = all.First().Sender.ID;
            var receiverID = all.First().Receiver.ID;
            var filtered = await messageController
                 .AddSelectQuery(m => m.Receiver.ID == receiverID && m.Sender.ID == senderID).All();
            Assert.AreNotEqual(0, filtered.Count);

        }

        #endregion

    }

}
