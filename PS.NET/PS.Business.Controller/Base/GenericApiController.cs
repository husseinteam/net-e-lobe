﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using PS.Business.Controller.Base;
using PS.Business.Model.Base;
using PS.Core.Model.Entity.Base;

namespace PS.Business.Controller.Base {
    public abstract class GenericApiController<TEntity> : ApiController
            where TEntity : BaseModel {

        #region Protected

        protected abstract BaseController<TEntity> ApiController { get; }

        #endregion


    }
}
