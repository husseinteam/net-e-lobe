﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PS.Business.Controller.Base;
using PS.Core.Model.Entity.POCO;

namespace PS.Business.Controller.Infrastructure {
    public class UserController : BaseController<User> {

        #region Ctor

        public UserController() {

            Repository
                .AddCollection(e => e.RecievedMessages)
                .AddCollection(e => e.SentMessages)
                .AddCollection(e => e.UsersInRoles);

        }

        #endregion


    }
}
