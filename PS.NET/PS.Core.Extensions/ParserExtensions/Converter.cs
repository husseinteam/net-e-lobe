﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PS.Core.Extensions.ParserExtensions {
    public static class Converter {

        public static Int32 ParseToInt32(this Int64 numberLong) {
            return numberLong < Int32.MaxValue ? (Int32)numberLong : -1;
        }

        public static void TransferTo(this Object source, Object dest) {

            var props = source.GetType().GetProperties(BindingFlags.Instance
                | BindingFlags.Public)
                .Where(pi => pi.CanRead && pi.CanWrite).ToList();

            dynamic destExp = dest;
            props.ForEach(pi => {
            var dict = (destExp as IDictionary<String, Object>);
                if (dict.ContainsKey(pi.Name))
                    dict[pi.Name] = pi.GetValue(source, null);
                else
                    dict.Add(pi.Name, pi.GetValue(source, null));
                });

        }

        public static TResponse Import<TResponse>(this Object dest, Object importedEExpandoObject) 
                where TResponse : class {

            var props = dest.GetType()
                .GetProperties(BindingFlags.Instance 
                | BindingFlags.Public)
                .Where(pi => pi.CanRead && pi.CanWrite).ToList();

            props.ForEach(pi =>
            {
                var dict = (importedEExpandoObject as IDictionary<String, Object>);
                if (dict.ContainsKey(pi.Name))
                    dict[pi.Name] = pi.GetValue(dest, null);
                else
                    dict.Add(pi.Name, pi.GetValue(dest, null));
            });
            return dest as TResponse;
        }

    }
}
