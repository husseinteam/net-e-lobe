﻿

using System.ComponentModel.DataAnnotations;

namespace PS.Core.Model.Entity.Enum {

    public enum ESessionState {

        [Display(Description = "Oturum açık")]
        LoggedIn = 1,

        [Display(Description = "Oturum kapalı")]
        LoggedOut

    }

}