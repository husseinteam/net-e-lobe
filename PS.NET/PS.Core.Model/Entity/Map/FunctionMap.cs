﻿using PS.Core.Model.Entity.Base;
using PS.Core.Model.Entity.POCO;
using System.ComponentModel.DataAnnotations.Schema;

namespace PS.Core.Model.Entity.Map {
    public class FunctionMap : BaseMap<Function> {

        public FunctionMap() {

            #region Properties

            this.Property(t => t.Code)
                .HasColumnName("Code")
                .HasMaxLength(255)
                .IsRequired();

            this.Property(t => t.Description)
                .HasColumnName("Description")
                .HasMaxLength(255)
                .IsOptional();

            #endregion

            #region Navigation Properties

            this.HasRequired(t => t.Module)
                .WithMany(t => t.Functions)
                .Map(x => x.MapKey("ModuleID"));

            #endregion

            this.ToTable("Functions");

        }

    }
}
