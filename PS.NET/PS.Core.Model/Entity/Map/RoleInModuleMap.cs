﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PS.Core.Model.Entity.Base;

namespace PS.Core.Model.Entity.POCO {
    public class RoleInModuleMap : BaseMap<RoleInModule> {

        public RoleInModuleMap() {

            #region Navigation Properties

            this.HasRequired(t => t.Module)
                .WithMany(t => t.RolesInModules)
                .Map(x => x.MapKey("ModuleID"));

            this.HasRequired(t => t.Role)
                .WithMany(t => t.RolesInModules)
                .Map(x => x.MapKey("RoleID"));

            #endregion

            this.ToTable("RolesInModules");
        }

    }
}
