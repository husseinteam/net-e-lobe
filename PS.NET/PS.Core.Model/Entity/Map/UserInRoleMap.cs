﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PS.Core.Model.Entity.Base;

namespace PS.Core.Model.Entity.POCO {
    public class UserInRoleMap : BaseMap<UserInRole> {

        public UserInRoleMap() {

            #region Navigation Properties

            this.HasRequired(t => t.Role)
                .WithMany(t => t.UsersInRoles)
                .Map(x => x.MapKey("RoleID"));

            this.HasRequired(t => t.User)
                .WithMany(t => t.UsersInRoles)
                .Map(x => x.MapKey("UserID"));

            #endregion

            this.ToTable("UsersInRoles");
        }

    }
}
