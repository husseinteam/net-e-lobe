﻿using PS.Core.Model.Entity.Base;
using PS.Core.Model.Entity.POCO;
using System.ComponentModel.DataAnnotations.Schema;


namespace PS.Core.Model.Entity.Map {
    public class RoleMap : BaseMap<Role>{

        public RoleMap() {

            #region Properties

            this.Property(t => t.Code)
                .HasColumnName("Code")
                .HasMaxLength(255)
                .IsRequired();

            #endregion

            #region Navigation Properties


            #endregion

            this.ToTable("Roles");

        }

    }
}
