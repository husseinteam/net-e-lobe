﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS.Core.Model.Entity.Base {
    public class BaseMap<TPOCO> : EntityTypeConfiguration<TPOCO>
    where TPOCO : BaseModel {

        #region Ctor

        public BaseMap() {

            this.HasKey(t => t.ID)
                .Property(t => t.ID)
                .HasColumnName("ID")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();
            this.Property(t => t.IsDeleted)
                .HasColumnName("IsDeleted")
                .IsRequired();
        }

        #endregion

    }
}
