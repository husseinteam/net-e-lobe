﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace Gene.Framework {
    public interface IEmitter {

        #region Methods

        PropertyBuilder CreateProperty(TypeBuilder generator, Type type, Type propertyType, Type fieldType, String propertyName, MethodAttributes attributes);
        TypeBuilder CreateTypeBuilder(AssemblyName name, string typeName, AssemblyBuilder assemblyBuilder);
        AssemblyBuilder CreateAssemblyBuilder(AssemblyName name);
        void SetAttribute<TAttribute>(dynamic builder, Type[] argTypes, Object[] args);

        #endregion

    }
}
