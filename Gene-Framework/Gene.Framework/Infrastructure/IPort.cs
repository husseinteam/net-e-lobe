﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Gene.Framework {
	public interface IPort {

		#region Properties

        Assembly Porter { get; set; }
        DbConnection Connection { get; }
        INetworkCredentials NetworkCredentials { get; set; }
		#endregion

	}
}
