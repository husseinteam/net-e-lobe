﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gene.Framework {
  public interface ITranslation {

        #region Properties
        String Information { get; }
        EGain Gain { get; set;  }
        #endregion

        #region Methods
        ITranslation Append(Exception error);
        #endregion

    }
}
