﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gene.Framework {

	public class LimitsToAttribute : MaxLengthAttribute {
		public LimitsToAttribute() {

		}
		public LimitsToAttribute(int length)
			: base(length) {

		}
		public new string ErrorMessageResourceName {
			get {
				return base.ErrorMessageResourceName;
			}
			set {
				base.ErrorMessageResourceName = value;
			}
		}
		public new Type ErrorMessageResourceType {
			get {
				return base.ErrorMessageResourceType;
			}
			set {
				base.ErrorMessageResourceType = value;
			}
		}

	}

}
