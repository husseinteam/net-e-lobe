﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using Gene.Framework;

namespace Gene.Framework {
	public abstract class Genome : IGenome {

		#region Inherits

		#region Fields
		private static ILexicon _Lexer;
		protected static ILexicon Lexer {
			
			get {
				return _Lexer ?? (_Lexer = SFactory.Object().Generate<ILexicon>());
			}
		
		}

		#endregion

		#endregion

		#region IGene Members

		[Key]
		public Guid Epitope { get; set; }

		public IGenome Primase<TGene>(Action<TGene> enzyme)
				where TGene : IGenome {
			enzyme((TGene)(this as IGenome));
			return this;
		}

		public TGene TerminateSequence<TGene>() where TGene : IGenome {
			return (TGene)(this as IGenome);
		}

		public virtual TGene Replicate<TGene>()
			where TGene : class, IGenome {
			Epitope = Guid.NewGuid();
			return this as TGene;
		}

		#endregion

	}
}
