﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Gene.Framework {
    internal class NetworkCredentials : INetworkCredentials {

        #region ctor

        public NetworkCredentials() {
            _IntegratedSecurity = true;
        }

        #endregion

        #region Fields

        private String _UserID;
        private String _Password;
        private string _ConnectorFormat;
        private Boolean _IntegratedSecurity;
        private bool _PersistSecurityInfo;
        private String _AttachDbFileName;

        #endregion

        #region INetworkCredentials Members

        public String DataSource { get; set; }
        public String InitialCatalog { get; set; }
        public String UserID {
            get { return _UserID; }
            set {
                _UserID = value;
                _IntegratedSecurity = String.IsNullOrEmpty(value);
            }
        }
        public String Password {
            get { return _Password; }
            set {
                _Password = value;
                _IntegratedSecurity = String.IsNullOrEmpty(value);
            }
        }

        public bool PersistSecurityInfo {
            set { _PersistSecurityInfo = value; }
        }

        public String ConnectorFormat {
            set {
                _ConnectorFormat = value;
            }
        }

        public String AttachDbFileName {
            set {
                _AttachDbFileName = value;
            }
        }
        public String GeneratePassthrough() {
            return String.Format(_ConnectorFormat ?? "Data Source={0};" + (_AttachDbFileName != null ?
                String.Format(@"AttachDbFilename={0}", _AttachDbFileName) : "Initial Catalog={1};") +
                (_IntegratedSecurity ? "Integrated Security=True;" : "User ID={2};Password={3};Persist Security Info={4};MultipleActiveResultSets=True;"), DataSource, InitialCatalog, UserID, Password, _PersistSecurityInfo);

        }
        #endregion

    }
}
