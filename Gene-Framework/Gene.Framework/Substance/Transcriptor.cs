﻿using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Dynamic;
using System.Data.Entity;
using System.Data;
using System.Reflection;
using System.Collections.Generic;
using Gene.Framework;

namespace Gene.Framework {
    public class Transcriptor : ITranscriptor {

        #region Fields
        private string _GeneIdentifier;
        private INucleus _ViewPort;
        private Type _GeneType;
				private static IClassParser _ClassParser;
        #endregion

        #region Properties

        private Nucleus GeneViewPort {
            get {
                return _ViewPort as Nucleus;
            }
        }

        private DbSet DataSet {
            get {
                return GeneViewPort.Set(GeneType);
            }
        }

        private Type GeneType {
            get {
                if (_GeneType == null) {
                    _GeneType = AppDomain.CurrentDomain.GetAssemblies().Single(@as => @as.GetType(_GeneIdentifier) != null).GetType(_GeneIdentifier);
                }
                return _GeneType;
            }
        }

        #endregion

        #region ITranscriptor Members

        public string GeneIdentifier {
            set { _GeneIdentifier = value; }
        }

        public ITranscriptor Insert(dynamic item) {
            DataSet.Add(item);
            return this;
        }

        public ITranscriptor Swap(dynamic id, dynamic item) {
            var secretion = SFactory.Object().Generate<ISecretion>(null, DataSet);
            var dbItem = secretion.The(id);
						_ClassParser = _ClassParser ?? SFactory.Object().Generate<IClassParser>();
						_ClassParser.HollowMembers<dynamic>(item, dbItem);
            if (GeneViewPort.Entry(dbItem).State == EntityState.Detached) {
                DataSet.Attach(dbItem);
            }
            return this;
        }

        public ITranscriptor Delete(dynamic item) {
            DataSet.Remove(item);
            return this;
        }

        public INucleus Nucleus {
            set {
                _ViewPort = value;
            }
        }

        #endregion

        #region IGolgi Members

        public ISecretion Secrete() {
            var secretion = SFactory.Object().Generate<ISecretion>(null, DataSet);
            if (GeneViewPort.ChangeTracker.Entries().Count() == 0) {
                secretion.Gain = EGain.Warning;
            } else {
                try {
                    GeneViewPort.SaveChanges();
                    secretion.Gain = EGain.Done;
                } catch (Exception ex) {
                    secretion.Append(ex);
                    secretion.Gain = EGain.Fail;
                }
            }

            return secretion;
        }
        public IGolgi Reincarnate() {

            var secretion = SFactory.Object().Generate<ISecretion>(null, DataSet);
            secretion.Each((i, d) => Delete(d));
            return this;

        }

        #endregion

    }
}
