﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Gene.Framework {
    internal class ClassParser : IClassParser {

        #region Methods
        public void HollowMembers<T>(T source, T destination) where T : class {
            var props = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public);
            foreach (var prop in props) {
                var v = prop.GetValue(source, null);
                if (IsAssigned<T>(v)) {
                    prop.SetValue(destination, v, null);
                }
            }
        }
        #endregion

        #region Private
        private Boolean IsAssigned<T>(Object o) {
            var assigned = (o != null && !String.IsNullOrEmpty(o.ToString()))
                || (typeof(T).Equals(typeof(ValueType))
                && (o.ToString() != "0" || o.ToString() != Guid.Empty.ToString()));
            return assigned;
        }
        #endregion

    }
}
