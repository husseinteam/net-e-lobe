﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Dynamic;
using Ground.Framework.Properties;
using Gene.Framework;

namespace Ground.Framework.Seeds {
	internal class AddressesSeed : ISeeder<ETAddress> {

		#region Outlet Methods

		internal ICollection<ETAddress> GetFullList() {

			var mock = new List<ETAddress>();

			using (var connection = new OleDbConnection(Settings.Default.AddressesConnectionString)) {
				var ctCity = "SELECT tblİller.[Kimlik] AS ID, tblİller.[İlAdı] AS Identifier, tblİller.[İlKodu] AS AddressCode FROM tblİller;";
				var cmCity = new OleDbCommand(ctCity, connection);
				cmCity.Connection.Open();
				var rdrCity = cmCity.ExecuteReader(CommandBehavior.CloseConnection);

				while (rdrCity.Read()) {
					var idCity = rdrCity["ID"].ToString();
					var identifierCity = rdrCity["Identifier"].ToString();
					var addressCodeCity = rdrCity["AddressCode"].ToString();

					var ctCounty = String.Format("SELECT tblİlçeler.Kimlik AS ID, tblİlçeler.İlKodu AS CityAddressCode, tblİlçeler.İlçeAdı AS Identifier, tblİlçeler.İlçeKodu AS AddressCode FROM tblİlçeler WHERE tblİlçeler.İlKodu='{0}';"
							, addressCodeCity);
					var cmCounty = new OleDbCommand(ctCounty, connection);
					var rdrCounty = cmCounty.ExecuteReader();

					//Mock Object
					var city = new ETAddress()
						.Primase<ETAddress>(adr => adr.Epitope = Guid.NewGuid())
						.Primase<ETAddress>(adr => adr.Site = identifierCity)
						.Primase<ETAddress>(adr => adr.AddressCode = addressCodeCity)
						.Primase<ETAddress>(adr => adr.ParentAddress = null)
						.TerminateSequence<ETAddress>();

					mock.Add(city);

					while (rdrCounty.Read()) {
						var idCounty = rdrCounty["ID"].ToString();
						var cityAddressCodeCounty = rdrCounty["CityAddressCode"].ToString();
						var identifierCounty = rdrCounty["Identifier"].ToString();
						var addressCodeCounty = rdrCounty["AddressCode"].ToString();

						//Mock Object
						var county = new ETAddress()
							.Primase<ETAddress>(adr => adr.Epitope = Guid.NewGuid())
							.Primase<ETAddress>(adr => adr.Site = identifierCounty)
							.Primase<ETAddress>(adr => adr.AddressCode = addressCodeCounty)
							.Primase<ETAddress>(adr => adr.ParentAddress = city)
						.TerminateSequence<ETAddress>();

						mock.Add(county);

						var ctDistrict = String.Format("SELECT TblMahalleler.[Kimlik] AS ID, TblMahalleler.[İlçeKodu] AS CountyAddressCode, TblMahalleler.[MahalleAdı] AS Identifier, TblMahalleler.[Tür] AS DistrictType FROM TblMahalleler WHERE TblMahalleler.[İlçeKodu]='{0}';", addressCodeCounty);
						var cmDistrict = new OleDbCommand(ctDistrict, connection);
						var rdrDistrict = cmDistrict.ExecuteReader();

						while (rdrDistrict.Read()) {
							var idDistrict = rdrCounty["ID"].ToString();
							var countyAddressCodeDistrict = rdrDistrict["CountyAddressCode"].ToString();
							var identifierDistrict = rdrDistrict["Identifier"].ToString();
							var typeDistrict = rdrDistrict["DistrictType"].ToString();

							//Mock Object
							var district = new ETAddress() {
								Epitope = Guid.NewGuid(),
								Site = identifierDistrict,
								AddressCode = countyAddressCodeDistrict,
								ParentAddress = county
							};
							mock.Add(district);

							var ctStreet = String.Format("SELECT TblCaddeler.[Kimlik] AS ID, TblCaddeler.[İlçeKodu] AS CountyAddressCode, TblCaddeler.[MahalleAdı] AS DistrictIdentifier, TblCaddeler.[CaddeAdı] AS StreetIdentifier, TblCaddeler.[Tür] AS DistrictType FROM TblCaddeler WHERE TblCaddeler.[İlçeKodu]='{0}' and TblCaddeler.[MahalleAdı]='{1}';"
									, countyAddressCodeDistrict, identifierDistrict);
							var cmStreet = new OleDbCommand(ctStreet, connection);
							var rdrStreet = cmStreet.ExecuteReader();

							while (rdrStreet.Read()) {
								var idStreet = rdrCounty["ID"].ToString();
								var countyAddressCodeStreet = rdrStreet["CountyAddressCode"].ToString();
								var identifierDistrictStreet = rdrStreet["DistrictIdentifier"].ToString();
								var identifierStreet = rdrStreet["StreetIdentifier"].ToString();
								var typeDistrictStreet = rdrStreet["DistrictType"].ToString();

								//Mock Object
								var street = new ETAddress() {
									Epitope = Guid.NewGuid(),
									Site = identifierStreet,
									AddressCode = countyAddressCodeStreet,
									ParentAddress = district
								};
								mock.Add(street);
							}
							rdrStreet.Close();
						}
						rdrDistrict.Close();
					}
					rdrCounty.Close();
				}
				rdrCity.Close();
			}

			return mock;

		}
		public ICollection<ETAddress> GetSeed(int N = 10) {
			var mock = new List<ETAddress>();
			//Mock Object
			var izmir = new ETAddress() {
				Epitope = Guid.NewGuid(),
				Site = "İzmir",
				AddressCode = "35",
				ParentAddress = null
			};
			mock.Add(izmir);

			var bornova = new ETAddress() {
				Epitope = Guid.NewGuid(),
				Site = "Bornova",
				AddressCode = "35.31",
				ParentAddress = izmir
			};
			mock.Add(bornova);

			return mock;
		}
		#endregion

	}
}
