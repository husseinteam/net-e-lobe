﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using Gene.Framework;

namespace Ground.Framework {
    [Table("Addresses")]
    public class ETAddress : Genome {

        #region Properties
        public String AddressCode { get; set; }
        public String Site { get; set; }
        #endregion

        #region Reference Properties

        public virtual ETAddress ParentAddress { get; set; }

        #endregion

    }
}
