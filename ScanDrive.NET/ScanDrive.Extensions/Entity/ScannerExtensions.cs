﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScanDrive.Entity;
using ScanDrive.Extensions.Code;
using ScanDrive.Extensions.Enums;
using ScanDrive.Facade.Derived;

namespace ScanDrive.Extensions.Entity {
    public static class ScannerExtensions {
        public static IList<FileInfo> Scan(this String rootDirectory
            , Action<Element> tick) {

            ScannerExtensions.tick = tick;
            var items = rootDirectory.CrawlTopDown<Int32>(Commit
                , str => new FileInfo(str)).Result;
            return items;

        }

        #region Private
        private static async Task Commit(FileInfo fi, EElementType elementType) {

            var pid =  : 0;
            var e = new Element()
            {
                Name = fi.Name,
                Extension = fi.Extension,
                ParentID = pid
            };
            lock (locked) {
                ScannerExtensions.tick(e);
            }
            await Task.Run(() => Elements.Insert.New(e));

        }
        #endregion

        #region Private

        private static Action<Element> tick;
        private static Object locked = new Object();
        #endregion

    }
}
