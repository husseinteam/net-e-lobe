﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScanDrive.Extensions.Code {
    public static class AssertExtensions {

        public static void Commit(this Boolean trueOrFalse, Action<Boolean> commit) {

            commit(trueOrFalse);

        }

        public static void Assert<TException>(this Boolean isTrue, String message) 
                where TException : Exception, new() {

            if (!isTrue) {
                var ex = Activator.CreateInstance(typeof(TException), message)
                    as TException;
                throw ex;
            }

        }

    }
}
