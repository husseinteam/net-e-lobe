﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScanDrive.Extensions.Code {
    public static class ParseExtensions {

        public static Int32 ToInt32(this Object instance) {

            try {
                return (Int32)Convert.ToUInt32(instance);
            } catch {
                return (Int32)Convert.ToUInt32(instance.ToString());
            }
        }

    }
}
