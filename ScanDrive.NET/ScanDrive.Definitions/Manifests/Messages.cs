﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScanDrive.Definitions.Base;
using ScanDrive.Extensions.Code;

namespace ScanDrive.Definitions.Manifests {
    public class Messages : BaseManifest<String> {

        protected override String Category {
            get {
                return "Messages";
            }
        }

        protected override Dictionary<String, String> Book {
            get {
                return new Dictionary<String, String>()
                {
                    { "", "" },
                };
            }
        }
    }
}
