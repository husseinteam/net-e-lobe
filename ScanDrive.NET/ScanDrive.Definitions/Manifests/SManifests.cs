﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScanDrive.Definitions.Manifests {
    public static class SManifests {


        #region Fields
        private static Lazy<Messages> _Messages = new Lazy<Messages>(() => new Messages());
        #endregion

        public static Messages ForMessage {

            get {
                return _Messages.Value;
            }

        }

    }
}
