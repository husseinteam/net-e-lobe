﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScanDrive.Entity;
using ScanDrive.Facade.Derived;

namespace ScanDrive.Core.UnitTests.Facade {
    [TestClass]
    public class ElementsTruncate {
        [TestMethod]
        public void Truncate() {

            Elements.Delete.Truncate();
            var dt = Elements.Select.All();
            Assert.AreEqual(0, dt.Rows.Count);

        }
    }
}
