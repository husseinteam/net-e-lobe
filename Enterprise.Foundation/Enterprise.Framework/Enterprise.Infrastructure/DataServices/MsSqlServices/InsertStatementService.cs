﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core;
using Enterprise.Core.Decorators;

namespace Enterprise.Infrastructure {
	internal class InsertStatementService<TModel> : StatementService<TModel>, IStatementService<TModel> {

		#region Overrides

		public override SqlCommand GetCommand() {

			var tableName = GetTableName();
			var columnsList = new StringBuilder();
			var valuesList = new StringBuilder();
			var columns = base.ListColumns();
			var typeParser = SEnterprise.GetTypeParser();
			var dbParameters = new List<SqlParameter>();

			columns.Where(col => !col.DataConstraints.Any(c => c.ConstraintType == EConstraintType.PrimaryKey)).ToList()
				.ForEach(col => {
					columnsList.AppendFormat(" [{0}], ", col.Title.Item);
				});

			var identityColumns = columns.Where(col => col.DataConstraints.Any(c => c.ConstraintType == EConstraintType.PrimaryKey));

			var values = typeParser.AnalyzeParameters<TModel>(this.Item).Where(val => !identityColumns.Any(icol => icol.Title.Item == val.Key));

			values = values.Select(val => {
				var isEnum = val.Column != null && val.Column.DataType == EDataType.Enumeration;
				var value = isEnum ? typeParser.ParseCode(val.Value) : val.Value;
				var dbParameter = new SqlParameter(val.Key, value);
				return new {
					Key = val.Key,
					Value = value,
					DbParameter = dbParameter
				};
			});

			values.ToList().ForEach(val => {
				valuesList.AppendFormat(" @{0}, ", val.Key);
				dbParameters.Add(val.DbParameter);
			});

			if (dbParameters.Count == 0) {
				throw new ArgumentException(String.Format("Type<{0}> doesn't provide required minimum", typeof(TModel).Name));
			}

			var commandText = String.Format("INSERT INTO {0}({1}) VALUES({2});", tableName
				, columnsList.ToString().TrimEnd(' ', ','), valuesList.ToString().TrimEnd(' ', ','));
			var command = new SqlCommand(commandText);
			command.Parameters.AddRange(dbParameters.ToArray());
			return command;
		}

		#endregion

	}
}
