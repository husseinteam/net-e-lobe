﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Enterprise.Core;

namespace Enterprise.Infrastructure {
	internal class DropTableService<TModel> : StatementService<TModel>, IStatementService<TModel> {

		#region Overrides

		public override SqlCommand GetCommand() {

			var dropFormat = "DROP TABLE {0};";
			var commandText = String.Format(dropFormat, GetTableName());
			var command = new SqlCommand(commandText);
			return command;

		}

		#endregion

	}
}
