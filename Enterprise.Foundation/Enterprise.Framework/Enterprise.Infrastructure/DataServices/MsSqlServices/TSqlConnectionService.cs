﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core;

namespace Enterprise.Infrastructure {
	internal class TSqlConnectionService : ConnectionService<SqlConnection>, IConnectionService<SqlConnection> {

		#region Ctor

		public TSqlConnectionService(IConnectionCredential credential)
			: base(credential) {

		}

		#endregion

	}
}
