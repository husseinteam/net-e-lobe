﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core;

namespace Enterprise.Infrastructure {
	internal class DataParameter<TParameter> : Service<TParameter>, IDataParameter<TParameter> {

		#region Private

		private Config<String> title;

        #endregion

        #region Ctor

		public DataParameter(String title) {
			this.title = new Config<String>(title); 
		}

		#endregion

		#region IDataParameter<TItem> Members

		public IConfig<String> Title {
			get {
				return this.title;
			}
		}

		#endregion
	}
}
