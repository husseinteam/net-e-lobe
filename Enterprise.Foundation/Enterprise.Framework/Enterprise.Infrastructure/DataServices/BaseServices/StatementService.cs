﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core;
using Enterprise.Core.Decorators;

namespace Enterprise.Infrastructure {

	internal abstract class StatementService<TModel> : Service<TModel>, IStatementService<TModel> {

		#region Ctor

		public StatementService() {
			this.schema = new Service<String>();
			this.schema.Set("dbo");
		}

		#endregion

		#region IStatementService<TModel> Members

		public abstract SqlCommand GetCommand();

		public IList<IDataColumn<TModel>> ListColumns() {

			var cols = typeof(TModel).GetProperties().Where<PropertyInfo>(pi => pi.GetCustomAttribute<ColumnAttribute>() != null)
				.Select<PropertyInfo, IDataColumn<TModel>>(fi => this.ExtractDataColumn(fi.GetCustomAttributes()));
			return cols.ToList();

		}

		public IService<String> Schema {
			get { return this.schema; }
		}

		#endregion

		#region Protected

		protected String GetTableName() {

			var tableMapping = typeof(TModel).GetCustomAttributes(typeof(TableMappingAttribute), false).FirstOrDefault() as TableMappingAttribute;
			var tableName = tableMapping != null ? tableMapping.TableName : String.Format("Table{0}", Guid.NewGuid().ToString("N"));
			return tableName;

		}

		private IDataColumn<TModel> ExtractDataColumn(IEnumerable<Attribute> attrs) {

			var dc = new Column<TModel>(attrs);
			return dc;

		}

		protected IService<string> schema;
		
		#endregion

	}
}
