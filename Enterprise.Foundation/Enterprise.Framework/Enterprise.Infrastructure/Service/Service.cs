﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core;

namespace Enterprise.Infrastructure {
    [MarkAsBase(typeof(IService<>))]
	internal class Service<TItem> : Config<TItem>, IService<TItem> {

		#region Ctor

		public Service() {

		}

		public Service(Func<TItem, TItem> generator)
			: base(generator) {
		}

		#endregion

		#region IService<TItem> Members

		public TResponse As<TResponse>()
			where TResponse : class, IService<TItem> {

			return this as TResponse;

		}

		public IService<TItem> Set(TItem item) {

			this.item = item;
			return this;

		}

		public IConfig<TResponse> Translate<TResponse>(Func<TItem, TResponse> engine) {

			return new Config<TResponse>(engine(this.Item));

		}

		#endregion
		
		#region Overrides

		public override IConfig<TItem> Me {
			get {
				return this;
			}
		}

		#endregion

	}
}
