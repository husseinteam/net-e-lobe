﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core.Decorators {
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false, AllowMultiple = false)]
	public sealed class MarkAsImplementorAttribute : Attribute {
		
		public MarkAsImplementorAttribute() {
			
		}

	}
}
