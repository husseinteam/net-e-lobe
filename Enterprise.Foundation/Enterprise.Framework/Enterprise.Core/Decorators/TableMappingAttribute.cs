﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core.Decorators {

	[AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
	public sealed class TableMappingAttribute : Attribute {

		readonly String tableName;
		
		public TableMappingAttribute(String TableName) {
			this.tableName = TableName;
		}

		public String TableName {
			get { return this.tableName; }
		}

	}
}
