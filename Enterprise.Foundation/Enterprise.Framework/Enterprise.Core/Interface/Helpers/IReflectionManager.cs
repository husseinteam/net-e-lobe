﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core {
	public interface IReflectionManager {

		#region Methods

		Object ExecuteMethod(dynamic target, String methodName, Type[] genericTypes, params Object[] parameters);

		TResponse ExecuteMethod<TResponse>(dynamic target, String methodName, Type[] genericTypes, params Object[] parameters)
			where TResponse : class;

		#endregion

	}
}
