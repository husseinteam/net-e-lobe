﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Enterprise.Core {
	public interface ILexerService {

		#region Methods

		String Pluralise(string name);
		String CapitalLetterToLower(String name);
		String GenerateIdentifier(String prefix = "", Int32 magnitude = 4);

		#endregion

	}
}
