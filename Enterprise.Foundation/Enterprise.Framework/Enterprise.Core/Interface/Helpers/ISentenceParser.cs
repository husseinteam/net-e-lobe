﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core {
	public interface ISentenceParser {

		ISentenceParser Construct(String formatSentence);
		String From(params String[] arguments);

	}
}
