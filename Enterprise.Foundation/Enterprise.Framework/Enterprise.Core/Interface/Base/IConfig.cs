﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core {
	public interface IConfig<TItem> : ISingularity<IConfig<TItem>> {

		#region Properties

		TItem Item { get; }

		#endregion


	}
}
