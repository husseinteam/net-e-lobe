﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core {
	public interface ITSqlModelService<TModel> : IModelService<TModel>
	where TModel : new() {

	}
}
