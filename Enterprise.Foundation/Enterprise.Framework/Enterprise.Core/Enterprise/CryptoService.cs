﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Enterprise.Core {
  internal class CryptoService : ICryptoService{

    #region Public Methods

    public String GetMD5String(String value) {
      var md5 = new MD5CryptoServiceProvider();

      var result = md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(value));
      var strBuilder = new StringBuilder();
      for (int i = 0; i < result.Length; i++) {
        strBuilder.Append(result[i].ToString("x2"));
      }

      return strBuilder.ToString();

    }
    
    public byte[] Encrypt(string plainText, byte[] key, byte[] iv) {
      ICryptoTransform encryptor = crypto.CreateEncryptor(key.ToArray(), iv.ToArray());
      using (var msEncrypt = new MemoryStream()) {
        var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);
        var swEncrypt = new StreamWriter(csEncrypt);
        swEncrypt.Write(plainText);
        swEncrypt.Dispose();
        csEncrypt.Dispose();
        return msEncrypt.ToArray();
      }
    }

    public string Decrypt(byte[] cipher, byte[] key, byte[] iv) {
      ICryptoTransform decryptor = crypto.CreateDecryptor(key.ToArray(), iv.ToArray());
      using (var msDecrypt = new MemoryStream(cipher.ToArray())) {
        var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read);
        var srDecrypt = new StreamReader(csDecrypt);
        return srDecrypt.ReadToEnd();
      }
    }

    public void GenerateSymmetricAlgorithms(out byte[] key, out byte[] iv) {
      using (var crpt = new RijndaelManaged()) {
        crpt.GenerateKey();
        crpt.GenerateIV();
        key = crpt.Key;
        iv = crpt.IV;
      }
    }
    
    #endregion

    #region Fields

    private RijndaelManaged crypto = new RijndaelManaged();

    #endregion

  }
}