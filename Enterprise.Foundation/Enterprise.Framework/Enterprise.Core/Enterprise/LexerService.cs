﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Enterprise.Core.Interface.Helpers.Deposit;

namespace Enterprise.Core {
	internal class LexerService : ILexerService {

		#region Fields

		private IEnumerable<String> Faust { get; set; }

		#endregion

		#region Public Static Methods

		public string Pluralise(string name) {
			return name.EndsWith("y") ? name.Substring(0, name.Length - 1) + "ies" : name.EndsWith("s") || name.EndsWith("z") ? name + "es" : name + "s";
		}

		public String CapitalLetterToLower(String name) {
			return name[0].ToString().ToLowerInvariant() + name.Substring(1);
		}

		public String GenerateIdentifier(String prefix = "", Int32 magnitude = 4) {
			
			var r = new Random();
			SetFaust();

			var identifier = new StringBuilder("");
			for (int i = 0; i < magnitude; i++) {
				identifier.Append(Faust.ElementAt(r.Next(Faust.Count())));
			}

			return prefix + identifier.ToString();

		}

		#endregion

		#region Private

		private void SetFaust() {
			if (Faust == null) {
				var text = Membrane.faust;
				Faust = Regex.Matches(text, @"([a-zA-Z]{4,})").OfType<Match>().Select<Match, String>((m) => m.Value.ToUpperInvariant()[0] + m.Value.ToLowerInvariant().Substring(1));
			}
		}

		#endregion

	}
}
