﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterprise.Core {
	internal class SentenceParser : ISentenceParser {

		#region Private

		private String formatSentence { get; set; }

		#endregion

		#region IParser Members

		public ISentenceParser Construct(String formatSentence) {

			this.formatSentence = formatSentence;
			return this;

		}

		public String From(params String[] arguments) {

			return String.Format(this.formatSentence, arguments);

		}

		#endregion

	}
}
