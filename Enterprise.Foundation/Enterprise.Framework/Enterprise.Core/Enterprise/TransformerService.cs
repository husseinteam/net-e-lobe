﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enterprise.Core.Decorators;

namespace Enterprise.Core {
	internal class TransformerService : ITransformerService {

		#region ITransformService Members

		public dynamic GenerateFormData(ITypeParser typeParserService, Object sourceObject) {

			Type type = sourceObject.GetType();

			Func<String, LocalsAttribute, String> formInputExtractor = (prop, local) => local.Language.GetProperty(prop)
				.GetValue(Activator.CreateInstance(local.Language)).ToString();

			Func<ITypeParser, IEnumerable<dynamic>> inputsExtractor = (tp) => tp.ExecuteByReflection<IEnumerable<dynamic>>("AnalyzeParameters"
				, new Type[] { type }
				, sourceObject).Where(param => (param.PropertyType as Type).GetCustomAttribute<ShallRenderAttribute>() != null)
				.Select(new Func<dynamic, dynamic>(param =>
					new {
						label = formInputExtractor(param.Key, type.GetCustomAttribute<LocalsAttribute>()),
						inputID = "input-" + param.Key,
						inputValue = param.Value,
						inputPlaceholder = "Please Enter a Value",
						inputType = GetInputType(param.Column.DataType)
					}));
			Func<ITypeParser, IEnumerable<dynamic>> optionsExtractor = (tp) => tp.ExecuteByReflection<IEnumerable<dynamic>>("AnalyzeParameters"
					, new Type[] { type }, sourceObject)
					.Where(param => param.Column.DataType == EDataType.Enumeration)
					.Select(param =>
						tp.ExecuteByReflection<dynamic>("AnalyzeEnumValues", new Type[] { param.PropertyType as Type }));
			var formData = new {
				Close = "Close",
				ModalTitle = "Modal Title",
				SaveChanges = "Save Changes",
				InputFields = inputsExtractor(typeParserService),
				OptionFields = optionsExtractor(typeParserService)

			};
			return formData;

		}
		public dynamic GenerateMutationList(ITypeParser typeParserService, Type sourceType, IEnumerable<dynamic> list) {

			var lang = Activator.CreateInstance(sourceType.GetCustomAttribute<LocalsAttribute>().Language);
			Func<dynamic, String> enumParser = (col) =>
				(typeParserService.ExecuteByReflection<dynamic>("AnalyzeEnumValues", new Type[] { col.PropertyType as Type })
				.Options as IEnumerable<dynamic>).Single(new Func<dynamic, Boolean>(op => op.DataValue == (int)col.Value)).TextValue;

			Func<Object, IEnumerable<dynamic>> columnsExtractor = (ms) =>
				typeParserService.ExecuteByReflection<IEnumerable<dynamic>>("AnalyzeParameters", new Type[] { sourceType }
				, typeParserService.GetValueOf(ms, "Item"))
				.Select<dynamic, dynamic>(col => {
					if (col.Column.DataType == EDataType.Enumeration) {
						col.Value = enumParser(col);
					}
					return col;
				});

			var sourceObject = Activator.CreateInstance(sourceType);
			dynamic colList = typeParserService.ExecuteByReflection<IEnumerable<dynamic>>("FilterParameters"
				, new Type[] { sourceType, typeof(ColumnAttribute), typeof(ShallRenderAttribute) }
				, new Func<ColumnAttribute, ShallRenderAttribute, Boolean>((ca, sha) => sha != null)
				, new Func<ColumnAttribute, ShallRenderAttribute, dynamic>((ca, sha) => new {
					Label = ca.DataType == EDataType.Enumeration
						? typeParserService.GetValueOf(lang, typeParserService.GetValueOf(sourceObject, ca.Title).GetType().Name)
						: ca.Title,
					Title = ca.Title
				}));
			return new {
				ColumnList = colList,
				DataList = new {
					Rows = list.Select(row => new {
						Columns = columnsExtractor(row),
						RowID = typeParserService.GetValueOf(typeParserService.GetValueOf(row, "Item"), "ID")
					})
				}
			};

		}
		#endregion

		#region  Private

		private String GetInputType(EDataType dt) {

			switch (dt) {
				case EDataType.Integer:
					return "number";
				case EDataType.VarChar:
					return "text";
				case EDataType.NVarChar:
					return "text";
				case EDataType.Date:
					return "date";
				case EDataType.Money:
					return "text";
				case EDataType.UniqueIdentifier:
					return "text";
				case EDataType.Enumeration:
					return "select";
				case EDataType.Boolean:
					return "checkbox";
				default:
					throw new NotImplementedException("default");
			}

		}

		#endregion

	}
}
