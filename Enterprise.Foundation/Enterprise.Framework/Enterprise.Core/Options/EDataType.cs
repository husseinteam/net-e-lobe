﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Enterprise.Core{
	public enum EDataType {

		[Description("int")]
		Integer = 1,

		[Description("varchar")]
		VarChar,

		[Description("nvarchar")]
		NVarChar,

		[Description("datetime")]
		Date,

		[Description("numeric")]
		Money,

		[Description("uniqueidentifiier")]
		UniqueIdentifier,

		[Description("int")]
		Enumeration,

		[Description("bit")]
		Boolean
	}
}
