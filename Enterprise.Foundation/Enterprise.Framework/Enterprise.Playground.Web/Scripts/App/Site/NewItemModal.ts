﻿class NewItemModal {


	static ConstructUI(config: ConfigSeen) {

		SiteMaster.AsyncGenome.Add(Code.Construct('key', 'NewItemModal'));

		if (App.Task.Core.IDExists(document.location.pathname)) {
			SiteMaster.SelectModelsForDetail(config);
		} else {
			SiteMaster.SelectModel(config);
		}

		config.AjaxTransform('NewItemModal', SiteMaster.AsyncGenome, NewItemModal.TransformArrived);
		return NewItemModal;

	}

	static TransformArrived(data: any) {

		var response = data.response;
		function NewItemModalViewModel() {
			var self = this;
			self.FormData = {
				Close: ko.observable(response.FormData.Close),
				ModalTitle: ko.observable(response.FormData.ModalTitle),
				SaveChanges: ko.observable(response.FormData.SaveChanges),
				OptionTitle: ko.observable(response.FormData.OptionTitle),
				InputFields: ko.observableArray(response.FormData.InputFields),
				OptionFields: ko.observableArray(response.FormData.OptionFields)
			};

			self.OptionFieldsExist = ko.observable(response.FormData.OptionFields.length > 0);
			self.InputFieldsExist = ko.observable(response.FormData.InputFields.length > 0);

		};

		SiteMaster.AsyncGenome.Add(Code.Construct('NewItemModalViewModel', NewItemModalViewModel));

		SiteMaster.SubscribeIndeeder('NewItemModalViewModel', data);

		SiteMaster.AsyncGenome.Indeed();

	}

	static Check(err: ErrorEvent, genome: Genome<Object>) {

		genome.Add(Code.Construct('StatusText', err['statusText']));

	}

	static Always(genome: Genome<Object>) {

		App.Task.Stream().SwitchTo(1);

	}
} 