class MainStream {

	constructor(transition: number, private anime?: Anime, private clickRegister = 0, private distRegister = 0) {
		this.anime = anime || new Anime('wanderfly');
	}

	static generate(config: ConfigSeen) {
		return new MainStream(config.TransitionLength);
	}

	SwitchTo(i: number) {
		ObjectModel.that('a[data-stream-index="' + i + '"]').click();
	}

	registerFlow() {

		var streamAnchors = new Array<HTMLAnchorElement>();
		var al = document.getElementsByTagName('a');
		for (var j = 0; j < al.length; j++) {
			var a = (<HTMLAnchorElement>al.item(j));
			if (a.attributes['data-stream-index'] !== undefined) {
				streamAnchors.push(al.item(j));
			}
		}
		for (var j = 0; j < streamAnchors.length; j++) {
			var a = streamAnchors[j];
			this.a(streamAnchors.length, a)
		}
		return this;

	}

	private a(count: number, a: HTMLAnchorElement) {
		a.addEventListener('click', function (that: HTMLAnchorElement, e: MouseEvent) {
			e.preventDefault();
			var clickedIndex = parseInt(a.attributes['data-stream-index'].value, 10);
			var diff = this.clickRegister - clickedIndex;
			var dist = this.distRegister;
			if (diff < 0) {
				//up
				for (var k = clickedIndex - 1; k >= clickedIndex + diff; k--) {
					dist -= document.getElementById('stream-' + k).clientHeight;
				}
			} else if (diff > 0) {
				//down
				for (var k = clickedIndex + 1; k <= clickedIndex + diff; k++) {
					dist += document.getElementById('stream-' + k).clientHeight;
				}
			} else {
				//just to the frame of clicked index
				dist = this.distRegister;
			}

			this.d(count, dist);
			this.clickRegister = clickedIndex;
			this.distRegister = dist;
			return false;
		}.bind(this, a), false);
	}

	private d(count: number, dist) {
		var pattern = 'translateY(' + (dist + ObjectModel.documentTop) + 'px)';
		for (var j = 0; j < count; j++) {
			//flow by dist
			var s = document.getElementById('stream-' + j);
			s && this.anime.monaic(s, pattern);
		}
	}

} 