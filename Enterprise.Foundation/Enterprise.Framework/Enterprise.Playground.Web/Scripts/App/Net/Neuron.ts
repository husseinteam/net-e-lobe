class Act<T extends Neuron> {

    constructor(public verb: (done: boolean, t: T, next: (done: boolean, bus?: Genome<string>) => void) => void) {

    }

}


class Neuron {

    private act: Act<Neuron>;
    private bus: Genome<string>;
    private soma: Soma;

    get Threshold() {
        return this.threshold;
    }

    get State() {
        return this.soma.state;
    }

    get Memory() {
        return this.bus;
    }

    get Value() {
        return this.soma.body.get('value');
    }

    get Affirmative() {
        return this.thresholder(this);
    }

    get Identity() {
        return this.soma.body.get('id') || this.soma.body.get('name');
    }

    private invoke(state: boolean) {

        this.soma.invoke(state);
        return this;

    }

    constructor(private thresholder: (t: Neuron) => boolean, private threshold = 0) {
        this.thresholder = thresholder;
    }

    actify(verb: (done: boolean, t: Neuron, next: (done: boolean, bus?: Genome<string>) => void) => void) {

        this.act = new Act<Neuron>(verb);
        return this;

    }

    modify(that: (corpus: Soma) => void) {

        that(this.soma);
        return this;

    }

    somatize(soma: Soma) {

        this.soma = soma;
        return this;

    }


    memorize(bus: Genome<string>) {

        this.bus = this.bus ? this.bus.Extend(bus) : bus;
        return this;

    }

    Forget(...exclude: string[]) {

        if (exclude.length > 0) {
            this.bus.ForEach((c, i?, g?) => !exclude.some(e=> Code.Transform(e) === c.code) && g.Remove(c));
        } else {
            this.bus.ForEach((c, i?, g?) => g.Remove(c));
        }
        return this;

    }

    innervate(that: Neuron) {

        if (that instanceof BipolarNeuron || !UIEngine.AreIdentical(this, that)) {
            this.soma.synapse.bind(that);
            that.threshold += 1;
        }
        return that;

    }

    charge<E extends Event>(type: string, charger: (e: E, neuron: Neuron, fire: () => void) => void) {

        this.soma.body.on<E>(type, (obj, ev) => {
            charger(ev, this,
                () => {
                    this.act.verb(this.Affirmative, this, (advance, bus?) => {
                        if (advance) {
                            this.soma.synapse.jumps(n=> n.memorize(bus).invoke(this.Affirmative).act.verb(n.Affirmative, n, (done, bus?) => n.memorize(bus)));
                        }
                    });
                });
        });
        return this;

    }

    private cascade(step: (neuron: Neuron) => void) {

        var last = this;
        do {
            step(last);
            last = last.soma.synapse.nexus;
        } while (last);
        return this;

    }

    static test() {

        //test('Neuron Test', (assert?) => {
        //    var n = new Neuron(n=> n.soma.body.get('tagName') === 'a').somatize(new Soma(new ObjectModel().create('a'))).actify((done, n, next) => {
        //        assert.ok(done, 'done revoked false!');
        //        next(done);
        //    }).charge<UIEvent>('click', e=> assert.ok(e));

        //    n.soma.body.run('click');
        //});

    }

}

class BipolarNeuron extends Neuron {

    constructor(private excitator: Neuron, private inhibitor: Neuron, threshold: (n: Neuron) => boolean) {
        super(threshold);
        this.somatize(new Soma(new ObjectModel().create('input').mark({ type: 'hidden' }).commit()));
    }

    resolve(resolver: (resolution: boolean, n: Neuron) => void) {

        //this.actify((done, n, next, bus) => {
        //    done && resolver(this.Affirmative, this.State ? this.excitator.memorize(bus) : this.inhibitor.memorize(bus));
        //});
        return this;

    }

}