
class UIEngine {

    private static registeredMessengerTop: number = 54;

    static Progress<T extends Neuron>(n: T) {
        n.modify((soma) => {
            var idSelector = '#' + n.Identity + '-skewed-ll';
            var skewed = new ObjectModel().create('div').mark({ id: idSelector.slice(1) }).hide();
            for (var k = 1; k <= 10; k++) {
                skewed.include(new ObjectModel().create('div').mark({ id: 'skew' + k })
                    .stylize(elem=> [{
                        width: '13px',
                        height: '10px',
                        marginLeft: '2px',
                        display: 'inline-block',
                    }]));
            }
            soma.body.append(skewed);
            skewed
                .animate({
                    0: ['translateX(0px)', 'skew(160deg)', 'scaleX(1.0)', 'opacity: 0.50'],
                    100: ['translateX(180px)', 'skew(160deg)', 'scaleX(0.1)', 'opacity: 0.05']
                }, new AnimeConfig('ease-out', 'infinite', 0, 1.8)).show()
                .stylize(elem => [{
                    width: soma.body.size.width + 'px',
                    backgroundColor: '#101010',
                    position: 'absolute',
                    zIndex: 6666,
                    right: '0px',
                    top: -Math.floor((soma.body.size.height + elem.size.height) / 2) + 'px'
                }]);
            soma.body.mark({ disabled: true });
        });

    }

    static EndProgress<T extends Neuron>(n: T, fadeout?: boolean) {

        n.modify((soma) => {
            var idSelector = '#' + n.Identity + '-skewed-ll';
            if (ObjectModel.contains(idSelector)) {
                ObjectModel.wipe(idSelector);
            }
            soma.body.mark({ disabled: false });
        });
        fadeout && UIEngine.Phantom(n);

    }

    static Phantom<T extends Neuron>(n: T) {

        n.modify(soma => {
            if (!soma.body.get('data-hidden-ll')) {
                soma.body.animate({ 0: ['opacity: 1.0'], 100: ['opacity: 0.0'] }).mark({ dataHiddenLl: true });
            } else {
                soma.body.animate({ 0: ['opacity: 0.0'], 100: ['opacity: 1.0'] }, new AnimeConfig('ease-out', 'forwards', 0, 1.8), true).mark({ dataHiddenLl: false });
            }
        });

    }

    static AreIdentical<T extends Neuron>(n: T, o: T) {

        return n.Identity === o.Identity;

    }

    static GenerateCode<T extends Neuron>(from: T) {

        return new Code<string>().assign(from.Identity, from.Value);

    }

    static CreateMessenger(message: string, withLink?: ObjectModel) {

        UIEngine.registeredMessengerTop = UIEngine.registeredMessengerTop > document.documentElement.clientHeight ? 54 : UIEngine.registeredMessengerTop;
        var msngr = new ObjectModel().create('div').mark({ id: 'div-vector-message' + UIEngine.registeredMessengerTop })
            .stylize(elem=> [{ position: 'fixed', top: ObjectModel.documentTop + UIEngine.registeredMessengerTop + 'px', right: '20px', width: (message.length * 0.6) + 'em', lineHeight: '140%', textAlign: 'center' }])
            .stylize(elem=> [{ borderRadius: '6px', padding: '18px 10px', backgroundColor: 'white', color: '101010', cursor: 'pointer', zIndex: 1800, border: '2px solid #101010', opacity: '0.6' }]);
        withLink ? msngr.include(withLink.put(message)) : msngr.put(message);
        msngr.commit().animate({
            0: ['translateX(' + msngr.size.width + 'px)', 'rotateX(90deg)'],
            100: ['translateX(0px)', 'rotateX(0deg)']
        })
            .on<MouseEvent>('click', (elem, e) =>
                elem.animate({
                    0: ['translateX(0px)', 'rotateX(0deg)'],
                    100: ['translateX(' + (elem.size.width + 20) + 'px)', 'rotateX(90deg)']
                }, null, true).terminate());
        UIEngine.registeredMessengerTop += msngr.size.height + 2;
        withLink && App.Task.Stream();

    }

    static LinkToStream(streamIndex: number, action: (elem: ObjectModel, ev: MouseEvent) => void) {

        var an = new ObjectModel().create('a').mark({ href: '#', 'dataStreamIndex': streamIndex })
            .on<MouseEvent>('click', (elem, ev) => action(elem, ev));
        return an;

    }

    static PaintRainbow(to: HTMLElement) {

        var text = to.innerText;
        to.innerText = '';
        var len = text.length;
        var toObj = new ObjectModel().take(to);
        for (var i = 0; i < len; i++) {
            var span = new ObjectModel().create('span').classify('rb' + i % 5).put(text[i]);
            toObj.include(span);
        }

    }
}