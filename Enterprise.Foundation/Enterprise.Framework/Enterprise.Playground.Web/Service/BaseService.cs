﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using Enterprise.Core;
using Enterprise.Core.Decorators;
using Newtonsoft.Json;

namespace Enterprise.Playground.Web.Service {
	public class BaseService : System.Web.Services.WebService {

		#region Protected Methods

		protected void AssignCorrectListings(dynamic data) {

			String requested = data["RequestedModel"];
			var one = this.DataStore.GetRegisteredDataClasses().Single(dc => dc.Key.Name == requested);
			ModelType = one.Key;

			if ((data as Dictionary<string, object>).Keys.Contains("RelatedModel")) {
				String related = data["RelatedModel"];
				var two = this.DataStore.GetRegisteredDataClasses().Single(dc => dc.Key.Name == related);
				RelatedModelType = two.Key;
			}

		}

		protected String GenerateJson(String message, params dynamic[] from) {

			return JsonConvert.SerializeObject(new {
				response = from.Length == 1 ? from[0] : from,
				message = message
			});

		}

		protected IEnumerable<ColumnAttribute> GetIDColumns(Type from) {
			var idColumns = from.GetProperties().Where(pi => pi.GetCustomAttribute<ConstraintAttribute>() != null)
				.Where(pi => pi.GetCustomAttribute<ConstraintAttribute>().ConstraintType == EConstraintType.PrimaryKey)
				.Select<PropertyInfo, ColumnAttribute>(pi => pi.GetCustomAttribute<ColumnAttribute>());
			return idColumns;
		}

		#endregion

		#region Protected Properties

		protected IDefinitions Definitions = SEnterprise.GetDefinitions();

		protected Type RelatedModelType;

		protected IDataStore<SqlConnection> DataStore {
			get {
				return SEnterprise.SessionItem<IDataStore<SqlConnection>>("DataStore");
			}
		}

		protected IConnectionCredential ConnectionCredential = SEnterprise.GetDefinitions().GetCredential();

		protected Type ModelType { get; private set; }

		protected ILexerService LexiconService = SEnterprise.GetImplementor<ILexerService>().Construct();

		protected ITransformerService TransformService = SEnterprise.GetImplementor<ITransformerService>().Construct();

		protected ITypeParser TypeParserService = SEnterprise.GetImplementor<ITypeParser>().Construct();

		#endregion

	}
}