﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Enterprise.Core.Decorators;
using Enterprise.Playground.Model.Localization.Titles;



namespace Enterprise.Playground.Model {

	[Locals(typeof(Titles), Title = "EWhere")]
	public enum EWhere {

		AtHome = 1,

		AtSchool,

		Outside,

		AtWork

	}
}
