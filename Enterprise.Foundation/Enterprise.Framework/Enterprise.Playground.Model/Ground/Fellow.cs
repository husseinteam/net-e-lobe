﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Enterprise.Core;
using Enterprise.Core.Decorators;
using Enterprise.Playground.Model.Localization.Titles;


namespace Enterprise.Playground.Model {

	[TableMapping("Fellows"), Locals(typeof(Titles))]
	public class Fellow : GenericModel<Fellow> {

		#region Properties

		[ShallRender, Column("PublicIdentifier", EDataType.NVarChar)]
		public String PublicIdentifier { get; set; }

		#endregion

		#region Reference Properties

		[Constraint(EConstraintType.ForeignKey, ReferencingType = typeof(Society), ReferencingColumn = "ID")]
		[Column("SocietyID", EDataType.Integer)]
		public Int32 SocietyID { get; set; }

		[Constraint(EConstraintType.ForeignKey, ReferencingType = typeof(Credential), ReferencingColumn = "ID")]
		[Column("CredentialID", EDataType.Integer)]
		public Int32 CredentialID { get; set; }

		#endregion

		#region Overrides

		public override Fellow Seed<TConnection>(IDataStore<TConnection> dataStore) {

			var that = this.Populate(model => model.PublicIdentifier = LexerServices.GenerateIdentifier("Fellow-"))
				.Populate(model => model.CredentialID = model.RelatedModel<Credential>().Seed<TConnection>(dataStore).ID)
				.Populate(model => model.SocietyID = model.RelatedModel<Society>().Seed<TConnection>(dataStore).ID).As<Fellow>();
			return dataStore.ExecuteDML<Fellow>(that, EDMLType.Insert);

		}

		#endregion

	}
}
